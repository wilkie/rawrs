# The entrypoint (first instruction) for the boot loader and kernel.

.include "const.s"

.global start

.text

# start(): Gets up from power on to a workable state
start:
  # We need to set up a paging structure for the rest of the boot
  # That is, we need to load the kernel

  # Our kernel will be in high memory (0xffff8000_00000000)

  # That address is due to the virtual memory space being 48-bits. All addresses
  # referring to the kernel will be 'negative' in that their first bits are 1.

  # This is nice because all userspace addresses can be 'positive' to quickly
  # distinguish them, but also, both userspace and kernel space can make use of
  # sign-extending immediates when acting upon its memory space. This is called a
  # 'kernel' memory model in systems like Linux.
  la    t0, boot_pdpt
  srli  t0, t0, PAGE_OFFSET
  li    t1, SATP_SV48
  or    t0, t0, t1
  csrw  satp, t0

  # All traps happen in 'supervisor' mode and not machine mode
  li    t0, -1

  # Set the delegation
  csrw  mideleg, t0

  # Except timers, which must happen in machine mode in order to delegate
  li    t1, ~MIP_MTIP
  and   t0, t0, t1

  # And Except supervisor ecalls
  li    t1, ~0x200
  and   t0, t0, t1

  # So, we do not delegate the enable bit for supervisor, just the
  # actual interrupt.
  csrw  medeleg, t0

  # Clear MIP/MIE (clears pending/disables all machine interrupts)
  csrw  mie, zero
  csrw  mip, zero

  # We still need a timer handler in machine mode since it cannot be directly
  # delegated to supervisor mode. The handler will just mret to supervisor mode
  # and perform the delegation in software.

  # We then need to go to 'supervisor' mode
  csrr  t0, mstatus
  li    t1, MSTATUS_MPP_CLEAR
  and   t0, t0, t1
  li    t1, MODE_SUPERVISOR
  sll   t1, t1, MSTATUS_MPP_OFFSET
  or    t0, t0, t1

  # Also, we want to set the SUM bit in mstatus. This lets supervisor mode
  # read USER-mode pages. (for system calls and such)
  li    t1, 1
  sll   t1, t1, MSTATUS_SUM_OFFSET
  or    t0, t0, t1

  # Write the MSTATUS register
  csrw  mstatus, t0

  # Write a stack to the mscratch register
  # First calculate the address of the stack
  la    sp, mtrap_stack
  csrw  mscratch, sp

  # Establish the trap vector
  la    t0, mtrap
  csrw  mtvec, t0

  # Enable timer interrupts via MTIE
  li    t0, MIP_MTIP
  csrw  mie, t0

  # Calculate the address of 'kmain'
  li    t0, KERN_BASE
  la    t1, kmain
  la    t2, start
  sub   t1, t1, t2  # addr = kmain - start
  add   t1, t1, t0  # addr = addr  + KERN_BASE
  csrw  mepc, t1

  # "Return" to Supervisor mode (call kmain)
  mret

mtrap:
  # Preserve the stack
  csrrw sp, mscratch, sp

  # Preserve a0 and a7
  sd    a0, 0(sp)
  sd    a7, 8(sp)

  # Determine the cause
  csrr  a0, mcause

  # If it is positive, it is an ecall from supervisor mode
  bgez  a0, _mtrap_ecall

  # If it is negative and lower bits are equal to 7, it is a machine timer
  sll   a0, a0, 1
  sra   a0, a0, 1
  li    a7, 0x7
  bne   a0, a7, _mtrap_exit

  # Ok, it is a machine timer event

  # Disable the machine timer interrupt
  li    a0, MIP_MTIP
  csrc  mie, a0

  # Raise the supervisor interrupt
  li    a0, MIP_STIP
  csrs  mip, a0

  j     _mtrap_exit

_mtrap_ecall:
  # Set the mepc to the next instruction
  csrr  a0, mepc
  addi  a0, a0, 4
  csrw  mepc, a0

  # Detect the ecall
  li    a0, 32
  bne   a0, a7, _mtrap_exit

  # This is a machine timer set ecall

  # An ecall should enable the timer interrupt
  li    a0, MIP_MTIP
  csrs  mie, a0

  # Clear the MIP
  li    a0, 0x200
  csrc  mip, a0

  j     _mtrap_exit

_mtrap_exit:
  # Retrieve a0 and a7
  ld    a0, 0(sp)
  ld    a7, 8(sp)

  # Return to supervisor mode
  csrrw sp, mscratch, sp
  mret

.balign(4096)
mtrap_stack:
.space(4096)

# Page Directory Page Table (PDPT) for the kernel

# We just need the simple stuff to map in our kernel memory space.

# It is really fun to have a self-referencing page table. And it makes lots
# of stuff a bit easier, honestly. But, RISC-V can't properly do it since PTEs
# (Page Table Entries) cannot both point to tables (inodes) AND leaves (actual
# physical pages)
.balign(4096 * 4)
boot_pdpt:
  # Lots of empty space for unmapped entries
  .space (PTE_SIZE * 256) # 256 empty entries!

  # Entry for 0xffff800000000000
  # This points to our kernel's code and data
  # (which is at physical address 0x80000000)
  # This is a super page, which means it maps the entire 512 GiB range to the
  # kernel. (which is all of RAM)

  # We are mapping 0xffff800000000000 -> physical 0x00000000 (where RAM starts)
  .dword (0x00000000 >> PAGE_OFFSET << PTE_PPN_OFFSET) + (PTE_VALID | PTE_EXECUTE | PTE_WRITE | PTE_READ | PTE_GLOBAL)

  .space (PTE_SIZE * 254) # Lots of empty entries

  # Another mapping of the system memory
  .dword (0x00000000 >> PAGE_OFFSET << PTE_PPN_OFFSET) + (PTE_VALID | PTE_WRITE | PTE_READ | PTE_GLOBAL)
