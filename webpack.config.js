const webpack = require('webpack');
const path = require('path');
const { ESBuildMinifyPlugin } = require('esbuild-loader');
const fs = require('fs');

let targets = {};

let files = fs.readdirSync("./assets/js/rawrs/targets");

files.forEach( (file) => {
    if (file.endsWith(".js")) {
        let target = file.substring(0, file.length - 3);
        targets[target] = [`./assets/js/rawrs/targets/${file}`];
        console.log(` - compiling target: ${target}`);
    }
});

/* This describes the behavior of webpack.
*/
module.exports = {
    /* The main javascript file for the application
    */
    entry: {
        rawrs: ["./assets/js/app.js"],
        xterm: ["./assets/js/vendor-xterm.js", "./assets/js/vendor-xterm.scss"],
        ...targets
    },

    /* The eventual transpiled output file.
    */
    output: {
        path: __dirname + "/assets/js",
        filename: "compiled-[name].js",
        sourceMapFilename: "compiled-[name].js.map"
    },

    mode: "production",

    /* We want source maps!
    */
    devtool: "source-map",

    /* What file types to filter.
    */
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss']
    },

    /* How to load/import modules (for each file).
    */
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: "esbuild-loader",
                options: {
                    target: 'es2015'
                }
            },
            {
                test: /\.s?css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sassOptions: (loaderContext) => {
                                // More information about available properties https://webpack.js.org/api/loaders/
                                const { resourcePath, rootContext } = loaderContext;
                                const relativePath = path.relative(rootContext, resourcePath);

                                return {
                                    includePaths: [path.resolve(__dirname, 'node_modules')],
                                };
                            }
                        }
                    },
                    {
                        loader: 'esbuild-loader',
                        options: {
                            loader: 'css',
                            minify: true
                        }
                    }
                ]
            },
        ]
    },

    /* Minimize all vendored css/js */
    optimization: {
        minimizer: [
            new ESBuildMinifyPlugin({
                target: 'es2015',
                css: true
            })
        ]
    }
}
