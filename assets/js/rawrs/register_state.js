// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Register } from './register.js';
import { EventComponent } from './event_component';

/**
 * This internal class represents a set of registers.
 */
class RegisterCategory extends EventComponent {
    constructor(name, listing) {
        super();

        this._names = Object.keys(listing);

        this.names.forEach( (name) => {
            let register = new Register(name, listing[name], 0);

            // Create the getter/setters
            Object.defineProperty(this, name, {
                get: () => register,
                set: (v) => { register.value = v }
            });

            register.on('change', (value) => {
                this.trigger('change', {
                    category: this,
                    register: register
                });
            });
        });
    }

    /**
     * Returns the name of the category of registers.
     */
    get name() {
        return this._name;
    }

    /**
     * Returns the set of names for each register within the category.
     */
    get names() {
        return this._names;
    }

    /**
     * Updates all registers within the category given a dictionary of values.
     *
     * @param {Object} list - A listing of register values to use.
     */
    update(list) {
        this.names.forEach( (name) => {
            if (list[name]) {
                this[name].value = list[name];
            }
        });
    }
}

/**
 * This class represents the register state of a simulation.
 *
 * You can hook events for when registers change their values and methods to
 * easily manipulate and examine the register state of the simulation.
 */
export class RegisterState extends EventComponent {
    /**
     * Constructs a RegisterState based on the register description given.
     *
     * @param {Object} description - A description of all possible registers.
     */
    constructor(description) {
        super();

        this._categories = Object.keys(description);

        // For every key in the passed dictionary, add a 'get' and 'set' for
        // that category of register.
        this.categories.forEach( (key) => {
            let category = new RegisterCategory(key, description[key]);

            // Create the getter/setters
            Object.defineProperty(this, key, {
                get: () => category,
                set: (v) => { category.update(v) }
            });

            category.on('change', (info) => {
                this.trigger('change', info);
            });
        });
    }

    /**
     * Returns a list of strings corresponding to all known register categories.
     */
    get categories() {
        return this._categories;
    }

    /**
     * Updates all given registers with the values indicated.
     *
     * @param {Object} state - A dictionary of categories and register names with new values.
     */
    update(state) {
        this.categories.forEach( (key) => {
            if (state[key]) {
                this[key].update(state[key]);
            }
        });
    }
}
