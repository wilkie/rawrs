// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Tabs } from './tabs';
import { Util } from '../util';

// Default editors
import { TextEditor } from '../editors/text_editor';
import { ELFEditor } from '../editors/elf_editor';
import { HexEditor } from '../editors/hex_editor';

import { EventComponent } from '../event_component.js';

export class Editors extends EventComponent {
    constructor(element) {
        super();

        // Retain the overall element.
        this._element = element;

        // Get a reference to the file tabs.
        this._tabs = Tabs.load(element.querySelector('#file-tabs'));

        // Bind tab events
        this._tabs.on('change', (tabButton) => {
            let file = tabButton.__file;
            this.trigger('select', file);
        });

        this._tabs.on('action', (info) => {
            let tab = info.tab;
            let action = info.action;

            // Remove the tab and close the editor
            if (action === 'close') {
                this.tabs.remove(tab);

                // Remove our notion of it as well as we can reopen it.
                delete this._editors[tab.__path];
            }
        });

        // Initially, we have no clue how to load any kind of content.
        // This can be affected through the `associate` function.
        this._associations = {
          general: {},
          specific: {}
        };

        // But set default editors for text and binary data.
        this.associate(TextEditor, 'text');
        this.associate(HexEditor, 'binary');

        // Add some for common executable formats.
        this.associate(ELFEditor, 'elf32');
        this.associate(ELFEditor, 'elf64');

        this._editors = {};
    }

    /**
     * Returns the element containing all editors and the file tabs.
     */
    get element() {
        return this._element;
    }

    /**
     * Returns a reference to the Tabs instance managing file tabs.
     */
    get tabs() {
        return this._tabs;
    }

    /**
     * Retrieves the active editor instance.
     */
    get editor() {
        let ret = null;
        return ret;
    }

    /**
     * Retrieves the current target that is being used.
     */
    get target() {
        return this._target;
    }

    /**
     * Sets the current target for the document.
     */
    set target(value) {
        // Clear away current loaded files.
        this.clear();

        // Set the internal target.
        this._target = value;
    }

    /**
     * Adds an association by pairing an Editor class and a type string.
     *
     * Types are specified similar to MIME-types where there is a general and
     * specific class for a given type. The general class is one that has less
     * specificity. For instance 'assembly_riscv/text' has the general type of
     * `text` which signifies that any editor that can handle general text would
     * suffice. However, if there is an editor that can more specifically handle
     * `assembly_riscv` flavored text, it should be used instead.
     *
     * @param {Editor} editor The Editor class to instantiate for this type.
     * @param {string} type The type to match against.
     */
    associate(editor, type) {
        let general = type.split('/').pop();
        let specific = type.split('/')[0];

        // If the general classifier is equal to type, then it is a simple type.
        // In these cases type *only* specifies the general case.
        if (general === type) {
            this._associations.general[general] = editor;
        }
        else {
            // Otherwise, we establish the specific association.
            this._associations.specific[type] = editor;
        }
    }

    /**
     * Retrieves an Editor class to represent the given type.
     */
    editorForType(type) {
        let general = type.split('/').pop();

        // Return the (preferred) specific association or general if it exists.
        // It falls back to the 'binary' type if all else fails (and hopes there
        // is something there!)
        return this._associations.specific[type] ||
               this._associations.general[general] ||
               this._associations.general['binary'];
    }

    /**
     * Focuses on the current editor, if possible.
     */
    focus() {
        if (this.editor) {
            this.editor.focus();
        }
    }

    /**
     * Unfocuses the current editor, if needed.
     */
    blur() {
        if (this.editor) {
            this.editor.blur();
        }
    }

    /**
     * Creates a tab and opens the associated viewer/editor for the given data.
     *
     * The given `file` must have a `name` and should have a `type`. The
     * 'binary' type is given to any file without a `type` field.
     *
     * @param {object} info The file information and metadata.
     *
     * @returns {Editor} The editor created to handle this file.
     */
    async open(info) {
        // Do not open an existing opened file. Instead, go to it.
        if (this.editorFor(info)) {
            // Get that reference
            let editor = this.editorFor(info);

            // Select the tab
            let id = editor.element.getAttribute('id');
            this.tabs.select(id);

            // Return the opened editor.
            return editor;
        }

        // Get the appropriate editor class.
        let editor = this.editorForType(info.type || 'binary');

        // Create a tab
        let tabTemplate = this.element.querySelector('template.file-tab');
        let panelTemplate = this.element.querySelector('template.file-tab-panel');

        let tab = Util.createElementFromTemplate(tabTemplate);
        let panel = Util.createElementFromTemplate(panelTemplate);

        // Update the name
        let tabButton = tab.querySelector('button');
        tabButton.querySelector('span.caption').textContent = info.name;

        // Keep a reference to the original file info (minus data).
        tabButton.__file = {
            path: info.path,
            name: info.name,
            type: info.type
        };

        // Add them to the tab strip.
        this.tabs.add(tab, panel);

        // Get the assigned id.
        let id = panel.getAttribute('id');

        // Switch tabs to the new one.
        this.tabs.select(id);

        // Load the template
        let template = this.element.querySelector(`template.editor.${editor.view}`);
        let editorContent = Util.createElementFromTemplate(template);
        panel.appendChild(editorContent);

        // Load the panel and store it to the element for safekeeping.
        panel.__editor = new editor(panel, info.options || {});

        // Update to the current locale.
        await this.locale.update(panel);

        // Assign the current target to it.
        panel.__editor.target = this.target;

        // Load the file data.
        panel.__editor.load(info.data, info.name, info.type);

        // Handle events and pass them up the chain.
        panel.__editor.on('change', () => {
            this.trigger('change', panel.__editor);
        });

        // Retain path/editor on tab as well
        tab.__path = info.path;
        tab.__editor = panel.__editor;

        // Retain the open tab by path.
        this._editors[info.path] = panel.__editor;

        // Return the Editor instance.
        return panel.__editor;
    }

    /**
     * Finds any open editor for the given file, if exists.
     */
    editorFor(info) {
        return this._editors[info.path];
    }

    /**
     * Removes all editor tabs and instances.
     */
    clear() {
        // Clear our registry of open files
        this._editors = {};

        // Clear tabs
        this.tabs.clear();
    }

    set locale(value) {
        this._locale = value;
    }

    get locale() {
        return this._locale;
    }
}
