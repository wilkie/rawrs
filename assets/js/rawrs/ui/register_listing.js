// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Util }                from '../util.js';
import { EventComponent }      from '../event_component.js';
import { FloatExplorerDialog } from '../dialogs/float_explorer_dialog.js';
import { Dropdown }            from './dropdown.js';
import { Register }            from '../register.js';
import { Tabs }                from './tabs.js';

export class RegisterListing extends EventComponent {
    /**
     * Create a new instance of a register listing for the given root element.
     *
     * The register description comes from the current Target and is implemented
     * for each target specifically. This description separates registers into
     * categories which will each get their own tab in the interface.
     *
     * @param {HTMLElement} root - The element to look for the register listing within.
     * @param {Object} description - The description of the possible registers.
     */
    constructor(root) {
        super();

        // Find and retain the element
        this._element = root.querySelector(".register-file");

        // Bind events
        this.bindEvents();

        // Initially there are no registers
        this._description = {};

        // Reset the view
        this.clear();
    }

    /**
     * Returns the element associated with this register listing.
     *
     * @returns {HTMLElement} The element for this register listing.
     */
    get element() {
        return this._element;
    }

    /**
     * Returns the description of registers this listing visualizes.
     */
    get description() {
        return this._description;
    }

    /**
     * Updates the register description.
     *
     * This will clear the register listing and redraw it.
     */
    set description(value) {
        this._description = value || {};
        this.clear();
    }

    /**
     * Resets the register listing to all zeros.
     */
    clear() {
        // For each category, clone a tab and tab-panel
        let tabTemplate = this.element.querySelector('template.register-tab');
        let tabPanelTemplate = this.element.querySelector('template.register-tab-panel');

        // Populate the table in the tab panel for each register based on its
        // type.
        let intRegisterTemplate = this.element.querySelector('template.int-register');
        let floatRegisterTemplate = this.element.querySelector('template.fp-register');

        // Get the tabs and tab-panels elements
        let tabs = this.element.querySelector('ol.tabs');
        let tabPanels = this.element.querySelector('ol.tab-panels');

        let tabStrip = Tabs.load(tabs);

        // Clear tabs
        tabs.innerHTML = "";
        tabPanels.innerHTML = "";

        // Add each tab and panel
        Object.keys(this.description).forEach( (category) => {
            let tab = Util.createElementFromTemplate(tabTemplate);
            let tabPanel = Util.createElementFromTemplate(tabPanelTemplate);

            // Assign an id and aria-controls
            let id = 'register-category-' + Util.hash(category);
            tabPanel.setAttribute('id', id);
            tab.querySelector('button').setAttribute('aria-controls', id);

            // Update tab text
            tab.querySelector("button").textContent = category;

            // Make it the active tab if it is the first added
            if (tabs.innerHTML === "") {
                tab.classList.add("active");
                tabPanel.classList.add("active");
            }

            // Add the tab
            tabs.appendChild(tab);

            // Bind button events
            tabStrip.bindTabEvents(tab);

            // Get the new, empty register table within the tab panel
            let table = tabPanel.querySelector('table.registers');

            // For each register in the category, add that row to the table
            Object.keys(this.description[category]).forEach( (registerName) => {
                let registerType = this.description[category][registerName];

                let register = null;
                if (registerType === Register.DOUBLEIEEE754) {
                    // Create a field for the floating point representation
                    register = Util.createElementFromTemplate(floatRegisterTemplate);
                }
                else {
                    // Assume it is an integer register
                    register = Util.createElementFromTemplate(intRegisterTemplate);
                }

                // Set id
                register.setAttribute('id', 'register-' + Util.hash(registerName));

                // Assign name
                register.querySelector('td.name').textContent = registerName;

                // Zero out
                let str = "";

                // Pad to proper length
                if (registerType === Register.INT64 || registerType === Register.DOUBLEIEEE754) {
                    str = str.padStart(16, '0');
                }
                else if (registerType === Register.INT32 || registerType === Register.SINGLEIEEE754) {
                    str = str.padStart(8, '0');
                }
                else if (registerType === Register.INT16) {
                    str = str.padStart(4, '0');
                }
                else if (registerType === Register.INT8) {
                    str = str.padStart(2, '0');
                }

                register.setAttribute("data-hex-value", "0x" + str);
                register.querySelector("td.value button").textContent = "0x" + str;
                this.switchToHex(register);

                // Add to table
                table.querySelector('tbody').appendChild(register);

                // Bind events
                this.bindRowEvents(register);
            });

            // Add to panels
            tabPanels.appendChild(tabPanel);
        });
    }

    /**
     * Removes highlights on updated entries.
     */
    unhighlight() {
        this.element.querySelectorAll("tr.register.updated").forEach( (register) => {
            register.classList.remove("updated");
        });
    }

    /**
     * Updates the register listing for the provided register values.
     *
     * @param {RegisterState} regs - The register values.
     */
    update(regs) {
        regs.categories.forEach( (category) => {
            let id = 'register-category-' + Util.hash(category);
            let tabPanel = this.element.querySelector('#' + id);

            if (tabPanel) {
                regs[category].names.forEach( (registerName) => {
                    let reg = regs[category][registerName];
                    let str = reg.value.toString(16);

                    // Pad to proper length
                    if (reg.type === Register.INT64) {
                        str = str.padStart(16, '0');
                    }
                    else if (reg.type === Register.INT32) {
                        str = str.padStart(8, '0');
                    }
                    else if (reg.type === Register.INT16) {
                        str = str.padStart(4, '0');
                    }
                    else if (reg.type === Register.INT8) {
                        str = str.padStart(2, '0');
                    }

                    let id = 'register-' + Util.hash(registerName);
                    let row = tabPanel.querySelector('#' + id);

                    if (row) {
                        // Determine what the value should be encoded as
                        let typeCell = row.querySelector("td.type");
                        let type = (typeCell ? typeCell.textContent : "x");

                        // Determine the new value and if the value has changed
                        let oldContent = row.getAttribute("data-hex-value");
                        let newContent = "0x" + str;
                        row.setAttribute("data-hex-value", newContent);

                        // Retain a reference to the RegisterState
                        row.__registerState = regs;
                        row.__registerCategory = regs[category];
                        row.__register = reg;

                        reg.on('change.listing', () => {
                            let str = reg.value.toString(16);

                            // Pad to proper length
                            if (reg.type === Register.INT64) {
                                str = str.padStart(16, '0');
                            }
                            else if (reg.type === Register.INT32) {
                                str = str.padStart(8, '0');
                            }
                            else if (reg.type === Register.INT16) {
                                str = str.padStart(4, '0');
                            }
                            else if (reg.type === Register.INT8) {
                                str = str.padStart(2, '0');
                            }

                            let oldContent = row.getAttribute("data-hex-value");
                            let newContent = "0x" + str;
                            row.setAttribute("data-hex-value", newContent);

                            // Conform to hex to start
                            this.switchToHex(row);

                            if (oldContent != newContent) {
                                row.classList.add("updated");
                            }

                            // Re-encode to the requested type
                            if (type == "f") {
                                this.switchToFloat(row);
                            }
                            else if (type == "d") {
                                this.switchToDouble(row);
                            }
                        });

                        // Conform to hex to start
                        this.switchToHex(row);

                        if (oldContent != newContent) {
                            row.classList.add("updated");
                        }

                        // Re-encode to the requested type
                        if (type == "f") {
                            this.switchToFloat(row);
                        }
                        else if (type == "d") {
                            this.switchToDouble(row);
                        }
                    }
                });
            }
        });
    }

    selectInput(val_td) {
        val_td.setAttribute('hidden', '');
        val_td.nextElementSibling.removeAttribute('hidden');
        let input = val_td.nextElementSibling.querySelector('input');
        input.focus();
        input.value = val_td.firstElementChild.textContent;
        input.select();
    }

    submitInput(input) {
        let valid = true;
        try {
            // TODO: interpret float/double when in that mode
            // Make sure that input.value can be interpreted as a BigInt
            BigInt(input.value);
        }
        catch(err) {
            valid = false;
        }
        finally {
            let td = input.parentNode;
            let row = td.parentNode;

            td.setAttribute('hidden', '');
            td.previousElementSibling.removeAttribute('hidden');

            if (valid) {
                let hexValue = "0x" + BigInt.asUintN(64, BigInt(input.value)).toString(16).padStart(16, '0');
                row.setAttribute("data-hex-value", hexValue);
                td.previousElementSibling.firstElementChild.textContent = hexValue;

                let reg = row.__register;
                reg.value = BigInt(hexValue);

                this.trigger('change', reg);
            }
        }
    }

    switchToHex(row) {
        let typeCell = row.querySelector("td.type");
        if (typeCell) {
            typeCell.textContent = "x";
        }

        let dataButton = row.querySelector("td.value button");

        if (!row.hasAttribute("data-hex-value")) {
            row.setAttribute("data-hex-value", dataButton.textContent);
        }

        dataButton.textContent = row.getAttribute("data-hex-value");

        // Enable all dropdown entries
        row.querySelectorAll("button[data-action^=\"as-\"]").forEach( (item) => {
            item.removeAttribute("disabled");
        });

        // Disable as-hex
        let asHexItem = row.querySelector("button[data-action=\"as-hex\"]");
        if (asHexItem) {
            asHexItem.setAttribute("disabled", "");
        }
    }

    switchToFloat(row) {
        // Switch to Hex first
        this.switchToHex(row);

        // Now switch to float
        let typeCell = row.querySelector("td.type");
        typeCell.textContent = "f";

        let value = BigInt(row.getAttribute("data-hex-value"));
        let buffer = new Uint8Array(8);
        let view = new DataView(buffer.buffer);
        view.setBigUint64(0, value);
        value = view.getFloat32(4).toString();
        if (value.indexOf(".") == -1 && value != "NaN") {
            value = value + ".0";
        }

        let dataButton = row.querySelector("td.value button");
        dataButton.textContent = value;

        // Enable all dropdown entries
        row.querySelectorAll("button[data-action^=\"as-\"]").forEach( (item) => {
            item.removeAttribute("disabled");
        });

        // Disable as-float
        row.querySelector("button[data-action=\"as-float\"]").setAttribute("disabled", "");
    }

    switchToDouble(row) {
        // Switch to Hex first
        this.switchToHex(row);

        // Now switch to double
        let typeCell = row.querySelector("td.type");
        typeCell.textContent = "d";

        let value = BigInt(row.getAttribute("data-hex-value"));
        let buffer = new Uint8Array(8);
        let view = new DataView(buffer.buffer);
        view.setBigUint64(0, value);
        value = view.getFloat64(0).toString();
        if (value.indexOf(".") == -1 && value != "NaN") {
            value = value + ".0";
        }

        let dataButton = row.querySelector("td.value button");
        dataButton.textContent = value;

        // Enable all dropdown entries
        row.querySelectorAll("button[data-action^=\"as-\"]").forEach( (item) => {
            item.removeAttribute("disabled");
        });

        // Disable as-double
        row.querySelector("button[data-action=\"as-double\"]").setAttribute("disabled", "");
    }

    bindEvents() {
    }

    bindRowEvents(row) {
        let tableCells = row.querySelectorAll("td.value");
        tableCells.forEach( (td) => {
            td.firstElementChild.addEventListener("click", this.selectInput.bind(this, td));

            // Bind dropdown events
            let actionButton = td.parentNode.querySelector("button.actions");
            if (actionButton) {
                // Create the dropdown menu
                let dropdown = new Dropdown(actionButton);

                dropdown.on('click', (event) => {
                    if (event == "as-hex") {
                        this.switchToHex(td.parentNode);
                    }
                    else if (event == "as-float") {
                        this.switchToFloat(td.parentNode);
                    }
                    else if (event == "as-double") {
                        this.switchToDouble(td.parentNode);
                    }
                    else if (event == "explore") {
                        let floatExplorer = new FloatExplorerDialog();
                        floatExplorer.update(BigInt(td.parentNode.getAttribute('data-hex-value')));

                        let typeCell = td.parentNode.querySelector("td.type");
                        if (typeCell && typeCell.textContent == "f") {
                            floatExplorer.view32();
                        }
                        else if (typeCell && typeCell.textContent == "d") {
                            floatExplorer.view64();
                        }
                    }
                });
            }
        });

        let inputCells = row.querySelectorAll("td.edit input");
        inputCells.forEach( (input) => {
            input.addEventListener("blur", this.submitInput.bind(this, input));
            input.addEventListener("keydown", (keyEvent) => {
                if(keyEvent.key === "Enter") {
                    this.submitInput(input);
                }
            });
        });
    }
}
