// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Editor } from '../editor.js';

/**
 * Represents a text editor widget for text file types.
 */
export class TextEditor extends Editor {
    /**
     * Constructs the editor instance at the given element to create the
     * `textarea`.
     *
     * This will convert the `<textarea>` tag it creates to the sophisticated
     * editor instance.
     *
     * @param {HTMLElement} element - The element containing the editor.
     * @param {object} options - The options for the editor.
     */
    constructor(element, options) {
        super(element, options);

        // Create the textarea we will turn into an editor.
        let textarea = element.querySelector('textarea');
        TextEditor.__count ||= 1;
        TextEditor.__count++;
        textarea.setAttribute('id', `text-editor-${TextEditor.__count.toString(16)}`);

        // Create an Ace Editor instance
        this._editor = window.ace.edit(textarea.getAttribute('id'));

        // Set theme
        this._editor.setTheme("ace/theme/monokai");

        // Add the editor icon to its place
        let icon = document.body.querySelector("svg.editor-icon[hidden]").cloneNode(true);
        let scroller = this.element.querySelector(".ace_scroller");
        if (scroller && icon) {
            icon.removeAttribute('hidden');
            icon.removeAttribute('aria-hidden');
            let content = scroller.querySelector(".ace_content");

            if (content) {
                scroller.insertBefore(icon, content);
            }
        }

        // Add the margins
        this._editor.setShowPrintMargin(false);

        // Have gutter (line numbers) be as wide as it needs to be instead of
        // getting wider as you hit line 100, 1000, etc.
        this._editor.setOption('fixedWidthGutter', true);

        // Enable autocomplete
        this._editor.setOptions({
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
        });

        this._editor.getSession().on('change', () => {
            this.trigger('change');
        });

        // Set the default 'mode' for highlighting
        this._editor.session.setMode("ace/mode/assembly_riscv");

        // Assign the completer
        var langTools = window.ace.require("ace/ext/language_tools");
        let completer = {
            getCompletions: (editor, session, pos, prefix, callback) => {
                if (this.options.onCompletion) {
                    let line = session.getLine(pos.row);
                    let linePrefix = line.substring(0, pos.column - prefix.length).trim();
                    var wordRange = session.getWordRange(pos.row, pos.column);
                    var word = session.getTextRange(wordRange);
                    var token = session.getTokenAt(pos.row, pos.column);
                    let kind = "comment";
                    if (token) {
                        kind = token.type;
                    }

                    callback(null, this.options.onCompletion(this, line, linePrefix, word, kind, pos.row, pos.column));
                }
                else {
                    callback(null, []);
                }
            },
            getDocTooltip: (item) => {}
        };

        // Set the completer to our completer
        this._editor.completers = [completer];

        // Now, we look at supporting instruction 'popovers'
        // Hook into the mousemove event to track the mouse position:
        this._editor.on("mousemove", (event) => {
            // Get the position within the document
            var pos = event.getDocumentPosition();

            // Get the word at this position
            var wordRange = this._editor.session.getWordRange(pos.row, pos.column);
            var word = this._editor.session.getTextRange(wordRange);
            let line = this._editor.session.getLine(pos.row);
            var token = this._editor.session.getTokenAt(pos.row, pos.column);
            let kind = "comment";
            if (token) {
                kind = token.type;
            }

            // Get word screen position (relative to top-left of editor)
            var startPoint = this._editor.renderer.$cursorLayer.getPixelPosition(
                wordRange.start, false // Ace does not seem to use this boolean.
            );                         // We will keep it false just in case.
            var endPoint = this._editor.renderer.$cursorLayer.getPixelPosition(
                wordRange.end, false
            );

            // Get editor position
            let editorNode = document.querySelector("pre.ace_editor");
            let editorBounds = editorNode.getBoundingClientRect();
            let left = startPoint.left + editorBounds.x;
            let top = startPoint.top + editorBounds.y;
            let end = endPoint.left + editorBounds.x;

            // We need to account for the gutter
            left += this._editor.renderer.gutterWidth;
            end += this._editor.renderer.gutterWidth;

            // We need to account for the editor scroll
            top -= this._editor.renderer.scrollTop;

            // We need to account for instructions that appear at the end of the line (like ecall)
            // which will popover as long as the mouse moves over the same row
            if (end < event.x) {
                word = "";
            }

            if (this.options.onMouseover) {
                if (!this._last || this._last[0] !== line || this._last[1] !== word) {
                    this._last = [line, word, kind];
                    this.options.onMouseover(this, line, word, kind, left, top);
                }
            }
        });

        // When the mouse leaves the editor, hide the tooltip
        this._editor.container.addEventListener("mouseout", this.hideTooltip.bind(this));

        // Inserts any new labels the user added when the cursor changes lines
        this._editor.session.selection.on('changeCursor', (e) => {
            // Get the properties of this cursor
            let pos = this._editor.getCursorPosition();
            let line = this._editor.session.getLine(pos.row);
            var wordRange = this._editor.session.getWordRange(pos.row, pos.column);
            var word = this._editor.session.getTextRange(wordRange);
            var token = this._editor.session.getTokenAt(pos.row, pos.column);
            let kind = "comment";
            if (token) {
                kind = token.type;
            }

            let info = {
                row: pos.row,
                column: pos.column,
                line: line,
                word: word,
                kind: kind
            };

            if (!this._lastCursorInfo) {
                this._lastCursorInfo = info;
            }

            if (this.options.onPosition) {
                this.options.onPosition(this, pos.row, pos.column, line, word, kind, this._lastCursorInfo);
            }

            this._lastCursorInfo = info;
        });
    }

    static get view() {
        return "text-editor";
    }

    /**
     * Retrieves the text in the current document.
     */
    get value() {
        return this._editor.getValue();
    }

    /**
     * Retrieves the current annotations found within the current document.
     */
    get annotations() {
        return this._editor.getSession().getAnnotations();
    }

    /**
     * Sets the annotations that will appear in the current document.
     */
    set annotations(value) {
        this._editor.getSession().setAnnotations(value);

        if (!value) {
            this._editor.getSession().clearAnnotations();
        }
    }

    /**
     * Loads the given text and sets the filename for the current document.
     *
     * When `type` is specified and the type contains a '/', the first part of
     * the type preceding the slash is the editor mode. This indicates the type
     * of syntax. For instance: 'assembly_x86/text' would highlight in the x86
     * assembly mode.
     *
     * During load, the `change` event does not fire.
     *
     * @param {string} data The content to load into the editor.
     * @param {string} filename The name of the file being loaded.
     * @param {string} type An optional type indicating the file content.
     */
    load(data, filename = 'input.s', type = '') {
        // Set the data source
        if (data instanceof Uint8Array) {
            data = (new TextDecoder('utf-8')).decode(data);
        }

        this.__loading = true;
        this.readonly = true;

        // Get the mode from the defaults or given type
        let mode = null;
        if ((type || '').indexOf('/') != -1) {
            mode = type.split('/')[0];
        }
        if (!mode) {
            if (filename.endsWith(".s")) {
                // Set the 'mode' for highlighting
                mode = "assembly_x86";
            }
            else if (filename.endsWith(".c") || filename.endsWith(".cc") || filename.endsWith("cpp")) {
                mode = "c_cpp";
            }
            else {
                mode = "c_cpp";
            }
        }

        // -1 moves the cursor to the start (without this,
        // it will select the entire text... I dunno)
        this._editor.setValue(data, -1);
        this._editor.getSession().setUndoManager(new window.ace.UndoManager());

        // Tell the target (and allow it to override the highlighting mode)
        if (this.options.onLoad) {
            this.options.onLoad(data, filename);
        }

        // Set the 'mode' for highlighting
        this._editor.session.setMode("ace/mode/" + mode);

        // Force recompute of the cursor info
        this._lastCursorInfo = null;

        this.__loading = false;
    }

    /**
     * Focus on the text editor.
     */
    focus() {
        this._editor.focus();
    }

    /**
     * Remove any focus on this text editor.
     */
    blur() {
        this._editor.blur();
    }

    /**
     * A helper to hide any displayed tooltip or popover.
     */
    hideTooltip() {
        let tooltipNode = Editor.tooltipNode;
        Editor.tooltipNode = null;
        if (tooltipNode && tooltipNode.parentNode) {
            tooltipNode.parentNode.removeChild(tooltipNode);
        }
    }

    /**
     * A helper to display the given html as a popover at the given location.
     *
     * @param {String} html - The HTML to add to the popover.
     * @param {number} x - The x position of the popover.
     * @param {number} y - The y position of the popover.
     */
    showTooltip(html, x, y) {
        if (!Editor.tooltipNode) {
            // Create a tooltip element
            Editor.tooltipNode = document.createElement("div");
            Editor.tooltipNode.classList.add("ace_tooltip");
            Editor.tooltipNode.classList.add("ace_doc-tooltip");
            Editor.tooltipNode.style.margin = 0;
            Editor.tooltipNode.style.pointerEvents = "none";
            Editor.tooltipNode.tabIndex = -1;
        }

        let tooltipNode = Editor.tooltipNode;
        tooltipNode.innerHTML = html;

        if (!tooltipNode.parentNode) {
            document.body.appendChild(tooltipNode);
        }

        // Position the node
        tooltipNode.style.bottom = (window.innerHeight - y) + 'px';
        tooltipNode.style.left = x + 'px';

        // Display the node
        tooltipNode.style.display = "block";
    }
}
