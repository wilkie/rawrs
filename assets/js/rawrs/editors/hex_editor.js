// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Util } from '../util.js';

import { Editor } from '../editor.js';

/**
 * Represents a hex editor widget for binary file types.
 */
export class HexEditor extends Editor {
    constructor(element, options) {
        // Goals:
        // We want a gutter area for the address.
        // We want a hex view for the data.
        // We want an ASCII view for printed characters.
        // We want it to more-or-less resemble the Ace editor interface.
        // Therefore, we base it off the Ace editor DOM class structure.
        super(element, options);

        this._hoveredClasses = [
            'ace_support', 'ace_function', 'ace_directive', 'ace_assembly'
        ];

        // We want to make sure Ace loads its stuff
        // Create the textarea we will turn into an editor.
        let textarea = element.querySelector('textarea.hex-editor-fake-textarea');
        HexEditor.__count ||= 1;
        HexEditor.__count++;
        textarea.setAttribute('id', `text-editor-${HexEditor.__count.toString(16)}`);
        this._editor = window.ace.edit(textarea.getAttribute('id'));
        this._editor.setTheme("ace/theme/monokai");
        textarea.style.opacity = '0';

        let fakeRegion = element.querySelector('.hex-editor-fake-region');
        fakeRegion.remove();

        textarea = element.querySelector('textarea.hex-editor-data-textarea');
        HexEditor.__count++;
        textarea.setAttribute('id', `text-editor-${HexEditor.__count.toString(16)}`);
        textarea.setAttribute('tabindex', '1');
        this._dataTextarea = textarea;
        textarea.style.opacity = '0';
        textarea.style.position = 'absolute';
        textarea.style.width = '1px';
        textarea.style.height = '1px';

        textarea = element.querySelector('textarea.hex-editor-ascii-textarea');
        HexEditor.__count++;
        textarea.setAttribute('id', `text-editor-${HexEditor.__count.toString(16)}`);
        textarea.setAttribute('tabindex', '1');
        this._asciiTextarea = textarea;
        textarea.style.opacity = '0';
        textarea.style.position = 'absolute';
        textarea.style.width = '1px';
        textarea.style.height = '1px';

        this._mainElement = this.element.querySelector('pre.hex-editor');

        // Add icon
        let icon = document.body.querySelector("svg.editor-icon[hidden]").cloneNode(true);
        this._scroller = this.element.querySelector(".ace_scroller");
        if (this._scroller && icon) {
            icon.removeAttribute('hidden');
            icon.removeAttribute('aria-hidden');
            let content = this._scroller.querySelector(".ace_content");

            if (content) {
                this._scroller.insertBefore(icon, content);
            }
        }

        // Set margins
        this._marginWidth = 5;

        this._gutter = element.querySelector('.hex-editor-gutter');
        this._gutterLayer = element.querySelector('.hex-editor-gutter > .ace_layer');

        this._data = element.querySelector('.hex-editor-data');
        this._dataLayer = this._data.querySelector('.ace_text-layer');
        this._dataCursorLayer = this._data.querySelector('.ace_cursor-layer');
        this._ascii = element.querySelector('.hex-editor-ascii');
        this._asciiLayer = this._ascii.querySelector('.ace_text-layer');
        this._asciiCursorLayer = this._ascii.querySelector('.ace_cursor-layer');

        this._scrollbar = element.querySelector('.hex-editor-scrollbar');
        this._scrollbarInner = this._scrollbar.querySelector('.ace_scrollbar-inner');

        // Measure the font
        this._charMetrics = Util.getFontMetrics(this._gutterLayer);
        this._charDimensions = this._charMetrics.measureCharacter('X');

        // Page top-left position
        this._address = 0;

        // Cursor
        this._cursor = 0;

        // Create a resize observer
        this._observer = new window.ResizeObserver( (e) => {
            this.setPosition(0, 0);
        });
        this._observer.observe(this.element);

        // Bind events
        this.bindEvents();
    }

    static get view() {
        return "hex-editor";
    }

    setPosition(x, y) {
        // Reposition cursor
        let address = 0;
        this._address = address;

        // Draw
        this.draw();
    }

    draw() {
        // Calculate the widths of each section (gutter, data, ascii)
        // based on the amount of data and the current size of the panel.
        this._charWidth = this._charDimensions.width;
        let gutterWidth = (this._charWidth * 8) + 33;
        let remainingWidth = this.element.clientWidth - (this._marginWidth * 2) - gutterWidth;

        this._scroller.style.width = `${this.element.clientWidth - gutterWidth}px`;
        this._scroller.style.top = '0px';
        this._scroller.style.left = `${gutterWidth}px`;
        this._scroller.style.height = '100%';

        // The ASCII section contains a character for each two characters left of it
        // The spacing between characters is just easier when considering it a whole
        // width of a character. Therefore, each line must accommodate a multiple of
        // four characters: 3 for two hex digits and space, and then 1 for the ASCII
        // representation.
        this._asciiWidth = remainingWidth / 4;
        this._dataWidth = this._asciiWidth * 3;

        // Render the content here
        this._lineCount = Math.floor(this._dataWidth / (this._charWidth * 3))
        this._linesCount = Math.ceil(this._source.byteLength / this._lineCount);

        let tempCell = document.createElement('div');
        tempCell.classList.add('ace_gutter-cell');
        tempCell.innerText = "0";
        this._gutterLayer.appendChild(tempCell);
        this._lineHeight = tempCell.clientHeight;
        let totalHeight = this._lineHeight * this._linesCount;
        tempCell.remove();

        if (totalHeight <= this.element.clientHeight) {
            // Hide scrollbar
            this._scrollbar.style.visibility = 'hidden';
        }
        else {
            // Show scrollbar
            this._scrollbar.style.visibility = '';

            // Resize again
            remainingWidth -= this._scrollbar.offsetWidth;
            this._asciiWidth = remainingWidth / 4;
            this._dataWidth = this._asciiWidth * 3;
            this._lineCount = Math.floor(this._dataWidth / (this._charWidth * 3))
            this._linesCount = Math.ceil(this._source.byteLength / this._lineCount);
            totalHeight = this._lineHeight * this._linesCount;
            this._scrollbarInner.style.height = `${totalHeight}px`;
            this._scrollbarInner.style.width = '1px';
            this._scrollIgnore = true;
            this._scrollbar.scrollTop = (this._address / this._lineCount) * this._lineHeight;
            this._scrollIgnore = false;
        }

        // Set the dimensions of each span element
        this._tupleWidth = this._charWidth * 3;

        // Resize elements
        this._data.style.width = `${this._dataWidth}px`;
        this._data.style.height = '100%';
        this._data.style.left = `${this._marginWidth}px`;
        this._data.style.top = '0px';
        this._dataLayer.style.top = '0px';
        this._dataLayer.style.height = '100%';
        this._dataCursorLayer.style.top = '0px';
        this._ascii.style.width = `${this._asciiWidth}px`;
        this._ascii.style.height = '100%';
        this._ascii.style.left = `${(this._marginWidth * 2) + this._dataWidth}px`;
        this._ascii.style.top = '0px';
        this._asciiLayer.style.top = '0px';
        this._asciiLayer.style.height = '100%';
        this._asciiCursorLayer.style.top = '0px';

        // Set the gutter layer positioning.
        this._gutterLayer.style.top = '0';
        this._gutterLayer.style.left = '0';
        this._gutterLayer.style.width = `${gutterWidth}px`;
        this._gutter.style.width = `${gutterWidth}px`;
        this._gutter.style.left = '0';
        this._gutterLayer.style.height = '1000000px';

        // Do not redraw if the view is already current
        let drawKey = `${this._address}-${this._lineCount}-${totalHeight}`;
        if (this._drawKey == drawKey) {
            return;
        }
        this._drawKey = drawKey;

        // Get the size key, so we throw away lines that are no longer valie
        // since they are too long or too short.
        let sizeKey = `${this._lineCount}-${totalHeight}`;

        // Destroy the gutter
        this._gutterLayer.innerHTML = "";

        let top = 0;
        let address = this._address;
        this._linesVisible = 0;
        for (let i = 0; i < this._source.byteLength; i += this._lineCount) {
            let arr = Array.from(this._source.slice(address, address + this._lineCount));

            // Print the numbers to the gutter layer.
            let lineAddress = this.formAddress(address);
            let cell = document.createElement('div');
            cell.classList.add('ace_gutter-cell');
            cell.style.top = `${top}px`;
            cell.innerText = lineAddress;
            this._gutterLayer.appendChild(cell);

            let lineElement = this._dataLayer.querySelector(`.ace_line[data-byte="${lineAddress}"][data-size="${sizeKey}"]`);
            if (!lineElement) {
                lineElement = document.createElement('div');
                lineElement.classList.add('ace_line');
                lineElement.setAttribute('data-byte', lineAddress);

                let line = arr.map( (byte) => {
                    return byte.toString(16).padStart(2, '0');
                }).join(' ');
                lineElement.textContent = line;

                this._dataLayer.appendChild(lineElement);
                lineElement.style.width = `${this._dataWidth}px`;
                lineElement.style.height = 'auto';
            }

            // Establish top
            lineElement.style.top = `${top}px`;

            // Mark as good
            lineElement.setAttribute('data-frame', this._drawKey);
            lineElement.setAttribute('data-size', sizeKey);

            let asciiElement = this._asciiLayer.querySelector(`.ace_line[data-byte="${lineAddress}"][data-size="${sizeKey}"]`);
            if (!asciiElement) {
                asciiElement = document.createElement('div');
                asciiElement.classList.add('ace_line');
                asciiElement.setAttribute('data-byte', lineAddress);

                let printed = arr.map( (byte) => {
                    var chr = String.fromCharCode(byte);
                    // A tricky way to decide if a character is "printable"
                    // Probably gonna have some edge cases that are missed
                    var uri = encodeURI(chr);
                    if (uri.length == 1 || uri === "%25" ||
                                           uri === "%3C" ||
                                           uri === "%3E" ||
                                           uri === "%22" ||
                                           uri === "%20" ||
                                           uri === "%5E" ||
                                           uri === "%60" ||
                                           uri === "%5B" ||
                                           uri === "%5D") {
                        // It is a proper character within a URI
                        // ... or it is a % or " " or ^ or ` or [ or ] (yikes)
                        return chr;
                    }
                    else {
                        return ".";
                    }
                }).join('');

                asciiElement.textContent = printed;
                this._asciiLayer.appendChild(asciiElement);
                asciiElement.style.height = 'auto';
            }

            asciiElement.style.top = `${top}px`;

            // Mark as good
            asciiElement.setAttribute('data-frame', this._drawKey);
            asciiElement.setAttribute('data-size', sizeKey);

            // Go to the next line
            address += this._lineCount;
            top += cell.clientHeight;

            // Unless we do not have a visible line
            if (top >= this.element.clientHeight) {
                break;
            }

            // Count this line (but only if it is fully visible)
            this._linesVisible++;
        }

        // Remove old lines
        let selector = `.ace_line:not([data-frame="${this._drawKey}"])`;
        [this._dataLayer, this._asciiLayer].forEach( (layer) => {
            layer.querySelectorAll(selector).forEach( (el) => {
                el.remove();
            });
        });

        // Position the cursor
        this.positionCursor();
    }

    positionCursor() {
        let address = this._cursor - this._address;
        let row = Math.floor(address / this._lineCount);
        let column = address % this._lineCount;

        // What is the next page's starting address?
        let nextAddress = this._address + (this._linesVisible * this._lineCount);

        [this._data, this._ascii].forEach( (el, i) => {
            // Get this panel's cursor.
            let cursor = el.querySelector('.ace_cursor');

            // Get position of the cursor.
            let left = (i == 0 ?  column * this._tupleWidth : column * this._charWidth);
            let top = row * this._lineHeight;

            // If the cursor is on the screen
            if (this._cursor >= this._address && this._cursor < nextAddress) {
                // Position hex octet cursor
                cursor.style.display = 'block';
                cursor.style.top = `${top}px`;
                cursor.style.left = `${left}px`;
                cursor.style.width = '6px';
                cursor.style.height = `${this._lineHeight}px`;
                cursor.style.animationDuration = '1000ms';

                if (i == 0) {
                    this._dataTextarea.style.left = `${left}px`;
                    this._dataTextarea.style.top = `${top}px`;
                }
                else {
                    this._asciiTextarea.style.left = `${left}px`;
                    this._asciiTextarea.style.top = `${top}px`;
                }
            }
            else {
                // Hide it otherwise
                cursor.style.display = 'none';
            }
        });
    }

    load(data, name, type) {
        // Set the data source
        if (!(data instanceof Uint8Array)) {
            data = (new TextEncoder('utf-8')).encode(data);
        }

        this._source = data;

        // Go to top-left
        this.setPosition(0, 0);
    }

    bindEvents() {
        this._mainElement.addEventListener('wheel', (event) => {
            let factor = 0.35;
            switch (event.deltaMode) {
                case event.DOM_DELTA_PIXEL:
                    event.wheelX = event.deltaX * factor || 0;
                    event.wheelY = event.deltaY * factor || 0;
                    break;
                case event.DOM_DELTA_LINE:
                case event.DOM_DELTA_PAGE:
                    event.wheelX = (event.deltaX || 0) * 5;
                    event.wheelY = (event.deltaY || 0) * 5;
                    break;
            }

            // See how many lines we should scroll
            let deltaY = Math.round(event.wheelY / this._lineHeight);
            this._address += deltaY * this._lineCount;
            this._address = Math.max(0, this._address);
            let maxAddress = this._source.byteLength - (this._source.byteLength % this._lineCount);
            maxAddress -= (this._lineCount * (this._linesVisible - 1));
            this._address = Math.min(this._address, maxAddress);

            // Redraw.
            this.draw();
        });

        this._scrollbar.addEventListener('scroll', (event) => {
            if (!this._scrollIgnore) {
                let top = this._scrollbar.scrollTop;
                let y = Math.floor(top / this._lineHeight);
                this._address = y * this._lineCount;
                this.draw();
            }
        });

        this._scroller.addEventListener('click', (event) => {
            if (event.target === this._data) {
                this._dataTextarea.focus();
            }
            else if (event.target === this._ascii) {
                this._asciiTextarea.focus();
            }

            if (this._hoveredTuple) {
                this._cursor = this._hoveredAddress;
                this.positionCursor();
            }
        });

        [this._dataTextarea, this._asciiTextarea].forEach( (ta) => {
            ta.addEventListener('focus', (event) => {
                this.focus();
            });

            ta.addEventListener('blur', (event) => {
                this.blur();
            });

            ta.addEventListener('keydown', (event) => {
                if (event.key === "ArrowUp") {
                    if (this._cursor >= this._lineCount) {
                        this._cursor -= this._lineCount;
                        this.positionCursor();
                    }
                }
                else if (event.key === "ArrowDown") {
                    if (this._cursor <= this._source.byteLength - this._lineCount) {
                        this._cursor += this._lineCount;
                        this.positionCursor();
                    }
                }
                else if (event.key === "ArrowLeft") {
                    if (this._cursor >= 1) {
                        this._cursor--;
                        this.positionCursor();
                    }
                }
                else if (event.key === "ArrowRight") {
                    if (this._cursor < this._source.byteLength - 1) {
                        this._cursor++;
                        this.positionCursor();
                    }
                }
            });
        });

        this._scroller.addEventListener('mouseleave', (event) => {
            // Undo any previous hovered styling
            if (this._hoveredTuple) {
                [this._hoveredTuple, this._hoveredCharacter].forEach( (el) => {
                    el.remove();
                });

                this._hoveredTuple = undefined;
                this._hoveredCharacter = undefined;
            }
        });

        this._scroller.addEventListener('mousemove', (event) => {
            // Undo any previous hovered styling
            if (this._hoveredTuple) {
                [this._hoveredTuple, this._hoveredCharacter].forEach( (el) => {
                    el.remove();
                });
            }

            // Determine the tuple or ascii character under the cursor.
            let x = event.offsetX;
            let y = event.offsetY;

            let row = Math.floor(y / this._lineHeight);
            let column = -1;

            if (event.target === this._data) {
                // We are over a hex octet
                column = Math.floor((x + (this._charWidth / 2)) / this._tupleWidth);
            }
            else {
                // We are over a printed character
                column = Math.floor(x / this._charWidth);
            }

            let address = this.formAddress(this._address + (row * this._lineCount));
            let line = this._dataLayer.querySelector(`.ace_line[data-byte="${address}"]`);
            let printedLine = this._asciiLayer.querySelector(`.ace_line[data-byte="${address}"]`);

            if (line && printedLine) {
                address = this.formAddress(this._address + (row * this._lineCount) + column);
                this._hoveredAddress = parseInt(address, 16);

                // Create a hovered item
                this._hoveredTuple = document.createElement('div');
                this._hoveredTuple.style.position = 'absolute';
                this._hoveredTuple.style.left = `${column * (this._charWidth * 3)}px`;
                this._hoveredTuple.style.top = `${row * this._lineHeight}px`;
                this._hoveredTuple.style.width = `${this._charWidth * 2}px`;
                this._hoveredTuple.style.height = `${this._lineHeight}px`;
                this._hoveredTuple.textContent = line.textContent.substring(column * 3, (column * 3) + 2);
                this._dataLayer.append(this._hoveredTuple);

                this._hoveredCharacter = document.createElement('div');
                this._hoveredCharacter.style.position = 'absolute';
                this._hoveredCharacter.style.left = `${column * this._charWidth}px`;
                this._hoveredCharacter.style.top = `${row * this._lineHeight}px`;
                this._hoveredCharacter.style.width = `${this._charWidth}px`;
                this._hoveredCharacter.style.height = `${this._lineHeight}px`;
                this._hoveredCharacter.textContent = printedLine.textContent.substring(column, column + 1);
                this._asciiLayer.append(this._hoveredCharacter);

                let mainStyles = document.defaultView.getComputedStyle(this._scroller, "");
                [this._hoveredTuple, this._hoveredCharacter].forEach( (el) => {
                    let color = mainStyles.backgroundColor;
                    if (color.endsWith(', 0)')) {
                        color = color.slice(0, color.length - 4) + ', 1)';
                    }
                    el.style.color = color;
                    el.style.background = mainStyles.color;
                });
            }
        });
    }

    formAddress(address) {
        return address.toString(16).padStart(8, '0');
    }

    focus() {
        if (document.activeElement === this._dataTextarea) {
            this._dataCursorLayer.classList.remove('ace_hidden-cursors');
            this._dataCursorLayer.classList.add('ace_animate-blinking');
        }
        else {
            this._dataCursorLayer.classList.add('ace_hidden-cursors');
            this._dataCursorLayer.classList.remove('ace_animate-blinking');
        }

        if (document.activeElement === this._asciiTextarea) {
            this._asciiCursorLayer.classList.remove('ace_hidden-cursors');
            this._asciiCursorLayer.classList.add('ace_animate-blinking');
        }
        else {
            this._asciiCursorLayer.classList.add('ace_hidden-cursors');
            this._asciiCursorLayer.classList.remove('ace_animate-blinking');
        }
    }

    blur() {
        this._asciiCursorLayer.classList.add('ace_hidden-cursors');
        this._dataCursorLayer.classList.remove('ace_animate-blinking');
    }
}
