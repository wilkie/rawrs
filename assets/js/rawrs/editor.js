// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from './event_component.js';

/**
 * This represents the text editor component for the user interface.
 */
export class Editor extends EventComponent {
    constructor(element, options) {
        super();

        this.__element = element;

        // By default, we are loading.
        this.__loading = true;

        // By default, we are readonly.
        this.__readonly = true;

        // Retain options block.
        this.__options = options || {};
    }

    /**
     * Returns the class that specifies the editor HTML template.
     */
    static get view() {
        throw new Error("editor must specify a view");
    }

    /**
     * Returns a reference to the options block passed to the editor.
     *
     * Commonly, a Target can assign an Editor to a type of file and with that
     * pass unique options for that file type. These can include event handlers
     * for different events that the editor might care about.
     */
    get options() {
        return this.__options;
    }

    /**
     * The element this editor is rooted against.
     *
     * This is typically the tab panel for the file viewers.
     */
    get element() {
        return this.__element;
    }

    /**
     * Returns true when content is being loaded.
     */
    get loading() {
        return this.__loading;
    }

    /**
     * Updates the editor to be readonly or not.
     */
    set readonly(value) {
        this.__readonly = value;
    }

    /**
     * Returns true when the content is read-only.
     */
    get readonly() {
        return this.__readonly;
    }

    /**
     * Retrieves the current annotations found within the current document.
     */
    get annotations() {
        return [];
    }

    /**
     * Sets the annotations that will appear in the current document.
     */
    set annotations(value) {
        // Do nothing with them by default
    }

    /**
     * Retrieves the current target that is being used.
     */
    get target() {
        return this.__target;
    }

    /**
     * Sets the current target for the document.
     */
    set target(value) {
        this.__target = value;
    }

    /**
     * Generic function to load data.
     *
     * @param {string|Uint8Array} data The file data.
     * @param {string} name The file name.
     * @param {string} type The file type.
     */
    load(data, name, type) {
        // Throw error when not overridden.
        throw 'unimplemented';
    }
}
