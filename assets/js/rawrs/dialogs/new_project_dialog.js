// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Dialog } from '../ui/dialog';

import { Util } from '../util';

/**
 * Represents the file copy dialog.
 */
export class NewProjectDialog extends Dialog {
    constructor(target) {
        super('dialog-new-project');

        this._target = target;
        this._projects = target.projects;

        this.open();

        this._cancelButton = this.dialog.querySelector("#button-cancel-new-project");
        this._createButton = this.dialog.querySelector("#button-complete-new-project");
        this._createButton.setAttribute('disabled', '');

        this._nameInput = this.dialog.querySelector("input.project-name");
        this._templateInput = this.dialog.querySelector("select");

        // Create dropdown content
        this.loadProjectTemplates();

        // Bind the specific events for the dialog
        this.bindEvents();

        // Focus on the input field for the name
        window.requestAnimationFrame( () => {
            this._nameInput.focus();
            this._nameInput.setSelectionRange(0, 8);
        });
    }

    loadProjectTemplates() {
        this._templateInput.innerHTML = '';

        let optionTemplate = this.dialog.querySelector('template.project-option');

        this._projects.forEach( (project) => {
            let option = Util.createElementFromTemplate(optionTemplate);
            option.setAttribute('value', project);
            option.textContent = project;
            this._templateInput.appendChild(option);
        });
    }

    /**
     * Closes the dialog.
     */
    close() {
        super.close();
    }

    /**
     * Creates the project, if possible.
     */
    async create() {
        this._createButton.setAttribute('disabled', '');
        let name = this._nameInput.value;
        let project = this._templateInput.value;
        await this.trigger('create', {
            project: {
                name: project,
                files: this._target.projectFor(project)
            },
            name: name
        });
        this.close();
    }

    /**
     * Binds events to the dialog to handle the file copy and close buttons.
     *
     * This is called internally when the dialog is created.
     */
    bindEvents() {
        // Bail if this was called before
        if (this.dialog.classList.contains("bound")) {
            return;
        }

        // Do not allow the events to be bound twice
        this.dialog.classList.add("bound");

        let changeEvent = (event) => {
            if (this._nameInput.value.trim() !== '') {
                this._createButton.removeAttribute('disabled');
            }
            else {
                this._createButton.setAttribute('disabled', '');
            }
        };

        this._nameInput.addEventListener("keyup", changeEvent);
        this._nameInput.addEventListener("input", changeEvent);
        this._nameInput.addEventListener("change", changeEvent);

        this._cancelButton.addEventListener("click", this.close.bind(this));
        this._createButton.addEventListener("click", this.create.bind(this));
    }
}
