// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Dialog } from '../ui/dialog';

/**
 * Represents a yes/cancel dialog for confirmations.
 */
export class ConfirmDialog extends Dialog {
    constructor() {
        super('dialog-confirm');

        this._cancelButton = this.dialog.querySelector("#button-cancel-confirm");
        this._confirmButton = this.dialog.querySelector("#button-complete-confirm");

        this.open();

        // Bind the specific events for the dialog
        this.bindEvents();
    }

    /**
     * Binds events to the dialog to handle yes/cancel buttons.
     *
     * This is called internally when the dialog is created.
     */
    bindEvents() {
        // Bail if this was called before
        if (this.dialog.classList.contains("bound")) {
            return;
        }

        // Do not allow the events to be bound twice
        this.dialog.classList.add("bound");

        this._cancelButton.addEventListener("click", this.close.bind(this));
        this._confirmButton.addEventListener("click", async () => {
            await this.trigger('confirm', this);
            this.close();
        });
    }
}
