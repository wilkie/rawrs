// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Compiler } from '../../processes/compiler';

import { LCC } from './lcc';

export class Z80GBCCompiler extends Compiler {
    /**
     * Retrieves the options.
     */
    static get options() {
        return {
            // cache the .o files
            cache: (name) => {
                return name.substring(0, name.length - 2) + ".o"
            },

            // give us ALL .c files
            upload: /\.c$/
        };
    }

    perform(resolve, reject) {
        if (!this._lcc) {
            this._lcc = new LCC(this);
        }

        try {
            (new Promise( async (innerResolve, innerReject) => {
                let ret = await this._lcc._perform();
                innerResolve(ret);
            })).then( (binary) => {
                resolve(binary);
            }).catch( (e) => {
                reject(e);
            });
        }
        catch (e) {
            reject(e);
        }
    }
}
