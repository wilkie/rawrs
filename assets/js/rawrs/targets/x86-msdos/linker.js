// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Linker } from '../../processes/linker';
import { GCC } from './gcc.js';

export class X86MSDOSLinker extends Linker {
    /**
     * Retrieves the host triple for this target.
     */
    get targetTriple() {
        return "i586-pc-msdosdjgpp";
    }

    /**
     * Retrieves the options.
     */
    static get options() {
        return {
            upload: /\.ld$/,  // Give us any linker scripts
            notify: /\.c$/,   // Tell the linker what C files exist, if any
            aggregate: /\.o$/ // pass ALL .o files (second stage)
        };
    }

    _perform(resolve, reject) {
        let linkerScriptFile = null;

        if (this.seen.length == 0) {
            // Use the default linker script
            var linkerScript = "SECTIONS { . = 0x00400000; .text : { *(.text) } . = 0x10010000; .data : { *(.data) } } ENTRY (main)";
            linkerScript = linkerScript + "\n";
            var fileData = new Blob([linkerScript], {'type': 'text/plain'});

            var blobs = [{
              name: ".linker.ld",
              data: fileData
            }];

            linkerScriptFile = "/input/.linker.ld";
        }

        if (this.files[0]) {
            // Linker script file is here instead
            linkerScriptFile = "/input/" + this.files[0].name;
        }

        let mounts = [
            {
                type: "WORKERFS",
                opts: {
                    blobs: blobs
                },
                "mountpoint": "/input"
            }
        ];

        let workerURL = `${this.basepath}js/targets/msdos/binutils/${this.targetTriple}-ld.js`;

        // Pass along the linker script, if any
        if (linkerScriptFile) {
            args = args.concat(["-T", linkerScriptFile]);
        }
        args = args.concat(["-o", basename + ".elf", "-g"]);

        let projectName = this.project;
        let objects = this.working;

        // Initially, there are no errors
        this._errors = [];

        var worker = new Worker(workerURL);

        var files = objects;
        let args = objects.map( (info) => info.name ).sort();
        let basename = projectName;

        console.log("args", args);

        worker.onmessage = (e) => {
            var msg = e.data;

            switch(msg.type) {
                case "ready":
                    worker.postMessage({
                        type: "run",
                        MEMFS: files,
                        mounts: mounts,
                        ENV: {
                            "TMPDIR": "/work",
                            "C_INCLUDE_PATH": `/usr/${this.targetTriple}/sys-include`,
                            "CPLUS_INCLUDE_PATH": `/usr/${this.targetTriple}/sys-include`,
                            "OBJC_INCLUDE_PATH": `/usr/${this.targetTriple}/sys-include`,
                            "LIBRARY_PATH": `/usr/${this.targetTriple}/lib`
                        },
                        arguments: args
                    });
                    break;
                case "stdout":
                    this.terminal.writeln(msg.data);
                    break;
                case "stderr":
                    // Remove "/input/" from prececding the message data
                    if (msg.data.startsWith("/input/")) {
                        msg.data = msg.data.substring("/input/".length);
                    }

                    // Replace the name it *thinks* it is with the actual name
                    if (msg.data.startsWith("./this.program")) {
                        msg.data = msg.data.replace("./this.program", "ld");
                    }

                    // Ignore some strange errors emscripten reports sometimes
                    if (msg.data.indexOf("warning: unsupported syscall:") >= 0) {
                        break;
                    }

                    // Check for error statements
                    var matches = X86MSDOSLinker.ERROR_REGEX.exec(msg.data);
                    if (matches) {
                        var error = {
                            file: matches[1],
                            row: parseInt(matches[2]) - 1,
                            column: 0,
                            type: 'error',
                            text: matches[3]
                        };
                        this._errors.push(error);
                        this.trigger('error', error);
                    }

                    this.terminal.writeln(msg.data);
                    break;
                case "exit":
                    break;
                case "done":
                    if (msg.data.MEMFS[0] && this._errors.length == 0) {
                        this.terminal.write("Linking successful.");
                    }
                    else {
                        this.terminal.writeln("");
                        this.terminal.write("Linking failed.");
                    }

                    this.trigger('done');
                    if (msg.data.MEMFS[0] && this._errors.length == 0) {
                        resolve(msg.data.MEMFS[0]);
                    }
                    else {
                        resolve();
                    }
                    worker.terminate();
                    break;
                default:
                    break;
            }
        };
    }

    perform(resolve, reject) {
        var libc = true;

        if (this.seen.length == 0) {
            // We saw no C files... so, we are merely assembling
            libc = false;
        }

        console.log("WITH LIB C PLEASE");
        if (libc) {
            if (!this._gcc) {
                this._gcc = new GCC(this);
            }

            try {
                (new Promise( async (innerResolve, innerReject) => {
                    let ret = await this._gcc._perform();
                    innerResolve(ret);
                })).then( (binary) => {
                    console.log("resolving finally", binary);
                    resolve(binary);
                });
            }
            catch (e) {
                reject(e);
            }
        }
        else {
            this._perform(resolve, reject);
        }
    }
}

X86MSDOSLinker.ERROR_REGEX = /^(\S+):(\d+):\s+(.+)$/;
