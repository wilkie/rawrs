## Related Links

* [How to build DOS COM files with GCC](https://nullprogram.com/blog/2014/12/09/) - The linker scripts and arguments to produce 386/486 era (ish) 32-bit COM files. (should probably switch the operand/address modes in the init function tho... maybe `-Os` does this somehow)
