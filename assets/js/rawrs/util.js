// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

export class Util {
    /**
     * This function retrieves the query value from the given url.
     *
     * @param {string} name The query key to look for.
     * @param {string} [url] The URL to parse. If not given, then it will use
     *                       the current location.
     * @returns {string} If found, the value for the given key is given.
     */
    static getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /**
     * Hashes the given string.
     */
    static hash(str) {
        return btoa(str).replaceAll('=', '');
    }

    /*
     * Internal function to convert various JS arrays into a Uint8Array.
     */
    static toU8(data) {
        if (typeof data === 'string' || data instanceof String) {
            data = data.split("").map( (c) => c.charCodeAt(0) );
        }

        if (Array.isArray(data) || data instanceof ArrayBuffer) {
            data = new Uint8Array(data);
        }
        else if (!data) {
            // `null` for empty files.
            data = new Uint8Array(0);
        }
        else if (!(data instanceof Uint8Array)) {
            // Avoid unnecessary copying.
            data = new Uint8Array(data.buffer);
        }
        return data;
    }

    static u8ToASCII(array) {
        return Array.from(array).map( (byte) => {
            var chr = String.fromCharCode(byte);
            // A tricky way to decide if a character is "printable"
            // Probably gonna have some edge cases that are missed
            var uri = encodeURI(chr);
            if (uri.length == 1 || uri === "%25" ||
                                   uri === "%3C" ||
                                   uri === "%3E" ||
                                   uri === "%22" ||
                                   uri === "%20" ||
                                   uri === "%5E" ||
                                   uri === "%60" ||
                                   uri === "%5B" ||
                                   uri === "%5D") {
                // It is a proper character within a URI
                // ... or it is a % or " " or ^ or ` or [ or ] (yikes)
                return chr;
            }
            else {
                return ".";
            }
        }).join('');
    }

    /**
     * Loads the script at the given URL and resolves when is loaded.
     *
     * @param {string} url - The URL of the JavaScript to load.
     */
    static async loadScript(url) {
        if (!Util.loadedScripts[url]) {
            Util.loadedScripts[url] = new Promise( (resolve, reject) => {
                // Add a <script> to the head of the document
                var head = document.head;
                var script = document.createElement('script');
                script.src = url;

                // Pass 'resolve' as a our event handler for the various state
                // change event handler options for different browsers.
                let blah = () => { resolve(); };
                script.onreadystatechange = blah;
                script.onload = blah;

                // Callback for errors
                script.onerror = reject;

                // Append to the <head> and it will now download
                head.appendChild(script);
            });
        }

        return Util.loadedScripts[url];
    }

    /**
     * This will create a new element from a template.
     *
     * This assumes the template has exactly one root child, which will be
     * instantiated and returned.
     *
     * @param {HTMLElement} template - The template element.
     *
     * @returns {HTMLElement} The new element.
     */
    static createElementFromTemplate(template) {
        let ret = null;
        if ('content' in template) {
            ret = document.importNode(template.content, true);
            ret = ret.children[0];
        }
        else {
            ret = template.children[0].cloneNode(true);
        }
        return ret;
    }

    /**
     * Returns a structure that can measure the text for the given element.
     *
     * @param {HTMLElement} element - The element to measure.
     */
    static getFontMetrics(element) {
        // Create an element
        let div = document.createElement("div");

        // Add content we want to measure
        div.innerHTML = "X".repeat(256);

        // Copy styles to it
        let styles = document.defaultView.getComputedStyle(element, "");
        div.style.cssText = styles.cssText;
        div.style.font = styles.font;

        // But then force the ones we need for positioning
        div.style.position = "absolute";
        div.style.padding = "0";
        div.style.margin = "0";
        div.style.whiteSpace = "pre";
        div.style.visibility = "hidden";
        div.style.left = "0px";
        div.style.top = "0px";
        div.style.width = "auto";
        div.style.height = "auto";

        let sizes = {};

        return {
            __sizes: sizes,
            measureCharacter: (ch) => {
                if (sizes[ch] === undefined) {
                    // Add to body
                    document.body.appendChild(div);

                    // Measure
                    div.innerHTML = ch.repeat(256);
                    let rect = div.getBoundingClientRect();
                    sizes[ch] = {
                        width: rect.width / 256,
                        height: rect.height
                    };

                    // Remove from body
                    div.remove();
                }

                return sizes[ch];
            },
            measure: (str) => {
                // Add to body
                document.body.appendChild(div);

                // Measure
                div.innerHTML = ch.repeat(256);
                let rect = div.getBoundingClientRect();

                // Remove from body
                div.remove();

                return {
                    width: rect.width,
                    height: rect.height
                };
            }
        };
    }
}

Util.loadedScripts = {};
