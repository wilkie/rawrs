export class KeyBindings {
    constructor(element) {
        this._bindings = {
            "": {},
            "Alt": {
              "Control": {
                "Shift": {}
              },
              "Shift": {}
            },
            "Control": {
              "Shift": {}
            },
            "Shift": {}
        };

        document.body.addEventListener('keydown', (ev) => {
            let bindings = this._bindings;
            if (ev.altKey) {
                bindings = bindings["Alt"];
            }

            if (ev.ctrlKey) {
                bindings = bindings["Control"];
            }

            if (ev.shiftKey) {
                bindings = bindings["Shift"];
            }

            // Click on the element if the key binding matches.
            if (bindings[ev.code]) {
                ev.preventDefault();
                ev.stopPropagation();

                bindings[ev.code].click();
            }
        });

        let items = element.querySelectorAll("*[data-keybinding]");
        items.forEach( (item) => {
            let binding = item.getAttribute('data-keybinding');

            let modifiers = binding.split('+');
            let code = modifiers.pop();

            // Sort: Alt, Control, Shift
            modifiers = modifiers.sort().reverse();

            let bindings = this._bindings;
            while (modifiers.length) {
                const modifier = modifiers.pop()

                if (bindings[modifier]) {
                    bindings = bindings[modifier];
                }
                else {
                    throw new Error(`binding modifier unknown in ${binding}`);
                }
            }

            // Establish the binding.
            bindings[code] = item;

            // Add key binding help pane.
            let span = document.createElement("span");
            span.classList.add("key-binding-help");
            span.setAttribute('aria-hidden', 'true');
            span.textContent = binding.replace("Control", "Ctrl");
            item.appendChild(span);
        });

        //this.show();
    }

    /**
     * Shows the key bindings on the screen.
     */
    show() {
        document.body.classList.add('key-bindings-show');
    }

    /**
     * Hides the key bindings on the screen.
     */
    hide() {
        document.body.classList.remove('key-bindings-show');
    }
}
