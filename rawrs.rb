# RAWRS - Robust Assembler and Web-based Runtime System
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'bundler'
Bundler.require

require_relative './lib/rouge/lexers/riscv.rb'

require 'rouge/plugins/redcarpet'

require 'base64'
require 'yaml'

require 'sinatra/reloader'

class RAWRS < Sinatra::Base
  module Helpers
    # Creates a sanitary id for an HTML anchor.
    def slugify(str)
      str.gsub(/[^\w]/, "-").gsub(/-+/, '-').gsub(/^-|-$/, '').downcase
    end

    def targets
      # Pull the directory listing at assets/js/rawrs/targets
      ret = []
      Dir[File.join(File.dirname(__FILE__), 'assets', 'js', 'rawrs', 'targets', '*.js')].each do |path|
        ret.push(File.basename(path, '.js'))
      end

      # The items found in assets/js/targets/:target will be the built programs.
      # Usually this is where JavaScript compiled versions of programs are placed.
      # To 'enable' a target dynamically, ensure that a directory exists with the
      # name of the target in assets/js/targets/. even when no such compiled
      # program is necessary (all code is within the assets/js/rawrs/targets path)
      installed = Dir[File.join(File.dirname(__FILE__), 'assets', 'js', 'targets', '*')].map do |path|
        File.basename(path)
      end

      # It should filter by those found in assets/js/targets
      ret = ret.filter do |item|
        installed.include?(item)
      end

      # Now pull info sheets and return those
      ret.map do |tag|
        infoPath = File.join(File.dirname(__FILE__), 'assets', 'js', 'rawrs', 'targets', tag, 'info.json')
        info = nil
        if File.exist?(infoPath)
          File.open(infoPath) do |f|
            info = JSON.parse(f.read)
          end
        end

        {
          tag: tag,
          info: info
        }
      end
    end

    def partial(page, options={}, &block)
      if page.to_s.include? "/"
        page = page.to_s.sub(/[\/]([^\/]+)$/, "/_\\1")
      else
        page = "_#{page}"
      end

      render(:slim, page.to_sym, options.merge!(:layout => false), &block)
    end

    def no_cache
      headers "Expires"       => "Fri, 01 Jan 1980 00:00:00 GMT",
              "Pragma"        => "no-cache",
              "Cache-Control" => "no-cache, max-age=0, must-revalidate"
    end

    def forever_cache
      now = Time.now
      headers "Date" => now.to_s,
              "Expires" => (now + 31536000).httpdate,
              "Cache-Control" => "public, max-age=31536000"
    end

    # Renders the given markdown guidance page with or without the layout.
    def render_guidance(page, target, layout = nil)
      # Get interpolated strings
      if !defined?(@@binutils_authors)
        begin
          @@binutils_authors = JSON.parse(`ruby scripts/parse_binutils_authors.rb`).join(", ")
        rescue
          @@binutils_authors = "various"
        end
      end

      lang = :en

      page = page.to_s

      if page.start_with?("/")
        page = page[1..-1]
      end

      # Negotiate whether or not there is a "_#{page}.slim" or just "#{page}.md"
      # file and render one of them accordingly (preference to slim).
      if target
        slimpath = :"guidance/targets/#{target}/#{lang.to_s}/_#{page}"
        mdpath = :"guidance/targets/#{target}/#{lang.to_s}/#{page}"
      else
        # Discover if there is a non-localized page
        slimpath = :"guidance/_#{page}"
        if !File.exist?("views/#{slimpath}.slim")
          slimpath = :"guidance/#{lang.to_s}/_#{page}"
          mdpath = :"guidance/#{lang.to_s}/#{page}"
        end
      end

      if !File.exist?("views/#{mdpath}.md") && !File.exist?("views/#{slimpath}.slim")
        # default to the english docs if we cannot find the requested language
        if target
          slimpath = :"guidance/targets/#{target}/en/_#{page}"
          mdpath = :"guidance/targets/#{target}/en/#{page}"
        else
          slimpath = :"guidance/en/_#{page}"
          mdpath = :"guidance/en/#{page}"
        end
      end

      if layout.nil?
        ret = render(:slim, :index, :locals => {
          :tab => :guidance,
          :target => target,
          :guidance => page
        })

        # Add permalinks and identifiers to headers that need them
        html = Nokogiri::HTML.parse(ret)
        ['h2', 'h3', 'h4'].each do |tag|
          html.css(".content.documentation #{tag}").each do |el|
            id = el.attr('id')

            if !id
              el.set_attribute('id', slugify(el.text))
            end

            permalink = el.at_css('a.permalink')
            if !permalink
              el << "<a href='##{id}' class='permalink'><svg><use xlink:href='#{@basepath}images/symbol/ui.svg#link'></svg></a>"
            end
          end
        end

        # Add the clear bar to the h1
        h1 = html.at_css('.content.documentation > h1')
        if h1
          h1.add_next_sibling("<div class='clear'></div>")
        end

        # Re-serialize (nokogiri likes to insert newlines between <li>s)
        ret = html.to_html.gsub("\n<li", "<li")
      else
        if File.exist?("views/#{slimpath}.slim")
          ret = render(:slim, slimpath.to_sym, :layout => false)
        else
          data = File.read("views/#{mdpath}.md")
          data.gsub!("{% binutils_authors %}", @@binutils_authors)
          ret = render(:markdown, "# basepath: #{@basepath}\n\n" + data, :layout => layout)
        end
        ret.gsub!(" href=\"http", " target=\"_blank\" href=\"http")
      end

      ret.gsub!("<p><img", "<p class=\"image\"><img")
      ret
    end

    def guidance_pages_for(target)
      # If there is a 'toc.json', use that
      tocPath = "views/guidance/targets/#{target}/toc.json"
      ret = []
      if File.exist?(tocPath)
        File.open(tocPath) do |f|
          ret = JSON.parse(f.read)
        end
      end
      ret
    end
  end

  class DocHTMLRenderer < Redcarpet::Render::HTML
    include Rouge::Plugins::Redcarpet # yep, that's it. (Thanks Jeanine!!)
    include RAWRS::Helpers

    def codespan(code)
      classes = ""
      if code.start_with?("{.")
        # Add class
        klass = code[2...code.index('}')]
        code = code[3 + klass.length..-1]
        classes = " class=\"#{klass}\""
      end

      "<code#{classes}>#{code}</code>"
    end

    def header(title, level)
      # Kludge to pass along the basepath to the markdown renderer
      if !@basepath && title.strip.start_with?("basepath:")
        @basepath = title.split(':')[1...].join('').strip
        return ""
      end

      # Get the slug for the title and use it as an id
      id = slugify(title)

      "<h#{level} id=#{id}>#{title}#{level > 1 ? "<a href='##{id}' class='permalink'><svg><use xlink:href='#{@basepath}images/symbol/ui.svg#link'></svg></a></h#{level}>" : "</h1><div class='clear'></div>"}"
    end
  end

  # Helpers
  helpers RAWRS::Helpers

  # Use HTML5
  set :haml, :format => :html5

  # Use root directory as root
  set :app_file => '.'

  # Static Asset Management
  set :public_folder, "assets"
  set :static_cache_control, [:public, max_age: 60 * 60 * 24 * 365]

  # Markdown
  Tilt.register Tilt::RedcarpetTemplate, 'md'
  Tilt.prefer   Tilt::RedcarpetTemplate
  set :markdown, :renderer => RAWRS::DocHTMLRenderer,
                 :layout_engine => :slim,
                 :layout => :"guidance/layout",
                 :tables => true,
                 :fenced_code_blocks => true

  # I18n
  I18n.load_path += Dir[File.join(File.dirname(__FILE__), 'locales', '**', '*.yml')]
  I18n.load_path += Dir[File.join(Gem::Specification.find_by_name('rails-i18n').gem_dir, 'rails', 'locale', '*.yml')]

  # Ensure wasm is seen with the correct mime type
  Rack::Mime::MIME_TYPES[".wasm"] = "application/wasm"

  # Reloader
  register Sinatra::Reloader

  # Also reload locales
  locale_reloader = Sinatra::Reloader::Watcher::List.for(:locales)
  Dir[File.join(File.dirname(__FILE__), "locales", '**', '*.yml')].each do |file|
    locale_reloader.watch_file(file)
  end

  MUTEX_FOR_PERFORM = Mutex.new
  before do
    MUTEX_FOR_PERFORM.synchronize {
      locale_reloader.updated.each do |watcher|
        I18n.reload!
        watcher.update
      end
    }
  end

  # Routes

  # Index page
  get '/' do
    @basepath = ""
    render(:slim, :index)
  end

  # Target Listing
  get '/targets.json' do
    content_type 'application/json'

    # Return an array of targets as JSON, such as:
    # [
    #   {
    #     "tag": "riscv64",
    #     "info": { ... }
    #   },
    #   {
    #     "tag": "riscv64-linux",
    #     "info": { ... }
    #   }
    # ]
    targets().to_json
  end

  # Edit page
  get '/edit' do
    @basepath = ""
    render(:slim, :index, :locals => {
      :tab => :edit
    })
  end

  # Retrieve the file listing for a target
  get '/files/:target.html' do
    @basepath = params[:basepath] || ""
    render(:slim, :"edit/_project-pane-directory", :locals => {
      :path => "assets/files/#{params[:target]}",
      :root => "files/#{params[:target]}",
      :suppress => true
    })
  end

  # Run page
  get '/run' do
    @basepath = ""
    render(:slim, :index, :locals => {
      :tab => :run
    })
  end

  # Guidance page index
  get '/guidance/:page' do
    @basepath = "../"
    if params[:page].end_with?('_ajax')
      params[:page] = params[:page][0...-5]
      render_guidance(params[:page], nil, nil)
    else
      render_guidance(params[:page], nil)
    end
  end

  # Guidance page for target
  get '/guidance/:target/:page' do
    @basepath = "../../"
    if params[:page].end_with?('_ajax')
      params[:page] = params[:page][0...-5]

      render_guidance(params[:page], params[:target], nil)
    else
      render_guidance(params[:page], params[:target])
    end
  end

  # Stylesheets
  get '/css/highlight.css' do
    content_type 'text/css', :charset => 'utf-8'
    Rouge::Themes::Base16.mode(:dark).render(scope: '.highlight')
  end

  get '/css/:filename.css' do
    content_type 'text/css', :charset => 'utf-8'
    scss "css/#{params[:filename]}".intern
  end

  # Gather locale information from the YAML for JS translation
  get '/locales/:lang.json' do
    I18n.t('.')

    content_type 'application/json'

    class I18n::Backend::Simple
      class << self
        attr_accessor :translations
      end
    end

    I18n.config.backend.translations[params[:lang].intern].to_json
  end

  # Landing / Marketing pages

  get '/landing' do
    @basepath = ""
    render(:slim, :"landing/index")
  end

  get '/downloads' do
    @basepath = ""
    render(:slim, :"landing/downloads")
  end

  get '/learn-more' do
    @basepath = ""
    render(:slim, :"landing/learn-more")
  end

  get '/education' do
    @basepath = ""
    render(:slim, :"landing/education")
  end

  # Returns safe HTML to embed an SVG into a page
  #
  # Taken from https://coderwall.com/p/d1vplg/embedding-and-styling-inline-svg-documents-with-css-in-rails
  # Thanks James Martin! I've added width/height setting and custom coloring.
  def embed_svg(path, options={})
    require 'nokogiri'
    url = path
    if path.start_with?("/")
      path = path[1..-1]
    end
    path = File.join("assets", path)
    if !path.end_with?("svg")
      # Abort
      return "<img src='#{url}'>"
    end

    if options[:hex]
      options[:color] = "##{options[:hex]}"
    end

    svgData = File.read(path)
    doc = Nokogiri::HTML::DocumentFragment.parse svgData
    svg = doc.at_css 'svg'
    if svg.nil?
      return ""
    end

    if options[:viewBox]
      svg['viewBox'] = options[:viewBox]
    end
    if options[:class]
      svg['class'] = options[:class]
    end
    if options[:id]
      svg['id'] = options[:id]
    end
    if options[:width]
      svg['width'] = options[:width]
    end
    if options[:height]
      svg['height'] = options[:height]
    end
    if options[:color]
      svg.add_child("<style>rect, path, circle { fill: #{options[:color]} !important; }</style>")
    end
    doc.to_html
  end
end
