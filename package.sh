ROOTDIR=$PWD

mkdir -p static
rm -rf static/public
rackula generate -p assets -o static/public

# Fix guidance instruction links
if [ -f static/public/guidance/riscv64/instructions_ajax.html ]; then
  sed -i s/instructions_ajax.html\#/\#/g static/public/guidance/riscv64/instructions_ajax.html
fi

# Remove things we don't need
rm -rf static/public/js/ace-builds/.git
rm -rf static/public/js/ace-builds/.github
rm -rf static/public/js/ace-builds/src
rm -rf static/public/js/ace-builds/src-min
rm -rf static/public/js/ace-builds/src-min-noconflict
rm -rf static/public/js/ace-builds/demo

# Remove landing stuff from normal package
rm -rf static/public/landing

# Start packaging / removing targets

# First create the base
if [ -d static/dist ]; then
  rm -rf static/dist
fi
cp -r static/public static/dist

# Just remove all targets
rm -rf static/dist/js/targets
rm -rf static/dist/landing

# Package targets seperately
for f in assets/js/rawrs/targets/*; do
  if [ -d "$f" ]; then
    TARGET=$(basename $f)

    echo ""
    echo "Packaging ${TARGET} ..."

    # Remove existing
    if [ -d packages/${TARGET}/dist ]; then
      rm -rf packages/${TARGET}/dist
    fi

    if [ -f assets/landing/targets/${TARGET}.zip ]; then
      rm assets/landing/targets/${TARGET}.zip
    fi

    # Create dist paths
    mkdir -p packages/${TARGET}/dist/js/targets/${TARGET}
    mkdir -p packages/${TARGET}/dist/files/${TARGET}
    mkdir -p packages/${TARGET}/dist/static/${TARGET}

    echo " - compiled JavaScript"

    # Remove for the base package
    if [ -f static/dist/js/compiled-${TARGET}.js ]; then
      rm -rf static/dist/js/compiled-${TARGET}.js
    fi
    if [ -f static/dist/js/compiled-${TARGET}.js.map ]; then
      rm -rf static/dist/js/compiled-${TARGET}.js.map
    fi

    # Add to the package distribution
    if [ -f assets/js/compiled-${TARGET}.js ]; then
      cp assets/js/compiled-${TARGET}.js packages/${TARGET}/dist/js/.
      cp assets/js/compiled-${TARGET}.js.map packages/${TARGET}/dist/js/.
    fi

    # Remove for the base package
    if [ -d static/dist/js/targets/${TARGET} ]; then
      rm -rf static/dist/js/targets/${TARGET}
    fi

    # Add to the package distribution
    if [ -d assets/js/targets/${TARGET} ]; then
      cp -r assets/js/targets/${TARGET}/* packages/${TARGET}/dist/js/targets/${TARGET}/.
    fi

    echo " - examples"

    # Remove for the base package
    if [ -d static/dist/files/${TARGET} ]; then
      rm -rf static/dist/files/${TARGET}
    fi

    # Add to the package distribution
    if [ -d assets/files/${TARGET} ]; then
      cp -r assets/files/${TARGET}/* packages/${TARGET}/dist/files/${TARGET}/.
    fi

    echo " - static assets"

    # Remove for the base package
    if [ -d static/dist/static/${TARGET} ]; then
      rm -rf static/dist/static/${TARGET}
    fi

    # Add to the package distribution
    if [ -d assets/static/${TARGET} ]; then
      cp -r assets/static/${TARGET}/* packages/${TARGET}/dist/static/${TARGET}/.
    fi

    mkdir -p assets/landing/targets
    cd packages/${TARGET}/dist
    zip ../../../assets/landing/targets/${TARGET}.zip -qr ./
    cd $ROOTDIR
  fi
done

mkdir -p assets/landing/dist

# Package it (all)
echo ""
echo "Packaging RAWRS [all] ..."
if [ -f assets/landing/dist/rawrs-all.zip ]; then
  rm assets/landing/dist/rawrs-all.zip
fi

cd static/public
zip ../../assets/landing/dist/rawrs-all.zip -qr ./
cd $ROOTDIR

# Package it (core)
echo ""
echo "Packaging RAWRS [core] ..."
if [ -f assets/landing/dist/rawrs-core.zip ]; then
  rm assets/landing/dist/rawrs-core.zip
fi

cd static/dist
zip ../../assets/landing/dist/rawrs-core.zip -qr ./
cd $ROOTDIR
