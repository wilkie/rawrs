#include <libretro.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <emscripten.h>

#include "../gb/gb.h"
#include "../gb/gbMemory.h"

extern "C" {
    void get_registers(void* data) {
        uint16_t* buffer = (uint16_t*)data;
        buffer[0] = AF.W;
        buffer[1] = BC.W;
        buffer[2] = DE.W;
        buffer[3] = HL.W;
        buffer[4] = SP.W;
        buffer[5] = PC.W;
    }

    void set_registers(void* data) {
        uint16_t* buffer = (uint16_t*)data;
        AF.W = buffer[0];
        BC.W = buffer[1];
        DE.W = buffer[2];
        HL.W = buffer[3];
        SP.W = buffer[4];
        PC.W = buffer[5];
    }

    uint8_t read_memory8(uint64_t ptr) {
        return gbReadMemory((uint16_t)ptr);
    }

    void write_memory8(uint64_t ptr, uint8_t value) {
        gbWriteMemory((uint16_t)ptr, value);
    }

    uint16_t read_memory16(uint64_t ptr) {
        // Little-endian read
        return (gbReadMemory((uint16_t)ptr)) |
               (gbReadMemory((uint16_t)(ptr + 1)) << 8);
    }

    void write_memory16(uint64_t ptr, uint16_t value) {
        // Little-endian store
        gbWriteMemory((uint16_t)ptr, value & 0xff);
        gbWriteMemory((uint16_t)(ptr + 1), (value >> 8) & 0xff);
    }

    uint32_t read_memory32(uint64_t ptr) {
        // Little-endian read
        return (gbReadMemory((uint32_t)ptr)) |
               (gbReadMemory((uint32_t)(ptr + 1)) << 8) |
               (gbReadMemory((uint32_t)(ptr + 2)) << 16) |
               (gbReadMemory((uint32_t)(ptr + 3)) << 24);
    }

    void write_memory32(uint64_t ptr, uint32_t value) {
        // Little-endian store
        gbWriteMemory((uint16_t)ptr, value & 0xff);
        gbWriteMemory((uint16_t)(ptr + 1), (value >> 8) & 0xff);
        gbWriteMemory((uint16_t)(ptr + 2), (value >> 16) & 0xff);
        gbWriteMemory((uint16_t)(ptr + 3), (value >> 24) & 0xff);
    }
}
