

self.initWraps = () => {
    if (self.Module && !self.Module.__wraps) {
        self.Module.__push_samples = (data, frames) => {
            // Read audio samples from data
            if (self.Module.__audioDataLeft) {
                let audioDataLeft = self.Module.__audioDataLeft;
                let audioDataRight = self.Module.__audioDataRight;
                self.Module.__audioDataLeft = null;
                self.Module.__audioDataRight = null;

                // Write samples from data to left and right buffers
                let ptrLeft = data;
                let ptrRight = data + (512 * 4);

                frames = Math.min(audioDataLeft.length, frames);

                audioDataLeft.set(self.Module.HEAPU32.subarray(ptrLeft >> 2, (ptrLeft >> 2) + frames), 0);
                audioDataRight.set(self.Module.HEAPU32.subarray(ptrRight >> 2, (ptrRight >> 2) + frames), 0);

                let left = self.Module.__audioViewLeft.buffer;
                let right = self.Module.__audioViewRight.buffer;

                // Send message
                self.postMessage({
                    type: 'audio',
                    name: 'audio',
                    channels: [left, right],
                    samples: frames
                }, [left, right]);
            }
        };

        self.Module.__push_frame = (data, width, height, pitch) => {
            // Read data into the provided buffer
            if (self.Module.__imageData) {
                let imageData = self.Module.__imageData;
                self.Module.__imageData = null;

                imageData.set(self.Module.HEAPU32.subarray(data >> 2, (data >> 2) + (width * height)), 0);
                let buffer = self.Module.__imageView.buffer;

                // Send message
                self.postMessage({
                    type: 'frame',
                    name: 'frame',
                    buffer: buffer,
                    width: width,
                    height: height
                }, [buffer]);
            }
        };

        self.Module.__wraps = {
            js_init: self.Module.cwrap('js_init', null, []),
            load_game: self.Module.cwrap('load_game', null, ['string']),
            get_registers: self.Module.cwrap('get_registers', null, ['number']),
            set_registers: self.Module.cwrap('set_registers', null, ['number']),
            read_memory8: self.Module.cwrap('read_memory8', 'number', ['number']),
            write_memory8: self.Module.cwrap('write_memory8', null, ['number', 'number']),
            read_memory16: self.Module.cwrap('read_memory16', 'number', ['number']),
            write_memory16: self.Module.cwrap('write_memory16', null, ['number', 'number']),
            read_memory32: self.Module.cwrap('read_memory32', 'number', ['number']),
            write_memory32: self.Module.cwrap('write_memory32', null, ['number', 'number']),
            controller_port_count: self.Module.cwrap('controller_port_count', 'number', []),
            controller_port_type_count: self.Module.cwrap('controller_port_type_count', 'number', ['number']),
            controller_port_type_id: self.Module.cwrap('controller_port_type_id', 'number', ['number', 'number']),
            controller_port_type_name: self.Module.cwrap('controller_port_type_name', 'string', ['number', 'number']),
            controller_port_type_item_count: self.Module.cwrap('controller_port_type_item_count', 'number', ['number', 'number']),
            controller_port_type_item_id: self.Module.cwrap('controller_port_type_item_id', 'number', ['number', 'number', 'number']),
            controller_port_type_item_device: self.Module.cwrap('controller_port_type_item_device', 'number', ['number', 'number', 'number']),
            controller_port_type_item_name: self.Module.cwrap('controller_port_type_item_name', 'string', ['number', 'number', 'number']),
            controller_port_type_item_set_state: self.Module.cwrap('controller_port_type_item_set_state', null, ['number', 'number', 'number', 'number']),
            retro_init: self.Module.cwrap('retro_init', null, []),
            retro_deinit: self.Module.cwrap('retro_deinit', null, []),
            retro_reset: self.Module.cwrap('retro_reset', null, []),
            retro_run: self.Module.cwrap('retro_run', null, []),
            retro_load_game: self.Module.cwrap('retro_load_game', null, ['number']),
            retro_load_game_special: self.Module.cwrap('retro_load_game_special', null, ['number', 'number', 'number']),
            retro_serialize_size: self.Module.cwrap('retro_serialize_size', 'number', []),
            retro_serialize: self.Module.cwrap('retro_serialize', 'bool', ['number', 'number']),
            retro_unserialize: self.Module.cwrap('retro_unserialize', 'bool', ['number', 'number']),
            retro_get_memory_data: self.Module.cwrap('retro_get_memory_data', 'number', ['number']),
            retro_get_memory_size: self.Module.cwrap('retro_get_memory_size', 'number', ['number']),
            retro_cheat_reset: self.Module.cwrap('retro_cheat_reset', null, []),
            retro_cheat_set: self.Module.cwrap('retro_cheat_set', null, ['number', 'bool', 'string']),
            retro_unload_game: self.Module.cwrap('retro_unload_game', null, []),
            retro_get_region: self.Module.cwrap('retro_get_region', 'number', []),
            retro_api_version: self.Module.cwrap('retro_api_version', 'number', []),
            retro_set_controller_port_device: self.Module.cwrap('retro_set_controller_port_device', null, ['number', 'number']),
            retro_get_system_info: self.Module.cwrap('retro_get_system_info', null, ['number']),
            retro_get_system_av_info: self.Module.cwrap('retro_get_system_av_info', null, ['number']),

            // TODO: these callback functions have to be set up in particular ways
            retro_set_environment: self.Module.cwrap('retro_set_environment', null, ['number']),
            retro_set_audio_sample: self.Module.cwrap('retro_set_audio_sample', null, ['number']),
            retro_set_audio_sample_batch: self.Module.cwrap('retro_set_audio_sample_batch', null, ['number']),
            retro_set_input_poll: self.Module.cwrap('retro_set_input_poll', null, ['number']),
            retro_set_input_state: self.Module.cwrap('retro_set_input_state', null, ['number']),
            retro_set_video_refresh: self.Module.cwrap('retro_set_video_refresh', null, ['number']),
        };
    }
};

self.messageHook = (msg) => {
    self.initWraps();

    if (msg.type === 'api') {
        msg.args = msg.args || [];
        let argTypes = [];

        // If any arguments are a structure, we allocate a pointer to heap and
        // pull out the arguments that way
        // msg.args = [{
        //   'geometry': {
        //     'base_width': 'i32',
        //     'base_height': 'i32',
        //     'max_width': 'i32',
        //     'max_height': 'i32',
        //     'aspect_ratio': 'float'
        //   },
        //   'timing': {
        //     'fps': 'double',
        //     'sample_rate': 'double'
        //   }
        // }];
        for (let i = 0; i < msg.args.length; i++) {
            let arg = msg.args[i];

            if (arg instanceof ArrayBuffer) {
                // 'arg' being an array is a buffer of some kind
                argTypes.push(arg);

                // We need to allocate a similar sized buffer and pass it to the
                // function.
                let ptr = self.Module._malloc(arg.byteLength);

                // Copy the values from the array
                self.Module.HEAP8.set(new Uint8Array(arg), ptr);
            }
            else if (Object.keys(arg).length > 0 && !(typeof arg === 'string')) {
                argTypes.push(arg);

                // Count items
                let last = null;
                let count = (data) => {
                    if (last != data) {
                        last = data;
                    }
                    let size = 0;
                    let keys = Object.keys(data);
                    for (let j = 0; j < keys.length; j++) {
                        let key = keys[j];
                        switch (data[key]) {
                            case 'i8':
                                size += 1;
                                break;
                            case 'i16':
                                size += 2;
                                break;
                            case 'i32':
                            case 'float':
                                size += 4;
                                break;
                            case 'i64':
                            case 'double':
                                size += 8;
                                break;

                            default:
                                size += count(data[key]);
                                break;
                        }
                    }

                    return size;
                };

                let size = count(arg);

                // Allocate the structure
                let ptr = self.Module._malloc(size);

                msg.args[i] = ptr;
            }
            else {
                argTypes.push('number');
            }
        }

        // Run the function
        let result = self.Module.__wraps[msg.name](...msg.args);

        let resultArgs = [];
        let buffers = [];
        for (let i = 0; i < msg.args.length; i++) {
            let arg = argTypes[i];
            if (arg instanceof ArrayBuffer) {
                // Copy back the values
                let ptr = msg.args[i];
                let data = new Uint8Array(arg);
                data.set(self.Module.HEAP8.subarray(ptr, data.byteLength))

                // Free pointers
                self.Module._free(msg.args[i]);

                // Attach the buffer
                resultArgs.push(arg);
                buffers.push(arg);
            }
            else if (arg !== 'number') {
                // Re-interpret structure
                let parse = (data, ptrPtr) => {
                    let keys = Object.keys(data);
                    for (let j = 0; j < keys.length; j++) {
                        let key = keys[j];

                        switch (data[key]) {
                            case 'i8':
                                data[key] = self.Module.getValue(ptrPtr[0], data[key]);
                                ptrPtr[0]++;
                                break;
                            case 'i16':
                                data[key] = self.Module.getValue(ptrPtr[0], data[key]);
                                ptrPtr[0] += 2;
                                break;
                            case 'i32':
                            case 'float':
                                data[key] = self.Module.getValue(ptrPtr[0], data[key]);
                                ptrPtr[0] += 4;
                                break;
                            case 'i64':
                            case 'double':
                                data[key] = self.Module.getValue(ptrPtr[0], data[key]);
                                ptrPtr[0] += 8;
                                break;

                            default:
                                data[key] = parse(data[key], ptrPtr);
                                break;
                        }
                    }

                    return data;
                };

                // Parse memory for the values
                let value = parse(argTypes[i], [msg.args[i]]);

                // Free pointers
                self.Module._free(msg.args[i]);

                // Set value
                resultArgs.push(value);
            }
            else {
                resultArgs.push(msg.args[i]);
            }
        }

        self.postMessage({
            type: msg.type,
            name: msg.name,
            args: resultArgs,
            result: result
        }, buffers);
    }
    else if (msg.type == 'canvas') {
        // Retain the image data structure
        if (msg.buffer) {
            self.Module.__imageData = new Uint32Array(msg.buffer);
            self.Module.__imageView = new DataView(msg.buffer);
        }
    }
    else if (msg.type == 'audio') {
        // Retain the image data structure
        if (msg.channels) {
            self.Module.__audioDataLeft = new Uint32Array(msg.channels[0]);
            self.Module.__audioDataRight = new Uint32Array(msg.channels[1]);
            self.Module.__audioViewLeft = new DataView(msg.channels[0]);
            self.Module.__audioViewRight = new DataView(msg.channels[1]);
        }
    }
};
