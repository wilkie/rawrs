// A recreation of 816-opt.py for tcc optimizations

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

// If it is a jump or branch or ends in a colon, return true
int is_control(char* line) {
    if (((strlen(line) > 0) &&
        (line[0] == 'j' || line[0] == 'b' ||
         line[0] == '+' || line[0] == '-')) ||
        (line[strlen(line) - 1] == ':')) {

        return 1;
    }

    return 0;
}

// Whether or not the line changes 'accu'
int changes_accu(char* line) {
    int ispha = strncmp(line, "pha", 3) == 0;
    int issta = strncmp(line, "sta", 3) == 0;

    if (((line[2] == 'a') &&
        (!ispha && !issta)) ||
        (strlen(line) == 5 && line[strlen(line) - 1] == 'a')) {
        return 1;
    }

    return 0;
}

int match_storetopseudo(char* line) {
    if ((strncmp(line, "st", 2) == 0) &&
        (line[2] == 'a' || line[2] == 'x' || line[2] == 'y' || line[2] == 'z') &&
        (strncmp(&line[3], ".b tcc__", 8) == 0) &&
        (line[11] == 'r' || line[11] == 'f')) {
        return 1;
    }

    return 0;
}

int match_storexytopseudo(char* line) {
    if ((strncmp(line, "st", 2) == 0) &&
        (line[2] == 'x' || line[2] == 'y') &&
        (strncmp(&line[3], ".b tcc__", 8) == 0) &&
        (line[11] == 'r' || line[11] == 'f')) {
        return 1;
    }

    return 0;
}

int match_storeatopseudo(char* line) {
    if ((strncmp(line, "st", 2) == 0) &&
        (line[2] == 'a') &&
        (strncmp(&line[3], ".b tcc__", 8) == 0) &&
        (line[11] == 'r' || line[11] == 'f')) {
        return 1;
    }

    return 0;
}

int startswith(char* line, char* compare) {
    return strncmp(line, compare, strlen(compare)) == 0;
}

int equals(char* line, char* compare) {
    return strncmp(line, compare, strlen(compare) + 1) == 0;
}

int contains(char* line, char* search) {
    size_t len = strlen(line);
    for (size_t i = 0; i < len; i++) {
        if (startswith(&line[i], search)) {
            return 1;
        }
    }

    return 0;
}

int endswith(char* line, char* compare) {
    if (strlen(line) < strlen(compare)) {
        return 0;
    }

    return strncmp(&line[strlen(line) - strlen(compare)], compare, strlen(compare)) == 0;
}

void add_text_opt(char*** text_opt, size_t* text_opt_count, size_t* text_opt_max, char* line) {
    if (*text_opt_count == *text_opt_max) {
        *text_opt_max += 1000;
        *text_opt = (char**)realloc(*text_opt, sizeof(char*) * *text_opt_max);
    }

    // Create copy of string
    char* copy = (char*)malloc(sizeof(char) * (strlen(line) + 1));
    strcpy(copy, line);

    (*text_opt)[*text_opt_count] = copy;
    *text_opt_count = *text_opt_count + 1;
}

int main(int argc, char** argv) {
    FILE* f = fopen(argv[1], "rb");

    char** lines = NULL;
    size_t line_max = 1000;
    lines = (char**)malloc(sizeof(char*) * line_max);
    size_t line_count = 0;

    // Holds the bss section symbols
    size_t bss_max = 1000;
    char** bss = (char**)malloc(sizeof(char*) * bss_max);
    size_t bss_count = 0;

    // Read every line from the file and remove comments
    char* line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    while (read != -1) {
        read = getline(&line, &len, f);
        if (read == -1) {
            break;
        }

        // Chop newlines
        line[strlen(line) - 1] = '\0';

        // Ignore comments
        if (line[0] == ';') {
            continue;
        }

        if (line_count == line_max) {
            line_max += 1000;
            lines = (char**)realloc(lines, sizeof(char*) * line_max);
        }
        lines[line_count] = (char*)malloc(sizeof(char) * strlen(line) + 1);
        strncpy(lines[line_count], line, strlen(line) + 1);
        line_count++;
    }

    fclose(f);

    // Find .bss section symbols
    int bsson = 0;
    for (size_t i = 0; i < line_count; i++) {
        char* line = lines[i];

        if (strcmp(line, ".ramsection \".bss\" bank $7e slot 2") == 0) {
            bsson = 1;
            continue;
        }

        if (strcmp(line, ".ends") == 0) {
            bsson = 0;
        }

        if (bsson) {
            if (bss_count == bss_max) {
                bss_max += 1000;
                bss = (char**)realloc(bss, sizeof(char*) * bss_max);
            }
            bss[bss_count] = (char*)malloc(sizeof(char) * strlen(line) + 1);
            strncpy(bss[bss_count], line, strlen(line) + 1);

            // Cut at first ' '
            for (size_t j = 0; j < strlen(line); j++) {
                if (bss[bss_count][j] == ' ') {
                    bss[bss_count][j] = '\0';
                    break;
                }
            }

            bss_count++;
        }
    }

    // Total number of optimizations performed
    size_t totalopt = 0;

    // Have we optimized in this pass?
    int opted = -1;

    // Optimization pass counter
    size_t opass = 0;

    size_t text_opt_max = 1000;
    char** text_opt = NULL;
    size_t text_opt_count = 0;

    while (opted) {
        if (text_opt_count > 0) {
            // Copy optimized lines to be the actual lines

            // Destroy lines
            for (size_t i = 0; i < line_count; i++) {
                free(lines[i]);
            }
            free(lines);

            line_count = text_opt_count;
            line_max = text_opt_max;
            lines = text_opt;
        }

        fprintf(stderr, "pass %d %d looking at %d lines\n", opass, opted, line_count);
        opass++;
        opted = 0;

        // Reset the optimization text array
        text_opt_max = 1000;
        text_opt = (char**)malloc(sizeof(char*) * text_opt_max);
        text_opt_count = 0;

        // For every line
        for (size_t i = 0; i < line_count; ) {
            size_t s = i;
            char* line = lines[i];
            char* next = lines[i + 1];
            char* nextnext = lines[i + 2];
            char* nextnextnext = lines[i + 3];
            char* nextnextnextnext = lines[i + 4];

            // Stores (accu/x/y/zero) to pseudo-registers
            if (startswith(line, "st")) {
                if (match_storetopseudo(line)) {
                    char reg = line[2];
                    char* label = &line[11];
                    char* fulllabel = &line[6];

                    int doopt = 0;

                    // Eliminate redundant stores
                    for (size_t j = i + 1; j < MIN(line_count, i + 30); j++) {
                        char* look = lines[j];

                        if (match_storetopseudo(look) && (strcmp(label, &look[11]) == 0)) {
                            // Another store to the same pseudo-register
                            doopt = 1;
                            break;
                        }

                        if (startswith(look, "jsr.l ") && !startswith(look, "jsr.l tcc__")) {
                            // Before function call (will be clobbered anyway)
                            doopt = 1;
                            break;
                        }

                        // Cases in which we don't pursue optimization further
                        if (is_control(look) || contains(look, fulllabel)) {
                            // Branch or other use of the pseudo-register
                            break;
                        }

                        char* labelpointer = (char*)malloc(sizeof(char) * strlen(fulllabel) + 2);
                        labelpointer[0] = '[';
                        strncpy(&labelpointer[1], fulllabel, strlen(fulllabel) + 1);
                        // Trim 'h' off end, if it exists
                        if (labelpointer[strlen(labelpointer) - 1] == 'h') {
                          labelpointer[strlen(labelpointer) - 1] = '\0';
                        }

                        if ((label[strlen(label) - 1] == 'h') &&
                            (contains(look, labelpointer))) {
                            // Use of label as a pointer
                            free(labelpointer);
                            break;
                        }

                        free(labelpointer);
                    }

                    if (doopt) {
                        // Skip redundant store
                        i++;
                        opted++;
                        continue;
                    }
                }

                // Stores (x/y) to pseudo-registers
                if (match_storexytopseudo(line)) {
                    char reg = line[2];
                    char* label = &line[11];
                    char* fulllabel = &line[6];

                    // store hwreg to pseudo-register, push pseudo-register
                    // function call
                    //   ->
                    // push hwreg
                    // function call
                    char* call = (char*)malloc(sizeof(char) * (strlen(label) + 10));
                    strcpy(call, "pei (tcc__");
                    strcat(call, label);
                    strcat(call, ")");
                    if (strcmp(next, call) == 0 && startswith(nextnext, "jsr.l ")) {
                        free(call);
                        char* opt = (char*)malloc(sizeof(char) * 4);
                        strcpy(opt, "phx");
                        opt[2] = reg;
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                        free(opt);

                        i += 2;
                        opted++;
                        continue;
                    }

                    // store hwreg to pseudo-register, push pseudo-register
                    //   ->
                    // store hwreg to pseudo-register, push hwreg (shorter)
                    if (strcmp(next, call) == 0) {
                        free(call);
                        char* opt = (char*)malloc(sizeof(char) * 4);
                        strcpy(opt, "phx");
                        opt[2] = reg;
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                        free(opt);

                        i += 2;
                        opted++;
                        continue;
                    }
                    free(call);

                    // store hwreg to pseudo-register,
                    // load hwreg from pseudo-register
                    //   ->
                    // store hwreg to pseudo-register,
                    // transfer hwreg to hwreg (shorter)
                    char* load = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                    strcpy(load, "lda.b tcc__");
                    strcat(load, label);
                    if (startswith(next, load)) {
                        if (endswith(next, " ; DON'T OPTIMIZE")) {
                           // Just keeping this check in place
                           // FIXME: shouldn't this be marked as DON'T OPTIMIZE again?
                        }

                        free(load);

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                        char* opt = (char*)malloc(sizeof(char) * 4);
                        strcpy(opt, "txa");
                        opt[1] = reg;
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                        free(opt);

                        i += 2;
                        opted++;
                        continue;
                    }
                    free(load);
                }

                if (match_storeatopseudo(line)) {
                    char reg = line[2];
                    char* label = &line[11];
                    char* fulllabel = &line[6];

                    // store pseudo-register followed by load pseudo-register
                    //   ->
                    // store pseudo-register (omit load)
                    char* load = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                    strcpy(load, "lda.b tcc__");
                    strcat(load, label);
                    if (equals(next, load)) {
                        // Keep store
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                        // Omit load
                        i += 2;
                        opted++;
                        continue;
                    }

                    // store pseudo-register followed by load pseudo-register
                    // with ldx/ldy in between
                    //   ->
                    // store pseudo-register, ldx/ldy, (omit load)
                    if ((startswith(next, "ldx") ||
                        startswith(next, "ldy")) &&
                        (equals(nextnext, load))) {
                        // Keep store
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                        // Keep ldx/ldy
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);

                        // Omit load
                        i += 3;
                        opted++;
                        continue;
                    }
                    free(load);

                    // store accu to pseudo-register
                    // push pseudo-register
                    // function call
                    //   ->
                    // push accu
                    // function call
                    char* call = (char*)malloc(sizeof(char) * (strlen(label) + 10));
                    strcpy(call, "pei (tcc__");
                    strcat(call, label);
                    strcat(call, ")");
                    if (startswith(next, call) && startswith(nextnext, "jsr.l ")) {
                        free(call);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, "pha");

                        i += 2;
                        opted++;
                        continue;
                    }

                    // store accu to pseudo-register
                    // push pseudo-register
                    //   ->
                    // store accu to pseudo-register
                    // push accu (shorter)
                    if (startswith(next, call)) {
                        free(call);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, "pha");

                        i += 2;
                        opted++;
                        continue;
                    }
                    // store accu to pseudo-register1
                    // push pseudo-register2
                    // push pseudo-register1
                    //   ->
                    // store accu to pseudo-register1
                    // push pseudo-register2
                    // push accu (shorter)
                    else if (startswith(next, "pei ") && startswith(nextnext, call)) {
                        free(call);
                        // <wilkie> Strange... it reorders
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, "pha");

                        i += 3;
                        opted++;
                        continue;
                    }
                    free(call);

                    // Convert incs/decs on pseudo-registers -> incs/decs on hwregs
                    int cont = 0;
                    for (size_t type = 0; type < 2; type++) {
                        char* crem = "inc";
                        if (type == 1) {
                            crem = "dec";
                        }

                        char* compare = (char*)malloc(sizeof(char) * (strlen(label) + 12));
                        strcpy(compare, crem);
                        strcat(compare, ".b tcc__");
                        strcat(compare, label);

                        if (startswith(next, compare)) {
                            // store to pseudo-register
                            // 'crement on pseudo-register

                            if (startswith(nextnext, compare) && startswith(nextnextnext, "lda")) {
                                // store to pseudo-register
                                // two 'crements on pseudo-register
                                //   ->
                                // increment accu first, then store to pseudo-register

                                char* opt = (char*)malloc(sizeof(char) * (strlen(label) + 12));

                                strcpy(opt, crem);
                                strcat(opt, " a");

                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);

                                strcpy(opt, "sta.b tcc__");
                                strcat(opt, label);

                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);

                                free(opt);
                                free(compare);

                                char* load = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                                strcpy(load, "lda.b tcc__");
                                strcat(load, label);
                                // Subsequent load can be omitted (accu holds correct value)
                                if (startswith(nextnext, load)) { i += 4; } else { i += 3; }
                                free(load);

                                opted++;
                                cont = 1;
                                break;
                            }
                            else if (startswith(nextnext, "lda")) {
                                // Same as above but with just one 'crement
                                // FIXME: there should be a more clever way to do this...
                                char* opt = (char*)malloc(sizeof(char) * (strlen(label) + 12));

                                strcpy(opt, crem);
                                strcat(opt, " a");

                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);

                                strcpy(opt, "sta.b tcc__");
                                strcat(opt, label);

                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);

                                free(opt);
                                free(compare);

                                char* load = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                                strcpy(load, "lda.b tcc__");
                                strcat(load, label);
                                // Subsequent load can be omitted (accu holds correct value)
                                if (startswith(nextnext, load)) { i += 3; } else { i += 2; }
                                free(load);

                                opted++;
                                cont = 1;
                                break;
                            }
                        }
                    }

                    if (cont) {
                        continue;
                    }

                    // python 816-opt.py line 167

                    if ((startswith(next, "lda.b tcc__")) &&
                        (next[11] == 'r' || next[11] == 'f')) {

                        char* label2 = &next[11];

                        if (startswith(nextnext, "and") || startswith(nextnext, "ora")) {
                            // store to pseudo-register1
                            // load from pseudo-register2
                            // and/or pseudo-register1
                            //   ->
                            // store to pseudo-register1
                            // and/or pseudo-register2
                            if ((startswith(&nextnext[3], ".b tcc__")) &&
                                (startswith(&nextnext[11], label))) {

                                // store
                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                                char* opt = (char*)malloc(sizeof(char) * (strlen(label2) + 12));
                                strncpy(opt, nextnext, 3);
                                opt[3] = '\0';
                                strcat(opt, ".b tcc__");
                                strcat(opt, label2);

                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                                free(opt);

                                i += 3;
                                opted++;
                                continue;
                            }
                        }
                    }

                    // python 816-opt.py line 181

                    // store to pseudo-register
                    // switch to 8-bits
                    // load from pseudo-register
                    //   ->
                    // store to pseudo-register
                    // switch to 8-bits
                    // (skip load)
                    char* load2 = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                    strcpy(load2, "lda.b tcc__");
                    strcat(load2, label);
                    if ((startswith(next, "sep #$20")) &&
                        (equals(nextnext, load2))) {
                        free(load2);

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);

                        // skip load
                        i += 3;
                        opted++;
                        continue;
                    }
                    free(load2);

                    // python 816-opt.py line 189

                    // two stores to pseudo-register without control flow or
                    // other uses of pseudo-register
                    //   ->
                    // skip first store
                    if (!is_control(next) &&
                        !contains(next, fulllabel)) {
                        if (equals(nextnext, line)) {
                            add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);
                            add_text_opt(&text_opt, &text_opt_count, &text_opt_max, nextnext);

                            // skip first store
                            i += 3;
                            opted++;
                            continue;
                        }
                    }

                    // python 816-opt.py line 199

                    // store hwreg to pseudo-register
                    // load hwreg from pseudo-register
                    //   ->
                    // store hwreg to pseudo-register
                    // transfer hwreg to hwreg (shorter)
                    if ((startswith(next, "ld")) &&
                        (next[2] == 'x' || next[2] == 'y') &&
                        (startswith(&next[3], ".b tcc__")) &&
                        (startswith(&next[11], label))) {

                        char reg2 = next[2];

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                        char* opt = (char*)malloc(sizeof(char) * 4);
                        strcpy(opt, "tax");
                        opt[2] = reg2;

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                        free(opt);

                        i += 2;
                        opted++;
                        continue;
                    }

                    // python 816-opt.py line 208

                    // store accu to pseudo-register
                    // (with something in-between that does not alter control
                    //  flow or touch accu or pseudo-register)
                    // load accu from pseudo-register
                    //   ->
                    // skip load
                    if (!(is_control(next) || changes_accu(next) || contains(next, fulllabel))) {
                        char* load = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                        strcpy(load, "lda.b tcc__");
                        strcat(load, label);
                        if (equals(nextnext, load)) {
                            free(load);

                            add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);
                            add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);

                            // skip load
                            i += 3;
                            opted++;
                            continue;
                        }

                        free(load);
                    }

                    // python 816-opt.py line 217

                    // store pseudo-register1
                    // clc
                    // load pseudo-register2
                    // add pseudo-register1
                    //   ->
                    // store pseudo-register1
                    // clc
                    // add pseudo-register2
                    if (equals(next, "clc")) {
                        if ((startswith(nextnext, "lda.b tcc__r"))) {
                            char* label2 = &nextnext[11];

                            char* add = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                            strcpy(add, "adc.b tcc__");
                            strcat(add, label);

                            if (equals(nextnextnext, add)) {
                                free(add);

                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);
                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);

                                add = (char*)malloc(sizeof(char) * (strlen(label2) + 13));
                                strcpy(add, "adc.b tcc__");
                                strcat(add, label2);
                                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, add);
                                free(add);

                                // skip load
                                i += 4;
                                opted++;
                                continue;
                            }

                            free(add);
                        }
                    }

                    // python 816-opt.py line 230

                    // store accu to pseudo-register
                    // asl pseudo-register
                    //   ->
                    // asl accu
                    // store accu to pseudo-register
                    // FIXME: is this safe? can we rely on code not making
                    //        assumptions about the contents of the accu
                    //        after the shift?
                    char* asl = (char*)malloc(sizeof(char) * (strlen(label) + 13));
                    strcpy(asl, "asl.b tcc__");
                    strcat(asl, label);
                    if (equals(next, asl)) {
                        free(asl);

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, "asl a");
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                        i += 2;
                        opted++;
                        continue;
                    }
                    free(asl);
                }

                // python 816-opt.py line 237

                // store followed by load
                if (startswith(line, "sta ") && endswith(line, ",s")) {
                    char* label = (char*)malloc(sizeof(char) * strlen(line));
                    strcpy(label, &line[4]);
                    label[strlen(label) - 2] = '\0';

                    char* load = (char*)malloc(sizeof(char) * (strlen(label) + 8));
                    strcpy(load, "lda ");
                    strcat(load, label);
                    strcat(load, ",s");

                    free(label);
                    if (equals(next, load)) {
                        free(load);

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                        // skip load
                        i += 2;
                        opted++;
                        continue;
                    }
                }
            } // end startswith("st")

            // python 816-opt.py line 246

            if (startswith(line, "ld")) {
                if (contains(line, "ldx #0")) {
                    if ((startswith(line, "lda.l ")) &&
                        (endswith(line, ",x"))) {

                        char* label = (char*)malloc(sizeof(char) * strlen(line));
                        strcpy(label, &line[4]);
                        label[strlen(label) - 2] = '\0';

                        char* load = (char*)malloc(sizeof(char) * (strlen(label) + 7));
                        strcpy(load, "lda.l ");
                        strcat(load, label);

                        free(label);

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, load);
                        free(load);

                        if (!endswith(nextnextnext, ",x")) {
                            i += 2;
                            opted++;
                            continue;
                        }

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, nextnext);

                        load = (char*)malloc(sizeof(char) * (strlen(nextnextnext) + 1));
                        strcpy(load, nextnextnext);
                        load[strlen(load) - 2] = '\0'; // replace(',x','')
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, load);
                        free(load);

                        i += 4;
                        opted++;
                        continue;
                    }
                }

                // python 816-opt.py line 263

                if (equals(line, "lda.w #0")) {
                    if ((startswith(next, "sta.b ")) &&
                        (startswith(nextnext, "lda"))) {
                        char* copy = (char*)malloc(sizeof(char) * (strlen(next) + 1));
                        strcpy(copy, next);
                        copy[2] = 'z'; // replace('sta.','stz.')
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, copy);
                        free(copy);

                        i += 2;
                        opted++;
                        continue;
                    }
                }
                else if (startswith(line, "lda.w #")) {
                    if ((equals(next, "sep #$20")) &&
                        (startswith(nextnext, "sta ")) &&
                        (equals(nextnextnext, "rep #$20")) &&
                        (startswith(nextnextnextnext, "lda"))) {

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, "sep #$20");

                        char* copy = (char*)malloc(sizeof(char) * (strlen(line) + 1));
                        strcpy(copy, line);
                        copy[4] = 'b'; // replace('lda.w','lda.b')
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, copy);
                        free(copy);

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, nextnext);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, nextnextnext);

                        i += 4;
                        opted++;
                        continue;
                    }
                }

                // python 816-opt.py line 276

                if ((startswith(line, "lda.b")) &&
                    (!is_control(next)) &&
                    (!contains(next, "a")) &&
                    (startswith(nextnext, "lda.b"))) {

                    add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);
                    add_text_opt(&text_opt, &text_opt_count, &text_opt_max, nextnext);

                    i += 3;
                    opted++;
                    continue;
                }

                // python 816-opt.py line 283

                // don't write pseudo-register high back to stack if it hasn't
                // been updated
                if ((endswith(next, "h")) &&
                    (startswith(next, "sta.b tcc__r")) &&
                    (startswith(line, "lda ")) &&
                    (endswith(line, ",s"))) {

                    char* local = &line[4];
                    char* reg = &next[6];

                    size_t j = i + 2;
                    while ((j < line_count - 2) && !is_control(lines[j]) && !contains(lines[j], reg)) {
                        j++;
                    }

                    if ((startswith(lines[j], "lda.b ")) &&
                        (equals(&lines[j][6], reg)) &&
                        (startswith(lines[j + 1], "sta ")) &&
                        (equals(&lines[j + 1][4], local))) {

                        while (i < j) {
                            add_text_opt(&text_opt, &text_opt_count, &text_opt_max, lines[i]);
                            i++;
                        }

                        // skip load high pseudo-register; sta stack
                        i += 2;
                        opted++;
                        continue;
                    }
                }

                // python 816-opt.py line 308

                // reorder copying of 32-bit value to pseudo-register if it
                // looks as if that could allow further optimization
                // looking for:
                //   lda something
                //   sta.b tcc_rX
                //   lda something
                //   sta.b tcc_rYh
                //   ...tcc_rX...

                if ((startswith(line, "lda")) &&
                    (startswith(next, "sta.b tcc__r"))) {

                    char* reg = &next[6];
                    if ((!endswith(reg, "h")) &&
                        (startswith(nextnext, "lda") && !endswith(nextnext, reg)) &&
                        (startswith(nextnextnext, "sta.b tcc__r") && endswith(nextnextnext, "h")) &&
                        (endswith(nextnextnextnext, reg))) {

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, nextnext);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, nextnextnext);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);

                        i += 4;
                        // this is not an optinization per se, so we don't count it
                        continue;
                    }
                }
            } // end startswith("ld")

            if (equals(line, "rep #$20") && equals(next, "sep #$20")) {
                i += 2;
                opted++;
                continue;
            }

            // python 816-opt.py line 327

            // looks like this unwinds an address calculation
            if ((equals(line, "sep #$20")) &&
                (startswith(next, "lda #")) &&
                (equals(nextnext, "pha")) &&
                (startswith(nextnextnext, "lda #")) &&
                (equals(nextnextnextnext, "pha"))) {

                // text_opt += ['pea.w (' + text[i+1].split('#')[1] + ' * 256 + ' + text[i+3].split('#')[1] + ')']

                char* opt = (char*)malloc(sizeof(char) * (strlen(next) + strlen(nextnextnext) + 20));
                strcpy(opt, "pea.w (");
                strcat(opt, &next[5]);
                strcat(opt, " * 256 + ");
                strcat(opt, &nextnextnext[5]);
                strcat(opt, ")");
                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                free(opt);

                add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);

                i += 5;
                opted++;
                continue;
            }

            // python 816-opt.py line 334

            if (startswith(line, "adc #")) {
                if ((startswith(next, "sta.b tcc__")) &&
                    (next[11] == 'f' || next[11] == 'r')) {

                    char* label = &next[6];

                    if ((startswith(nextnext, "inc.b ")) &&
                        (equals(&nextnext[6], label)) &&
                        (startswith(nextnextnext, "inc.b ")) &&
                        (equals(&nextnextnext[6], label))) {

                        // Turn adc #x; sta.b; inc.b; inc.b; -> adc #x + 2; sta.b
                        char* opt = (char*)malloc(sizeof(char) * (10 + strlen(line)));
                        strcpy(opt, line);
                        strcat(opt, " + 2");
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                        free(opt);

                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, next);

                        i += 4;
                        opted++;
                        continue;
                    }
                }
            }

            // python 816-opt.py line 345

            if ((startswith(line, "lda.l ")) ||
                (startswith(line, "sta.l "))) {
                int cont = 0;

                for (size_t j = 0; j < bss_count; j++) {
                    char* b = bss[j];

                    if ((startswith(&line[2], "a.l ")) &&
                        (startswith(&line[6], b)) &&
                        (line[6 + strlen(b)] == ' ')) {

                        char* copy = (char*)malloc(sizeof(char) * (strlen(line) + 1));
                        strcpy(copy, line);
                        copy[4] = 'w'; // replace('lda.l','lda.w').replace('sta.l','sta.w')
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, copy);
                        free(copy);

                        i += 1;
                        opted++;
                        cont = 1;
                        break;
                    }
                }

                if (cont) {
                    continue;
                }
            }

            // python 816-opt.py line 356

            // looks like we are getting rid of jumps to the next instruction
            if ((startswith(line, "jmp.w ")) ||
                (startswith(line, "bra __"))) {
                size_t j = i + 1;
                int cont = 0;

                while (j < line_count && endswith(lines[j], ":")) {
                    char* copy = (char*)malloc(sizeof(char) * (strlen(lines[j]) + 1));
                    strcpy(copy, lines[j]);
                    copy[strlen(copy) - 1] = '\0'; // remove final ':'

                    if (endswith(line, copy)) {
                        // redundant branch, discard it
                        i += 1;
                        opted++;
                        cont = 1;

                        free(copy);
                        break;
                    }

                    free(copy);
                    j++;
                }

                if (cont) {
                    continue;
                }
            }

            // python 816-opt.py line 369

            if (startswith(line, "jmp.w ")) {
                // worst case is a 4-byte instruction, so if the jump target is
                // closer than 32 instructions, we can safely substitute a
                // branch.
                char* label = (char*)malloc(sizeof(char) * (strlen(line) + 2));
                strcpy(label, &line[6]);
                strcat(label, ":"); // add final ':'
                int cont = 0;

                for (size_t j = MAX(0, i - 32); j < MIN(line_count, i + 32); j++) {
                    if (equals(lines[j], label)) {
                        char* opt = (char*)malloc(sizeof(char) * (10 + strlen(line)));
                        strcpy(opt, "bra ");
                        strcat(opt, &line[6]);
                        add_text_opt(&text_opt, &text_opt_count, &text_opt_max, opt);
                        free(opt);

                        i += 1;
                        opted++;
                        cont = 1;
                        break;
                    }
                }

                free(label);
                if (cont) {
                    continue;
                }
            }

            // We fell through... just add the normal line
            add_text_opt(&text_opt, &text_opt_count, &text_opt_max, line);
            i++;
        }
    }

    for (size_t i = 0; i < bss_count; i++) {
        free(bss[i]);
    }

    free(bss);

    for (size_t i = 0; i < line_count; i++) {
        free(lines[i]);
    }

    free(lines);

    // Print out optimized lines
    for (size_t i = 0; i < text_opt_count; i++) {
        printf("%s\n", text_opt[i]);
        free(text_opt[i]);
    }

    free(text_opt);

    return 0;
}
