#!/bin/bash

# Build assets
npm run compile-assets

# Build JavaScript application
npm run build
