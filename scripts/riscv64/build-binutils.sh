#!/bin/bash

HEADER="Building RISC-V binutils"
TARGET=riscv64
PACKAGE=binutils

# The host target we are building
HOST="riscv64-unknown-elf"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/riscv64/riscv-gnu-toolchain/riscv-binutils
BUILDDIR=packages/riscv64/riscv-gnu-toolchain/riscv-binutils-build
BUILDJSDIR=packages/riscv64/riscv-gnu-toolchain/riscv-binutils-js

INSTALLDIR=assets/js/targets/riscv64/binutils

echo " - installing/updating RISC-V packages"
./scripts/riscv64/install.sh &> ${ROOTDIR}/packages/riscv64/install-during-binutils.log

# Create build path for building native risc-v binutils
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Compile native risc-v binutils
if [ ! -f ${ROOTDIR}/${BUILDDIR}/bfd/doc/chew ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - configuring a native binutils for RISC-V targets"
  ${SRCDIR}/configure --prefix /usr --target=${HOST} --disable-nls &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native binutils for RISC-V targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/1-make.log"
  cd ${ROOTDIR}
else
  echo " - using native build of RISC-V binutils in ${BUILDDIR}"
fi

# Install native risc-v binutils
if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-as ]; then
  echo " - installing native build of RISC-V binutils to ${UTILSDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  make install DESTDIR=${ROOTDIR}/${UTILSDIR} &> ${ROOTDIR}/${BUILDDIR}/2-make-install.log
  cd ${ROOTDIR}
else
  echo " - using existing installed native binutils for RISC-V targets"
fi

echo " - native binutils for RISC-V targets installed to ${UTILSDIR}"

PROFILING_OPTS=

# Uncomment to gain stack traces
#PROFILING_OPTS=-g --profiling-funcs

# Create build path for JavaScript build of risc-v binutils
if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

# Initialize emscripten
if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

CFLAGS="-DHAVE_PSIGNAL -m32 ${PROFILING_OPTS} -s ALLOW_MEMORY_GROWTH=1"

# Build the JavaScript binutils for risc-v
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gas/as.o ]; then
  # Configure
  cd ${ROOTDIR}/${BUILDJSDIR}
  echo " - configuring a JavaScript binutils for RISC-V targets"
  emconfigure ${SRCDIR}/configure CFLAGS="${CFLAGS}" --target=${HOST} --prefix /usr \
    --disable-nls \
    --enable-lto \
    --disable-install-libiberty \
    --disable-multilib &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # Build first pass
  echo " - copying over native generators needed during compilation"

  COPIES="bfd/doc/chew bfd/doc/chew.stamp binutils/sysinfo"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  echo " - building a JavaScript binutils for RISC-V targets"
  run "emmake make all-ld CFLAGS=\"${CFLAGS}\"" "${ROOTDIR}/${BUILDJSDIR}/2-make.log"
else
  echo " - using existing built JavaScript binutils for RISC-V targets"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

# Assembler (as)
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-as.js ]; then
  echo " - creating ${INSTALLDIR}/${HOST}-as.js"
  emcc -O2 `find ${ROOTDIR}/${BUILDJSDIR}/intl -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/zlib -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/opcodes -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libiberty -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libctf -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/bfd -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/gas -name \*.o ! -name as-bare.o` -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-as-bare.js -s WASM=1 -s ASSERTIONS=1 -lworkerfs.js -lfs.js -s NO_EXIT_RUNTIME=0 -s ALLOW_MEMORY_GROWTH=1 ${PROFILING_OPTS}
  cat lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-as-bare.js lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-as.js
else
  echo " - creating ${INSTALLDIR}/${HOST}-as.js (exists already)"
fi

# Linker (ld)
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-ld.js ]; then
  echo " - creating ${INSTALLDIR}/${HOST}-ld.js"
  emcc -O3 `find ${ROOTDIR}/${BUILDJSDIR}/intl -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/zlib -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/opcodes -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libiberty -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libctf -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/bfd -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/ld -name \*.o ! -name ldmain-bare.o ! -name \*testplug\*` -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-ld-bare.js -s WASM=1 -s ASSERTIONS=1 -lworkerfs.js -lfs.js -s NO_EXIT_RUNTIME=0 -s ALLOW_MEMORY_GROWTH=1 ${PROFILING_OPTS}
  cat lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-ld-bare.js lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-ld.js
else
  echo " - creating ${INSTALLDIR}/${HOST}-ld.js (exists already)"
fi

# Disassembler (objdump)
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-objdump.js ]; then
  echo " - creating ${INSTALLDIR}/${HOST}-objdump.js"
  emcc -O3 `find ${ROOTDIR}/${BUILDJSDIR}/intl -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/zlib -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/opcodes -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libiberty -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libctf -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/bfd -name \*.o` ${ROOTDIR}/${BUILDJSDIR}/binutils/objdump.o ${ROOTDIR}/${BUILDJSDIR}/binutils/{bucomm,dwarf,prdbg,debug,elfcomm,version,rddbg,filemode,stabs,rdcoff}.o -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-objdump-bare.js -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -s ASSERTIONS=1 -lworkerfs.js -lfs.js ${PROFILING_OPTS}
  cat lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-objdump-bare.js lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-objdump.js
else
  echo " - creating ${INSTALLDIR}/${HOST}-objdump.js (exists already)"
fi

# Metadata-reader (readelf)
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-readelf.js ]; then
  echo " - creating ${INSTALLDIR}/${HOST}-readelf.js"
  emcc -O3 `find ${ROOTDIR}/${BUILDJSDIR}/intl -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/zlib -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/opcodes -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libiberty -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/libctf -name \*.o` `find ${ROOTDIR}/${BUILDJSDIR}/bfd -name \*.o` ${ROOTDIR}/${BUILDJSDIR}/binutils/readelf.o ${ROOTDIR}/${BUILDJSDIR}/binutils/{unwind-ia64,dwarf,prdbg,debug,elfcomm,version,rddbg,filemode,stabs,rdcoff}.o -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-readelf-bare.js -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s ASSERTIONS=1 ${PROFILING_OPTS}
  cat lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-readelf-bare.js lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-readelf.js
else
  echo " - creating ${INSTALLDIR}/${HOST}-readelf.js (exists already)"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
