#!/bin/bash

# cross compiler toolchain version
RISCV_TOOLCHAIN_URL=https://github.com/riscv/riscv-gnu-toolchain
RISCV_TOOLCHAIN_VERSION=d45cfc68be6ce0a2f69daf66e64fc446224b3416

# newlib version
RISCV_NEWLIB_URL=https://github.com/riscv-collab/riscv-newlib
RISCV_NEWLIB_VERSION=riscv-newlib-3.2.0

# simulator version
RISCV_TINYEMU_URL=https://gitlab.com/wilkie/tinyemu
RISCV_TINYEMU_VERSION=3d8ceb12db0b9d82912fdd1d8f9437a886c0daa1
