#!/bin/bash

ROOTDIR=$PWD

echo ""
if [ ${RAWRS_COUNTER} ]; then
  echo "${RAWRS_COUNTER}. Building RISC-V kernel"
  echo "-------------------------"
else
  echo "Building RISC-V kernel"
  echo "======================"
fi
echo ""

if [ ! -f ${ROOTDIR}/scripts/riscv64/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

source $PWD/scripts/riscv64/versions.sh

UTILSDIR=utils/riscv64

SRCDIR=${ROOTDIR}/packages/riscv64/kernel
BUILDDIR=packages/riscv64/kernel

INSTALLDIR=assets/static/riscv64/kernel
INSTALLSRCDIR=assets/files/riscv64/kernel

# Assemble the kernel
if [ ! -f ${SRCDIR}/kernel.bin ]; then
  echo " - building RISC-V kernel"
  cd ${SRCDIR}
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make &> ${SRCDIR}/0-make.log
else
  echo " - using existing built RISC-V kernel"
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for kernel binaries"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for kernel binaries"
fi

# Install the kernel binaries
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/kernel.bin ]; then
  echo " - creating ${INSTALLDIR}/kernel.bin"
  cp ${SRCDIR}/kernel.bin ${ROOTDIR}/${INSTALLDIR}/kernel.bin
else
  echo " - creating ${INSTALLDIR}/kernel.bin (exists already)"
fi

# Install the kernel source

if [ ! -d ${ROOTDIR}/${INSTALLSRCDIR} ]; then
  echo " - creating ${INSTALLSRCDIR} for kernel binaries"
  mkdir -p ${ROOTDIR}/${INSTALLSRCDIR}
else
  echo " - installing to existing ${INSTALLSRCDIR} for kernel source"
fi

echo " - copying kernel source to ${INSTALLSRCDIR}"
cp ${SRCDIR}/*.s ${SRCDIR}/*.ld ${ROOTDIR}/${INSTALLSRCDIR}/.

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
