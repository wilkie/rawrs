#!/bin/bash

ROOTDIR=$PWD

echo ""
if [ ${RAWRS_COUNTER} ]; then
  echo "${RAWRS_COUNTER}. Building TinyEmu RISC-V Emulator"
  echo "-----------------------------------"
else
  echo "Building TinyEmu RISC-V Emulator"
  echo "================================"
fi
echo ""

if [ ! -f ${ROOTDIR}/scripts/riscv64/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

source $PWD/scripts/riscv64/versions.sh

UTILSDIR=utils/riscv64

SRCDIR=${ROOTDIR}/packages/riscv64/tinyemu
BUILDDIR=packages/riscv64/tinyemu

INSTALLDIR=assets/js/targets/riscv64/tinyemu

# Build TinyEmu
if [ ! -f ${SRCDIR}/js/riscvemu64.js ]; then
  # Initialize emscripten
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi

  echo " - building TinyEmu"
  cd ${SRCDIR}
  make -f Makefile.js &> ${SRCDIR}/1-make.log
else
  echo " - using existing built TinyEmu"
fi

# Create destination path
if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript TinyEmu build"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript TinyEmu build"
fi

# Install TinyEmu
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/riscvemu64.js ]; then
  echo " - installing JavaScript build of TinyEmu to ${INSTALLDIR}"
  cd ${SRCDIR}
  cp js/riscvemu64* ${ROOTDIR}/${INSTALLDIR}/.
else
  echo " - using existing installed Javascript TinyEmu"
fi

# Double check
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/riscvemu64.js ]; then
  echo " - ERROR: could not find installed TinyEmu in ${INSTALLDIR}"
  exit 1
fi

echo " - Javascript TinyEmu installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
