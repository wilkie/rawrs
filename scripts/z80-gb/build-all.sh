#!/bin/bash

ROOTDIR=$PWD

echo ""
echo "Building GameBoy Target"
echo "======================="

if [ ! -f ${ROOTDIR}/scripts/z80-gb/versions.sh ]; then
  echo ""
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

RAWRS_COUNTER=1 ./scripts/z80-gb/build-gputils.sh
RAWRS_COUNTER=2 ./scripts/z80-gb/build-sdcc.sh
RAWRS_COUNTER=3 ./scripts/z80-gb/build-gbdk.sh
RAWRS_COUNTER=4 ./scripts/z80-gb/build-visualboy.sh

echo ""
