#!/bin/bash

HEADER="Building GBDK"
TARGET=z80-gb
PACKAGE=gbdk

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/z80-gb/gbdk-2020
BUILDDIR=packages/z80-gb/gbdk-2020-build
BUILDJSDIR=packages/z80-gb/gbdk-2020-js

INSTALLDIR=assets/js/targets/z80-gb/gbdk
LIBINSTALLDIR=assets/static/z80-gb
STATICDIR=assets/files/z80-gb

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

CFLAGS="-DHAVE_PSIGNAL -m32 ${PROFILING_OPTS}"

echo " - installing/updating GB packages"
mkdir -p ${ROOTDIR}/packages/z80-gb
./scripts/z80-gb/install.sh &> ${ROOTDIR}/packages/z80-gb/install-during-gbdk.log

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying to the ${BUILDDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -d ${ROOTDIR}/${UTILSDIR}/opt/gbdk ]; then
  echo " - building a native gbdk for GameBoy targets"
  cd ${ROOTDIR}/${BUILDDIR}
  SDCCDIR=${ROOTDIR}/${UTILSDIR}/usr PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "make" "${ROOTDIR}/${BUILDDIR}/0-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native gbdk"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native gbdk for GameBoy targets"
  SDCCDIR=${ROOTDIR}/${UTILSDIR}/usr PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make TARGETDIR=${ROOTDIR}/${UTILSDIR}/opt/gbdk install &> ${ROOTDIR}/${BUILDDIR}/1-make-install.log
else
  echo " - using existing installed native gbdk for GameBoy targets"
fi

echo " - native gbdk installed to ${UTILSDIR}"

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - copying to the ${BUILDJSDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDJSDIR}

  echo " - patching lcc"
  cd ${ROOTDIR}/${BUILDJSDIR}
  patch -s -Np1 -i ${PATCHDIR}/gbdk-lcc.patch &> ${ROOTDIR}/${BUILDJSDIR}/patching.log

  cd ${ROOTDIR}/${BUILDJSDIR}/gbdk-support
  for file in `ls ${ROOTDIR}/${BUILDJSDIR}/gbdk-support`; do
    echo " - patching gbdk-support/${file}/Makefile to use emscripten emcc/em++"
    sed "s;gcc;emcc;" -i ${ROOTDIR}/${BUILDJSDIR}/gbdk-support/$file/Makefile
    sed "s;g++;em++;" -i ${ROOTDIR}/${BUILDJSDIR}/gbdk-support/$file/Makefile
  done
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

# Override 'strip'
mkdir -p ${ROOTDIR}/${BUILDJSDIR}/path
ln -fs /bin/true ${ROOTDIR}/${BUILDJSDIR}/path/strip

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gas/as.o ]; then
  echo " - building a JavaScript gbdk for GameBoy targets"
  cd ${ROOTDIR}/${BUILDJSDIR}
  SDCCDIR=${ROOTDIR}/${UTILSDIR}/usr PATH=${ROOTDIR}/${BUILDJSDIR}/path:${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "emmake make" "${ROOTDIR}/${BUILDJSDIR}/1-make.log"

  # Not sure if we really need to do this... but it helped at one point in my
  # life and I am not about to mess with it now.
  echo " - installing the JavaScript binaries for GameBoy targets"
  SDCCDIR=${ROOTDIR}/${UTILSDIR}/usr PATH=${ROOTDIR}/${BUILDJSDIR}/path:${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "emmake make TARGETDIR=${ROOTDIR}/${BUILDJSDIR}/wasm/opt/gbdk install" "${ROOTDIR}/${BUILDJSDIR}/2-make-install.log"
else
  echo " - using existing built JavaScript gbdk for GameBoy targets"
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

# We need JavaScript versions of several applications:
BINARIES="gbdk-support/bankpack/bankpack gbdk-support/lcc/lcc gbdk-support/gbcompress/gbcompress gbdk-support/ihxcheck/ihxcheck gbdk-support/png2asset/png2asset"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  subpath=$(dirname ${binary})
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/${subpath}
    rm -f ${binary}
    rm -f ${binary}.wasm
    SDCCDIR=${ROOTDIR}/${UTILSDIR}/usr PATH=${ROOTDIR}/${BUILDJSDIR}/path:${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "emmake make" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"
    context=0
    while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "\-o ${binary}.\+${binary}\.o" -e "${binary}\.o.\+-o ${binary}" -A${context}` == *\\ ]]; do
      context=$[$context + 1]
    done

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "\-o ${binary}.\+${binary}\.o" -e "${binary}\.o.\+-o ${binary}" -A${context} | sed "s;-o ${binary};${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${dest}.js (exists already)"
  fi
done

# Install system root (${UTILSDIR}/opt => assets/static/z80-gb/opt)

if [ ! -d ${ROOTDIR}/${LIBINSTALLDIR}/opt ]; then
  echo " - installing native gbdk for GameBoy targets to system root"
  cd ${ROOTDIR}/${BUILDDIR}
  SDCCDIR=${ROOTDIR}/${UTILSDIR}/usr PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make TARGETDIR=${ROOTDIR}/${LIBINSTALLDIR}/opt/gbdk install &> ${ROOTDIR}/${BUILDDIR}/2-install-assets-static.log

  echo " - removing PDF manual from system root"
  rm ${ROOTDIR}/${LIBINSTALLDIR}/opt/gbdk/*.pdf
fi

# Purge examples from system root
if [ -d ${ROOTDIR}/${LIBINSTALLDIR}/opt/gbdk/examples ]; then
  echo " - removing /opt/gbdk/examples from system root"
  rm -r ${ROOTDIR}/${LIBINSTALLDIR}/opt/gbdk/examples
fi

# Purge binaries from system root
if [ -d ${ROOTDIR}/${LIBINSTALLDIR}/opt/gbdk/bin ]; then
  echo " - removing /opt/gbdk/bin from system root"
  rm -r ${ROOTDIR}/${LIBINSTALLDIR}/opt/gbdk/bin
fi

# Compress system root /opt
if [ ! -f ${ROOTDIR}/${LIBINSTALLDIR}/gbdk.zip ]; then
  echo " - creating ${LIBINSTALLDIR}/gbdk.zip"
  cp -r ${ROOTDIR}/${LIBINSTALLDIR}/opt/* ${ROOTDIR}/${LIBINSTALLDIR}/usr/.
  cp -r ${ROOTDIR}/${LIBINSTALLDIR}/opt/gbdk/include/* ${ROOTDIR}/${LIBINSTALLDIR}/usr/share/sdcc/include/.
  cd ${ROOTDIR}/${LIBINSTALLDIR}/usr
  zip ../gbdk.zip -qr *
else
  echo " - creating ${LIBINSTALLDIR}/gbdk.zip (already exists)"
fi

# Install examples
mkdir -p ${ROOTDIR}/${STATICDIR}/examples

# All examples
EXAMPLES="banks bcd colorbar comm crash dscan emu_debug filltest galaxy gb-dtmf gbdecompress incbin irq isr_vector large_map lcd_isr_wobble linkerfile metasprites paint ram_function rand rpn samptest scroller sgb_border sgb_multiplayer sgb_pong simple_physics sound space"

# Filtered down
EXAMPLES="galaxy paint rand scroller sound space"
for name in ${EXAMPLES}; do
  if [ ! -d ${ROOTDIR}/${STATICDIR}/examples/${name} ]; then
    echo " - installing ${name} example"
    cp -r ${ROOTDIR}/${BUILDDIR}/gbdk-lib/examples/gb/${name} ${ROOTDIR}/${STATICDIR}/examples/.

    cd ${ROOTDIR}/${STATICDIR}

    # Remove Makefiles
    find . -name Makefile -exec rm {} \;

    # Remove any built gb files
    find . -name \*.gb -exec rm {} \;

    # Remove any built noi files (debugging info)
    find . -name \*.noi -exec rm {} \;

    # Remove any built map files
    find . -name \*.map -exec rm {} \;

    # Remove any markdown
    find . -name \*.md -exec rm {} \;
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
