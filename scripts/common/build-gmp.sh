#!/bin/bash

if [ ! ${GMP_VERSION} ]; then
  GMP_VERSION=6.1.2
fi
GMP_URL=https://ftp.gnu.org/gnu/gmp
GMP_FILENAME=gmp-${GMP_VERSION}.tar.xz

# Use INSTALLDIR to pick the destination path to install gmp
if [ ! ${INSTALLDIR} ]; then
  INSTALLDIR=packages/system
fi

ROOTDIR=$PWD

if [ ! ${QUIET} ]; then
  echo ""
  if [ ${RAWRS_COUNTER} ]; then
    echo "${RAWRS_COUNTER}. Building GMP library"
    echo "-----------------------"
  else
    echo "Building GMP library"
    echo "===================="
  fi
  echo ""
fi

if [ ! -f ${ROOTDIR}/scripts/common/build-gmp.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

PACKAGEDIR=packages/common
BUILDJSDIR=packages/common/gmp-${GMP_VERSION}-js
SRCDIR=${ROOTDIR}/${PACKAGEDIR}/gmp-${GMP_VERSION}

if [ ! -d ${ROOTDIR}/${PACKAGEDIR} ]; then
  echo " - creating ${PACKAGEDIR}"
  mkdir -p ${ROOTDIR}/${PACKAGEDIR}
fi

# Install
if [ ! -d ${ROOTDIR}/${PACKAGEDIR}/gmp-${GMP_VERSION} ]; then
  echo " - downloading gmp ${GMP_VERSION}"
  wget -q -nc ${GMP_URL}/${GMP_FILENAME} -O ${ROOTDIR}/${PACKAGEDIR}/${GMP_FILENAME}
  cd ${ROOTDIR}/${PACKAGEDIR}
  tar xf ${GMP_FILENAME}
  cd ${ROOTDIR}
fi

# Do not build if it exists
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/.libs/libgmp.a ]; then
  if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
    echo " - creating ${BUILDJSDIR} directory"
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}
  else
    echo " - warning: using existing build directory at ${BUILDJSDIR}"
  fi

  # Activate our emscripten environment
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi
  
  cd ${ROOTDIR}/${BUILDJSDIR}

  echo " - configuring JavaScript gmp ${GMP_VERSION}"
  emconfigure ${SRCDIR}/configure --prefix=/ \
    --disable-shared \
    --enable-static \
    --disable-assembly \
    --host none \
    --enable-cxx &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # The different flags we need to compile
  CFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  CPPFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  LDFLAGS="-Wl,--no-check-features -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0"

  echo " - building JavaScript gmp ${GMP_VERSION}"
  emmake make CXX=emcc CFLAGS="${CFLAGS}" CPPFLAGS="${CPPFLAGS}" LDFLAGS="${LDFLAGS}" PTHREAD_CFLAGS="" &> ${ROOTDIR}/${BUILDJSDIR}/2-make.log
else
  echo " - using existing built JavaScript GMP library"
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/lib/libgmp.a ]; then
  emmake make install DESTDIR=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/3-make-install.log
else
  echo " - using existing installed JavaScript GMP library"
fi

echo " - installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
