#!/bin/bash

if [ ! ${MPFR_VERSION} ]; then
  MPFR_VERSION=4.1.0
fi
MPFR_URL=https://ftp.gnu.org/gnu/mpfr
MPFR_FILENAME=mpfr-${MPFR_VERSION}.tar.xz

# Use INSTALLDIR to pick the destination path to install mpfr
if [ ! ${INSTALLDIR} ]; then
  INSTALLDIR=packages/system
fi

ROOTDIR=$PWD

if [ ! ${QUIET} ]; then
  echo ""
  if [ ${RAWRS_COUNTER} ]; then
    echo "${RAWRS_COUNTER}. Building MPFR library"
    echo "------------------------"
  else
    echo "Building MPFR library"
    echo "====================="
  fi
  echo ""
fi

if [ ! -f ${ROOTDIR}/scripts/common/build-mpfr.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

PACKAGEDIR=packages/common
BUILDJSDIR=packages/common/mpfr-${MPFR_VERSION}-js
SRCDIR=${ROOTDIR}/${PACKAGEDIR}/mpfr-${MPFR_VERSION}

if [ ! -d ${ROOTDIR}/${PACKAGEDIR} ]; then
  echo " - creating ${PACKAGEDIR}"
  mkdir -p ${ROOTDIR}/${PACKAGEDIR}
fi

# Install
if [ ! -d ${ROOTDIR}/${PACKAGEDIR}/mpfr-${MPFR_VERSION} ]; then
  echo " - downloading mpfr ${MPFR_VERSION}"
  wget -q -nc ${MPFR_URL}/${MPFR_FILENAME} -O ${ROOTDIR}/${PACKAGEDIR}/${MPFR_FILENAME}
  cd ${ROOTDIR}/${PACKAGEDIR}
  tar xf ${MPFR_FILENAME}
  cd ${ROOTDIR}
fi

# Do not build if it exists
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/src/.libs/libmpfr.a ]; then
  if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
    echo " - creating ${BUILDJSDIR} directory"
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}
  else
    echo " - warning: using existing build directory at ${BUILDJSDIR}"
  fi

  # Activate our emscripten environment
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi
  
  cd ${ROOTDIR}/${BUILDJSDIR}

  echo " - configuring JavaScript mpfr ${MPFR_VERSION}"
  emconfigure ${SRCDIR}/configure --prefix=/ \
    --disable-shared \
    --enable-static \
    --host none \
    --with-gmp=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # The different flags we need to compile
  CFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  CPPFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  LDFLAGS="-Wl,--no-check-features -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0"

  echo " - building JavaScript mpfr ${MPFR_VERSION}"
  emmake make CXX=emcc CFLAGS="${CFLAGS}" CPPFLAGS="${CPPFLAGS}" LDFLAGS="${LDFLAGS}" PTHREAD_CFLAGS="" &> ${ROOTDIR}/${BUILDJSDIR}/2-make.log
else
  echo " - using existing built JavaScript MPFR library"
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/lib/libmpfr.a ]; then
  emmake make install DESTDIR=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/3-make-install.log
else
  echo " - using existing installed JavaScript MPFR library"
fi

echo " - installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
