#!/bin/bash

if [ ! ${ZLIB_VERSION} ]; then
  ZLIB_VERSION=1.2.11
fi
ZLIB_URL=https://www.zlib.net
ZLIB_FILENAME=zlib-${ZLIB_VERSION}.tar.gz

# Use INSTALLDIR to pick the destination path to install zlib
if [ ! ${INSTALLDIR} ]; then
  INSTALLDIR=packages/system
fi

ROOTDIR=$PWD

if [ ! ${QUIET} ]; then
  echo ""
  if [ ${RAWRS_COUNTER} ]; then
    echo "${RAWRS_COUNTER}. Building zlib library"
    echo "------------------------"
  else
    echo "Building zlib library"
    echo "====================="
  fi
  echo ""
fi

if [ ! -f ${ROOTDIR}/scripts/common/build-zlib.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

PACKAGEDIR=packages/common
BUILDJSDIR=packages/common/zlib-${ZLIB_VERSION}-js
SRCDIR=${ROOTDIR}/${PACKAGEDIR}/zlib-${ZLIB_VERSION}

if [ ! -d ${ROOTDIR}/${PACKAGEDIR} ]; then
  echo " - creating ${PACKAGEDIR}"
  mkdir -p ${ROOTDIR}/${PACKAGEDIR}
fi

# Install
if [ ! -d ${ROOTDIR}/${PACKAGEDIR}/zlib-${ZLIB_VERSION} ]; then
  echo " - downloading zlib ${ZLIB_VERSION}"
  wget -q -nc ${ZLIB_URL}/${ZLIB_FILENAME} -O ${ROOTDIR}/${PACKAGEDIR}/${ZLIB_FILENAME}
  cd ${ROOTDIR}/${PACKAGEDIR}
  tar xf ${ZLIB_FILENAME}
  cd ${ROOTDIR}
fi

# Do not build if it exists
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/libz.so ]; then
  if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
    echo " - creating ${BUILDJSDIR} directory"
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}
  else
    echo " - warning: using existing build directory at ${BUILDJSDIR}"
  fi

  # Activate our emscripten environment
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi
  
  cd ${ROOTDIR}/${BUILDJSDIR}

  echo " - configuring JavaScript zlib ${ZLIB_VERSION}"
  emconfigure ${SRCDIR}/configure --prefix=/ &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # The different flags we need to compile
  CFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  CPPFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  LDFLAGS="-Wl,--no-check-features -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0"

  echo " - building JavaScript zlib ${ZLIB_VERSION}"
  emmake make CXX=emcc CFLAGS="${CFLAGS}" CPPFLAGS="${CPPFLAGS}" LDFLAGS="${LDFLAGS}" PTHREAD_CFLAGS="" &> ${ROOTDIR}/${BUILDJSDIR}/2-make.log
else
  echo " - using existing built JavaScript ZLIB library"
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/lib/libz.so ]; then
  emmake make install DESTDIR=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/3-make-install.log
else
  echo " - using existing installed JavaScript zlib library"
fi

echo " - installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
