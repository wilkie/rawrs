#!/bin/bash

if [ ! ${MPC_VERSION} ]; then
  MPC_VERSION=1.2.1
fi
MPC_URL=https://ftp.gnu.org/gnu/mpc
MPC_FILENAME=mpc-${MPC_VERSION}.tar.gz

# Use INSTALLDIR to pick the destination path to install mpc
if [ ! ${INSTALLDIR} ]; then
  INSTALLDIR=packages/system
fi

ROOTDIR=$PWD

if [ ! ${QUIET} ]; then
  echo ""
  if [ ${RAWRS_COUNTER} ]; then
    echo "${RAWRS_COUNTER}. Building MPC library"
    echo "-----------------------"
  else
    echo "Building MPC library"
    echo "===================="
  fi
  echo ""
fi

if [ ! -f ${ROOTDIR}/scripts/common/build-mpc.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

PACKAGEDIR=packages/common
BUILDJSDIR=packages/common/mpc-${MPC_VERSION}-js
SRCDIR=${ROOTDIR}/${PACKAGEDIR}/mpc-${MPC_VERSION}

if [ ! -d ${ROOTDIR}/${PACKAGEDIR} ]; then
  echo " - creating ${PACKAGEDIR}"
  mkdir -p ${ROOTDIR}/${PACKAGEDIR}
fi

# Install
if [ ! -d ${ROOTDIR}/${PACKAGEDIR}/mpc-${MPC_VERSION} ]; then
  echo " - downloading mpc ${MPC_VERSION}"
  wget -q -nc ${MPC_URL}/${MPC_FILENAME} -O ${ROOTDIR}/${PACKAGEDIR}/${MPC_FILENAME}
  cd ${ROOTDIR}/${PACKAGEDIR}
  tar xf ${MPC_FILENAME}
  cd ${ROOTDIR}
fi

# Do not build if it exists
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/src/.libs/libmpc.a ]; then
  if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
    echo " - creating ${BUILDJSDIR} directory"
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}
  else
    echo " - warning: using existing build directory at ${BUILDJSDIR}"
  fi

  # Activate our emscripten environment
  if [ ! ${EMSDK} ]; then
    echo " - initializing emcripten"
    source "$ROOTDIR/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
  else
    echo " - using existing activated emcripten at ${EMSDK}"
  fi
  
  cd ${ROOTDIR}/${BUILDJSDIR}

  echo " - configuring JavaScript mpc ${MPC_VERSION}"
  emconfigure ${SRCDIR}/configure --prefix=/ \
    --disable-shared \
    --enable-static \
    --disable-assembly \
    --host none \
    --with-gmp=${ROOTDIR}/${INSTALLDIR} \
    --with-mpfr=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # The different flags we need to compile
  CFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  CPPFLAGS="-DHAVE_PSIGNAL -m32 -s USE_PTHREADS=0" 
  LDFLAGS="-Wl,--no-check-features -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0"

  echo " - building JavaScript mpc ${MPC_VERSION}"
  emmake make CXX=emcc CFLAGS="${CFLAGS}" CPPFLAGS="${CPPFLAGS}" LDFLAGS="${LDFLAGS}" PTHREAD_CFLAGS="" &> ${ROOTDIR}/${BUILDJSDIR}/2-make.log
else
  echo " - using existing built JavaScript MPC library"
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/lib/libmpc.a ]; then
  emmake make install DESTDIR=${ROOTDIR}/${INSTALLDIR} &> ${ROOTDIR}/${BUILDJSDIR}/3-make-install.log
else
  echo " - using existing installed JavaScript MPC library"
fi

echo " - installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
