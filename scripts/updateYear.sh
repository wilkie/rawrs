#!/bin/bash

# Updates the year in the LGPL preambles

DATE=`date -I`
YEAR=${DATE:0:4}

find {lib,assets/js/rawrs} -name \*.rb | xargs sed -i "s/Copyright\s(C)\s[0-9]\{4\}-[0-9]\{4\}/Copyright (C) 2017-${YEAR}/"
sed -i "s/Copyright\s(C)\s[0-9]\{4\}-[0-9]\{4\}/Copyright (C) 2017-${YEAR}/" rawrs.rb
