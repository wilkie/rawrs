#!/bin/bash

HEADER="Building SNES SDK (libc)"
TARGET=65816-snes
PACKAGE=snes-sdk

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/${TARGET}/snes-sdk/libs
BUILDDIR=packages/${TARGET}/snes-sdk-libs-build

echo " - installing/updating packages"
mkdir -p ${ROOTDIR}/packages/${TARGET}
./scripts/${TARGET}/install.sh &> ${ROOTDIR}/packages/${TARGET}/install-during-${PACKAGE}.log

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying to the ${BUILDDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/lib/crt0_snes.obj ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - do not use python"
  sed "s;python ../tcc-65816/816-opt.py;${ROOTDIR}/${UTILSDIR}/usr/bin/816-opt;" -i Makefile 

  echo " - use binaries already installed than relative paths"
  sed "s;./../tcc-65816/include;${ROOTDIR}/${UTILSDIR}/usr/include;" -i Makefile
  sed "s;../tcc-65816;${ROOTDIR}/${UTILSDIR}/usr/bin;" -i Makefile
  sed "s;../wla_dx;${ROOTDIR}/${UTILSDIR}/usr/bin;" -i Makefile

  echo " - building a native ${PACKAGE} for Super Nintendo targets"

  run "make PREFIX=${ROOTDIR}/${UTILSDIR}/usr" "${ROOTDIR}/${BUILDDIR}/0-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native ${PACKAGE}"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native ${PACKAGE} for Super Nintendo targets"
  mkdir -p ${ROOTDIR}/${UTILSDIR}/usr/lib

  cp *.obj ${ROOTDIR}/${UTILSDIR}/usr/lib/.

  echo " - installing hdr.asm to ${UTILSDIR}/usr/include"
  cp hdr.asm ${ROOTDIR}/${UTILSDIR}/usr/include/.
else
  echo " - using existing installed native ${PACKAGE} for Super Nintendo targets"
fi

echo " - native ${PACKAGE} installed to ${UTILSDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
exit
