#!/bin/bash

CC65_URL=https://github.com/cc65/cc65
CC65_VERSION=10c1b050c787554c351d0f729205a81ab24303ef

SNES_SDK_URL=https://github.com/optixx/snes-sdk
SNES_SDK_VERSION=9fa04efab6b3c9817badf6c9ab2518e5572a6fba

CLASSIC_KONG_URL=https://github.com/nathancassano/classickong
CLASSIC_KONG_VERSION=6ee8707fba0f54391794537d9116898d4c9a6721

SNES9X_URL=https://github.com/snes9xgit/snes9x
SNES9X_VERSION=7e97bb59a1a65a95ad47549f52d301cc09dd8db5
