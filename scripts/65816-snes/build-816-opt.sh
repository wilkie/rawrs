#!/bin/bash

HEADER="Building 816-opt"
TARGET=65816-snes
PACKAGE=816-opt

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/${TARGET}/snes-sdk/tcc-65816
BUILDDIR=packages/${TARGET}/816-opt-build
BUILDJSDIR=packages/${TARGET}/816-opt-js

INSTALLDIR=assets/js/targets/${TARGET}/816-opt

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

echo " - installing/updating packages"
mkdir -p ${ROOTDIR}/packages/${TARGET}
./scripts/${TARGET}/install.sh &> ${ROOTDIR}/packages/${TARGET}/install-during-${PACKAGE}.log

# Create build path for building native 816-opt
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}

  echo " - copying 816-opt.c to ${BUILDDIR}"
  cp ${PATCHDIR}/816-opt.c ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/816-opt ]; then
  echo " - building a native 816-opt for Super Nintendo targets"

  cd ${ROOTDIR}/${BUILDDIR}
  run "gcc -o 816-opt 816-opt.c" "${ROOTDIR}/${BUILDDIR}/0-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native 816-opt"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native 816-opt for Super Nintendo targets"
  mkdir -p ${ROOTDIR}/${UTILSDIR}/usr/bin
  cp 816-opt ${ROOTDIR}/${UTILSDIR}/usr/bin/.
else
  echo " - using existing installed native tcc for Super Nintendo targets"
fi

echo " - native 816-opt installed to ${UTILSDIR}"

# Create build path for building native 816-opt
if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}

  echo " - copying 816-opt.c to ${BUILDJSDIR}"
  cp ${PATCHDIR}/816-opt.c ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/816-opt.js ]; then
  echo " - building a JavaScript 816-opt for Super Nintendo targets"

  cd ${ROOTDIR}/${BUILDJSDIR}
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
  run "emcc -o ${ROOTDIR}/${INSTALLDIR}/816-opt-bare.js 816-opt.c ${EMSCRIPTEN_WASM_OPTS}" "${ROOTDIR}/${BUILDJSDIR}/0-make.log"

  cat ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/816-opt-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/816-opt.js
else
  echo " - using existing built JavaScript 816-opt for Super Nintendo targets"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
exit
