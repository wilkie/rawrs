#!/bin/bash

HEADER="Building 816-tcc"
TARGET=65816-snes
PACKAGE=816-tcc

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/${TARGET}/snes-sdk/tcc-65816
BUILDDIR=packages/${TARGET}/816-tcc-build
BUILDJSDIR=packages/${TARGET}/816-tcc-js

INSTALLDIR=assets/js/targets/${TARGET}/816-tcc

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

echo " - installing/updating packages"
mkdir -p ${ROOTDIR}/packages/${TARGET}
./scripts/${TARGET}/install.sh &> ${ROOTDIR}/packages/${TARGET}/install-during-${PACKAGE}.log

echo " - patching ${PACKAGE}"

cd ${SRCDIR}
if [ ! -f ${SRCDIR}/patching.log ]; then
  patch -N -i ${PATCHDIR}/64-bit-and-python-3.patch &> ${SRCDIR}/patching.log
else
  echo " - patches already believed to be applied (remove patching.log to reapply)"
fi

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying to the ${BUILDDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/816-tcc ]; then
  echo " - configuring a native tcc for Super Nintendo targets"
  cd ${ROOTDIR}/${BUILDDIR}
  ./configure --prefix=${ROOTDIR}/${UTILSDIR}/usr &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native tcc for Super Nintendo targets"

  cd ${ROOTDIR}/${BUILDDIR}
  run "make 816-tcc" "${ROOTDIR}/${BUILDDIR}/1-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native tcc"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native tcc for Super Nintendo targets"
  mkdir -p ${ROOTDIR}/${UTILSDIR}/usr/bin
  cp 816-tcc ${ROOTDIR}/${UTILSDIR}/usr/bin/.
else
  echo " - using existing installed native tcc for Super Nintendo targets"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/include/stddef.h ]; then
  echo " - installing standard headers to ${UTILSDIR}/usr/include"

  cd ${ROOTDIR}/${BUILDDIR}
  mkdir -p ${ROOTDIR}/${UTILSDIR}/usr/include
  cp include/*.h ${ROOTDIR}/${UTILSDIR}/usr/include

  # Remove symlinks
  rm ${ROOTDIR}/${UTILSDIR}/usr/include/stdarg.h
  rm ${ROOTDIR}/${UTILSDIR}/usr/include/stddef.h

  # Copy those as well
  cp stdarg.h ${ROOTDIR}/${UTILSDIR}/usr/include
  cp stddef.h ${ROOTDIR}/${UTILSDIR}/usr/include
fi

echo " - native tcc installed to ${UTILSDIR}"

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - copying to the ${BUILDJSDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDJSDIR}

  echo " - patching tcc"
  #cd ${ROOTDIR}/${BUILDJSDIR}
  #patch -s -Np1 -i ${PATCHDIR}/gbdk-lcc.patch &> ${ROOTDIR}/${BUILDJSDIR}/patching.log
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/816-tcc ]; then
  echo " - configuring a JavaScript tcc for Super Nintendo targets"
  cd ${ROOTDIR}/${BUILDJSDIR}
  ./configure --prefix=/usr &> "${ROOTDIR}/${BUILDJSDIR}/1-configure.log"

  echo " - building a JavaScript tcc for Super Nintendo targets"
  run "emmake make 816-tcc CFLAGS=\"-O2 -Wno-pointer-sign\" CC=emcc CXX=em++" "${ROOTDIR}/${BUILDJSDIR}/2-make.log"
else
  echo " - using existing built JavaScript tcc for Super Nintendo targets"
fi

BINARIES="816-tcc"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}
    rm -f ${binary}
    rm -f ${binary}.wasm
    run "emmake make CFLAGS=\"-O2 -Wno-pointer-sign ${EMSCRIPTEN_WASM_OPTS}\" EXESUF=\"-bare.js\" CC=emcc CXX=em++ ${binary}-bare.js" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"

    mkdir -p ${ROOTDIR}/${INSTALLDIR}
    cp ${ROOTDIR}/${BUILDJSDIR}/${binary}-bare.js ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js
    cp ${ROOTDIR}/${BUILDJSDIR}/${binary}-bare.wasm ${ROOTDIR}/${INSTALLDIR}/.

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${dest}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
exit
