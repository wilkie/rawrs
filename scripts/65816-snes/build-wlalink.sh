#!/bin/bash

HEADER="Building wlalink"
TARGET=65816-snes
PACKAGE=wlalink

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/${TARGET}/snes-sdk/wla_dx/wlalink
BUILDDIR=packages/${TARGET}/wlalink-build
BUILDJSDIR=packages/${TARGET}/wlalink-js

INSTALLDIR=assets/js/targets/${TARGET}/wlalink

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

echo " - installing/updating packages"
mkdir -p ${ROOTDIR}/packages/${TARGET}
./scripts/${TARGET}/install.sh &> ${ROOTDIR}/packages/${TARGET}/install-during-${PACKAGE}.log

echo " - patching ${PACKAGE}"

cd ${SRCDIR}
if [ ! -f ${SRCDIR}/patching.log ]; then
  patch -N -i ${PATCHDIR}/fastrom-check.patch &> ${SRCDIR}/patching.log
else
  echo " - patches already believed to be applied (remove patching.log to reapply)"
fi

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying to the ${BUILDDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/wlalink ]; then
  echo " - configuring a native ${PACKAGE} for Super Nintendo targets"
  cd ${ROOTDIR}/${BUILDDIR}
  ./configure &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native ${PACKAGE} for Super Nintendo targets"

  cd ${ROOTDIR}/${BUILDDIR}
  run "make PREFIX=${ROOTDIR}/${UTILSDIR}/usr" "${ROOTDIR}/${BUILDDIR}/1-make.log"

  # Install
  if [ ! -d ${ROOTDIR}/${UTILSDIR} ]; then
    echo " - creating ${UTILSDIR} to install native ${PACKAGE}"
    mkdir -p ${ROOTDIR}/${UTILSDIR}
  fi

  echo " - installing native ${PACKAGE} for Super Nintendo targets"
  mkdir -p ${ROOTDIR}/${UTILSDIR}/usr/bin
  cp wlalink ${ROOTDIR}/${UTILSDIR}/usr/bin/.
else
  echo " - using existing installed native ${PACKAGE} for Super Nintendo targets"
fi

echo " - native ${PACKAGE} installed to ${UTILSDIR}"

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - copying to the ${BUILDJSDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/asdf ]; then
  echo " - configuring a JavaScript ${PACKAGE} for Super Nintendo targets"
  cd ${ROOTDIR}/${BUILDJSDIR}
  ./configure &> "${ROOTDIR}/${BUILDJSDIR}/1-configure.log"

  echo " - building a JavaScript ${PACKAGE} for Super Nintendo targets"
  run "emmake make PREFIX=/usr CC=emcc LD=emcc CXX=em++" "${ROOTDIR}/${BUILDJSDIR}/2-make.log"
else
  echo " - using existing built JavaScript ${PACKAGE} for Super Nintendo targets"
fi

BINARIES="wlalink"

for binary in ${BINARIES}
do
  dest=$(basename ${binary})
  binary=$(basename ${binary})

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}
    rm -f ${binary}
    rm -f ${binary}.wasm
    run "emmake make PREFIX=/usr CC=emcc LD=emcc CXX=em++ ${binary}" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"

    # Creates *-bare.js
    mkdir -p ${ROOTDIR}/${INSTALLDIR}
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep -e "\-o ${binary}" | sed "s;-o ${binary};${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    mkdir -p ${ROOTDIR}/${INSTALLDIR}
    cd ${ROOTDIR}/${BUILDJSDIR}
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${dest}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
exit
