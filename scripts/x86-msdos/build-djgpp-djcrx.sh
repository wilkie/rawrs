#!/bin/bash

HEADER="Building MS-DOS djgpp-djcrx"
TARGET=x86-msdos
PACKAGE=djgpp-djcrx

# The host target we are building
HOST="i586-pc-msdosdjgpp"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/x86-msdos/djgpp-djcrx-${DJGPP_DJCRX_VERSION}
BUILDDIR=packages/x86-msdos/djgpp-djcrx-${DJGPP_DJCRX_VERSION}-build
BUILDJSDIR=packages/x86-msdos/djgpp-djcrx-${DJGPP_DJCRX_VERSION}-js

INSTALLSTATICDIR=assets/static/x86-msdos
INSTALLDIR=assets/js/targets/x86-msdos/djgpp-djcrx

echo " - installing/updating MS-DOS packages"
./scripts/x86-msdos/install.sh &> ${ROOTDIR}/packages/x86-msdos/install-during-djgpp-djcrx.log

echo " - patching djgpp-djcrx"

cd ${SRCDIR}

if [ ! -f ${SRCDIR}/patching.log ]; then
  sed -i "s/i586-pc-msdosdjgpp/$HOST/" src/makefile.def src/dxe/makefile.dxe &> ${SRCDIR}/patching.log
  sed -i 's/ln/ln -f/' src/dxe/makefile.dxe &>> ${SRCDIR}/patching.log

  # fix build with gcc >= 8 
  patch -s -Np1 < ${PATCHDIR}/djgpp-djcrx-gcccompat.patch &>> ${SRCDIR}/patching.log

  # gcc provides its own float.h which masks this one
  ln -fs float.h include/djfloat.h
  sed -i 's/<float\.h>/<djfloat.h>/' src/libc/{go32/dpmiexcp,emu387/npxsetup}.c src/utils/redir.c &>> ${SRCDIR}/patching.log

  # fix libc bugs
  patch -s -Np0 < ${PATCHDIR}/ttyscrn.patch &>> ${SRCDIR}/patching.log
  patch -s -Np0 < ${PATCHDIR}/nmemalign.patch &>> ${SRCDIR}/patching.log
  patch -s -Np0 < ${PATCHDIR}/fseeko64.patch &>> ${SRCDIR}/patching.log
  patch -s -Np0 < ${PATCHDIR}/asm.patch &>> ${SRCDIR}/patching.log

  # allow using dxe3gen without DJDIR and without dxe3res in PATH
  patch -s -Np0 < ${PATCHDIR}/dxegen.patch &>> ${SRCDIR}/patching.log

  # Fix use of 'register' class specifier
  sed "s;register ;;" -i ${SRCDIR}/src/libemu/src/emu387.cc

  # be verbose
  sed -i '/@$(MISC) echo - / d; s/^\t@/\t/' ${SRCDIR}/src/makefile.inc ${SRCDIR}/src/libc/makefile ${SRCDIR}/src/utils/makefile
else
  echo " - patches already believed to be applied (remove patching.log to reapply)"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying empty build to ${BUILDDIR}"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${BUILDDIR}/hostbin/stubify.exe ]; then
  echo " - building native build for msdos targets"
  cd ${ROOTDIR}/${BUILDDIR}
  cd src

  # Probably should patch these instead (some are, but some are now missing)
  # FULLSCR=1 is a weird one to get the debug source tree to compile...
  # Pretty much all of these are static analysis checks that are CONCERNING but
  # not the big of a deal... probably. until they are. haha. ha.
  EXTRA_CFLAGS="-Wno-free-nonheap-object -Wno-stringop-truncation -DFULLSCR=1 -Wno-array-bounds -Wno-nonnull-compare -Wno-format-overflow -fno-inline"

  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make clean &> ${ROOTDIR}/${BUILDDIR}/0-make-clean.log
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "make EXTRA_CFLAGS=\"${EXTRA_CFLAGS}\"" "${ROOTDIR}/${BUILDDIR}/1-make.log"

  cd dxe
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "make -f makefile.dxe" "${ROOTDIR}/${BUILDDIR}/2-make-dxe.log"
else
  echo " - using existing build for msdos targets (remove hostbin/stubify.exe to rebuild)"
fi

# Install to our sys root
if [ ! -d ${ROOTDIR}/${INSTALLSTATICDIR} ]; then
  echo " - creating ${INSTALLSTATICDIR} directory"
  mkdir -p ${ROOTDIR}/${INSTALLSTATICDIR}
fi

if [ ! -d ${ROOTDIR}/${INSTALLSTATICDIR}/usr ]; then
  echo " - creating ${INSTALLSTATICDIR}/usr directory"
  mkdir -p ${ROOTDIR}/${INSTALLSTATICDIR}/usr
fi

if [ ! -d ${ROOTDIR}/${INSTALLSTATICDIR}/usr/${HOST} ]; then
  echo " - creating ${INSTALLSTATICDIR}/usr/${HOST} directory"
  mkdir -p ${ROOTDIR}/${INSTALLSTATICDIR}/usr/${HOST}
fi

if [ ! -d ${ROOTDIR}/${INSTALLSTATICDIR}/usr/${HOST}/sys-include ]; then
  echo " - creating ${INSTALLSTATICDIR}/usr/${HOST}/sys-include directory"
  mkdir -p ${ROOTDIR}/${INSTALLSTATICDIR}/usr/${HOST}/sys-include
fi

if [ ! -d ${ROOTDIR}/${INSTALLSTATICDIR}/usr/${HOST}/lib ]; then
  echo " - creating ${INSTALLSTATICDIR}/usr/${HOST}/lib directory"
  mkdir -p ${ROOTDIR}/${INSTALLSTATICDIR}/usr/${HOST}/lib
fi

cd ${ROOTDIR}/${BUILDDIR}
echo " - copying header files to ${INSTALLSTATICDIR}/usr/${HOST}/sys-include"
cp -r include/* ${ROOTDIR}/${INSTALLSTATICDIR}/usr/$HOST/sys-include

echo " - copying libc and system libraries to ${INSTALLSTATICDIR}/usr/${HOST}/lib"
cp -r lib/* ${ROOTDIR}/${INSTALLSTATICDIR}/usr/$HOST/lib

#cd ${ROOTDIR}/${BUILDDIR}/hostbin
#for _file in djasm mkdoc stubedit stubify; do
#  install -m0755 $_file.exe "$pkgdir"/usr/$HOST/bin/$_file
#  ln -s ../$HOST/bin/$_file "$pkgdir"/usr/bin/$HOST-$_file
#done

#cd ${ROOTDIR}/${BUILDDIR}/src/dxe
#for _file in dxe3gen dxe3res; do
#  install -m0755 $_file "$pkgdir"/usr/$HOST/bin/$_file
#  ln -s ../$HOST/bin/$_file "$pkgdir"/usr/bin/$HOST-$_file
#done
#ln -s dxe3gen "$pkgdir"/usr/$HOST/bin/dxegen

# Compile options

PROFILING_OPTS="-O2"

# Uncomment to gain stack traces
#PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1  -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0 ${PROFILING_OPTS}"

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

# Create JavaScript build path
if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - copying empty build to ${BUILDJSDIR}"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

# Create destination path for JavaScript workers
if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} directory"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

# Compile JavaScript versions of stubify and stubedit
BINARIES="stubify stubedit"
for binary in ${BINARIES}
do
  dest=${binary}

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/src/stub

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDDIR}/1-make.log | grep "\-o ../../hostbin/${binary}.exe" | sed "s;gcc;emcc;" | sed "s;-o ../../hostbin/${binary}.exe;${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh
    source ${ROOTDIR}/${BUILDJSDIR}/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js (exists already)"
  fi
done

# Get DPMI server
# git clone https://gitlab.com/FreeDOS/util/cwsdpmi

# Create zip file of root
if [ ! -f ${ROOTDIR}/${INSTALLSTATICDIR}/root.zip ]; then
  echo " - creating ${INSTALLSTATICDIR}/root.zip"
  cd ${ROOTDIR}/${INSTALLSTATICDIR}/usr
  zip ../root.zip -qr ${HOST}/
else
  echo " - creating ${INSTALLSTATICDIR}/root.zip (already exists)"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
