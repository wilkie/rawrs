#!/bin/bash

HEADER="Building Examples"
TARGET=x86-msdos
PACKAGE=examples

source $PWD/scripts/common/include.sh

SRCDIR=${ROOTDIR}/packages/${TARGET}

INSTALLDIR=assets/files/x86-msdos/examples

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/cursecga ]; then
  echo " - creating ${INSTALLDIR}/cursecga directory"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}/cursecga
else
  echo " - warning: using existing build directory at ${INSTALLDIR}/cursecga"
fi

# Copy files
echo " - copying /src/*.{c,h}"
cp ${SRCDIR}/the-curse-of-cga/src/*.{c,h} ${ROOTDIR}/${INSTALLDIR}/cursecga/.
echo " - copying /README.md"
cp ${SRCDIR}/the-curse-of-cga/README.md ${ROOTDIR}/${INSTALLDIR}/cursecga/.
echo " - removing /root/"
rm -rf ${ROOTDIR}/${INSTALLDIR}/cursecga/root
echo " - making /root/ directory"
mkdir -p ${ROOTDIR}/${INSTALLDIR}/cursecga/root
echo " - copying /ASSETS/ to /root/"
cp -r ${SRCDIR}/the-curse-of-cga/ASSETS ${ROOTDIR}/${INSTALLDIR}/cursecga/root/.

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
