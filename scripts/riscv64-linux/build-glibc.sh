#!/bin/bash

HEADER="Building RISC-V Linux glibc"
TARGET=riscv64-linux
PACKAGE=glibc

# The host target we are building
HOST="riscv64-unknown-linux-gnu"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/riscv64/riscv-gnu-toolchain/riscv-glibc
BUILDDIR=packages/riscv64-linux/riscv-glibc-build

INSTALLDIR=assets/static/riscv64-linux

if [ ! ${QUIET} ]; then
  echo " - installing/updating RISC-V Linux packages"
  ./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-glibc.log
fi

# Create build path for building native risc-v glibc
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# I am going to borrow the arch-linux configure options for this
STRIP_BINARIES="--strip-all"
STRIP_STATIC="--strip-debug"
STRIP_SHARED="--strip-unneeded"

_configure_flags=(
    --prefix=/usr
    --host=${HOST}
    --build=`gcc -dumpmachine`
    --target=${HOST}
    --with-headers=${ROOTDIR}/${UTILSDIR}/usr/include
    --enable-add-ons
    --enable-bind-now
    --enable-lock-elision
    --enable-stack-protector=strong
    --enable-stackguard-randomization
		--enable-shared
		--enable-obsolete-rpc
		--enable-kernel=3.0.0
    --disable-profile
    --disable-werror
)

# Compile native risc-v glibc
if [ ! -f ${ROOTDIR}/${BUILDDIR}/libc.so ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo "slibdir=/usr/lib" >> configparms
  echo "rtlddir=/usr/lib" >> configparms
  echo "sbindir=/usr/bin" >> configparms
  echo "rootsbindir=/usr/bin" >> configparms

  echo " - configuring a native glibc for RISC-V Linux targets"
  "${SRCDIR}/configure" \
      --libdir=/usr/lib \
      --libexecdir=/usr/lib \
      ${_configure_flags[@]} &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  # build libraries with fortify disabled
  echo "build-programs=no" >> configparms
  echo " - building a native glibc for RISC-V Linux targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/1-make.log"

  # re-enable fortify for programs
  sed -i "/build-programs=/s#no#yes#" configparms

  echo " - building native glibc programs for RISC-V Linux targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/2-make-programs.log"

  cd ${ROOTDIR}
else
  echo " - using native build of RISC-V Linux glibc in ${BUILDDIR}"
fi

# Install native risc-v glibc
if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/lib/crt1.o ]; then
  echo " - installing native build of RISC-V Linux glibc to ${UTILSDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  run "make install_root=${ROOTDIR}/${UTILSDIR} install" "${ROOTDIR}/${BUILDDIR}/3-make-install.log"
  rm -f "${ROOTDIR}"/${UTILSDIR}/etc/ld.so.{cache,conf}

  cd ${SRCDIR}

  install -dm755 "${ROOTDIR}"/${UTILSDIR}/etc
  touch "${ROOTDIR}"/${UTILSDIR}/etc/ld.so.conf

  install -dm755 "${ROOTDIR}"/${UTILSDIR}/usr
  install -dm755 "${ROOTDIR}"/${UTILSDIR}/usr/lib
  install -dm755 "${ROOTDIR}"/${UTILSDIR}/usr/lib/{locale,systemd/system,tmpfiles.d}
  install -m644 nscd/nscd.conf "${ROOTDIR}"/${UTILSDIR}/etc/nscd.conf
  install -m644 nscd/nscd.service "${ROOTDIR}"/${UTILSDIR}/usr/lib/systemd/system
  install -m644 nscd/nscd.tmpfiles "${ROOTDIR}"/${UTILSDIR}/usr/lib/tmpfiles.d/nscd.conf
  install -dm755 "${ROOTDIR}"/${UTILSDIR}/var/db/nscd

  install -m644 posix/gai.conf "${ROOTDIR}"/${UTILSDIR}/etc/gai.conf

  install -dm755 "${ROOTDIR}"/${UTILSDIR}/usr/bin
  install -m755 "${PATCHDIR}/locale-gen" "${ROOTDIR}"/${UTILSDIR}/usr/bin

  # Create /etc/locale.gen
  install -m644 "${PATCHDIR}/locale.gen.txt" "${ROOTDIR}"/${UTILSDIR}/etc/locale.gen
  sed -e '1,3d' -e 's|/| |g' -e 's|\\| |g' -e 's|^|#|g' \
    "${SRCDIR}/localedata/SUPPORTED" >> "${ROOTDIR}"/${UTILSDIR}/etc/locale.gen

  #if check_option 'debug' n; then
  find "${ROOTDIR}"/${UTILSDIR}/usr/bin -type f -executable -exec ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-strip $STRIP_BINARIES {} + 2> /dev/null || true
  find "${ROOTDIR}"/${UTILSDIR}/usr/lib -name '*.a' -type f -exec ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-strip $STRIP_STATIC {} + 2> /dev/null || true

  # Do not strip these for gdb and valgrind functionality, but strip the rest
  find "${ROOTDIR}"/${UTILSDIR}/usr/lib \
    -not -name 'ld-*.so' \
    -not -name 'libc-*.so' \
    -not -name 'libpthread-*.so' \
    -not -name 'libthread_db-*.so' \
    -name '*-*.so' -type f -exec ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-strip $STRIP_SHARED {} + 2> /dev/null || true

  cd ${ROOTDIR}
else
  echo " - using existing installed native glibc for RISC-V Linux targets"
fi

echo " - native glibc for RISC-V Linux targets installed to ${UTILSDIR}"

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/usr/lib/crt1.o ]; then
  echo " - installing build of RISC-V glibc to ${INSTALLDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  run "make install_root=${ROOTDIR}/${INSTALLDIR} install" "${ROOTDIR}/${BUILDDIR}/4-make-install-assets.log"
  rm -f "${ROOTDIR}"/${INSTALLDIR}/etc/ld.so.{cache,conf}

  cd ${SRCDIR}

  install -dm755 "${ROOTDIR}"/${INSTALLDIR}/etc
  touch "${ROOTDIR}"/${INSTALLDIR}/etc/ld.so.conf

  install -dm755 "${ROOTDIR}"/${INSTALLDIR}/usr/lib/{locale,systemd/system,tmpfiles.d}
  install -m644 nscd/nscd.conf "${ROOTDIR}"/${INSTALLDIR}/etc/nscd.conf
  install -m644 nscd/nscd.service "${ROOTDIR}"/${INSTALLDIR}/usr/lib/systemd/system
  install -m644 nscd/nscd.tmpfiles "${ROOTDIR}"/${INSTALLDIR}/usr/lib/tmpfiles.d/nscd.conf
  install -dm755 "${ROOTDIR}"/${INSTALLDIR}/var/db/nscd

  install -m644 posix/gai.conf "${ROOTDIR}"/${INSTALLDIR}/etc/gai.conf

  install -dm755 "${ROOTDIR}"/${INSTALLDIR}/usr/bin
  install -m755 "${PATCHDIR}/locale-gen" "${ROOTDIR}"/${INSTALLDIR}/usr/bin/locale-gen

  # Create /etc/locale.gen
  install -m644 "${PATCHDIR}/locale.gen.txt" "${ROOTDIR}"/${INSTALLDIR}/etc/locale.gen
  sed -e '1,3d' -e 's|/| |g' -e 's|\\| |g' -e 's|^|#|g' \
    "${SRCDIR}/localedata/SUPPORTED" >> "${ROOTDIR}"/${INSTALLDIR}/etc/locale.gen

  #if check_option 'debug' n; then
  find "${ROOTDIR}"/${INSTALLDIR}/usr/bin -type f -executable -exec ${ROOTDIR}/${INSTALLDIR}/usr/bin/${HOST}-strip $STRIP_BINARIES {} + 2> /dev/null || true
  find "${ROOTDIR}"/${INSTALLDIR}/usr/lib -name '*.a' -type f -exec ${ROOTDIR}/${INSTALLDIR}/usr/bin/${HOST}-strip $STRIP_STATIC {} + 2> /dev/null || true

  # Do not strip these for gdb and valgrind functionality, but strip the rest
  find "${ROOTDIR}"/${INSTALLDIR}/usr/lib \
    -not -name 'ld-*.so' \
    -not -name 'libc-*.so' \
    -not -name 'libpthread-*.so' \
    -not -name 'libthread_db-*.so' \
    -name '*-*.so' -type f -exec ${ROOTDIR}/${INSTALLDIR}/usr/bin/${HOST}-strip $STRIP_SHARED {} + 2> /dev/null || true

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/usr/lib/crt1.o ]; then
    echo " - ERROR: could not install RISC-V glibc"
    exit 1
  fi
else
  echo " - using existing installed RISC-V glibc"
fi

rm -rf ${ROOTDIR}/${INSTALLDIR}/usr/bin/*

echo " - native glibc for RISC-V Linux targets also installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
