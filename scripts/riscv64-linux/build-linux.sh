#!/bin/bash

HEADER="Building RISC-V Linux"
TARGET=riscv64-linux
PACKAGE=linux

# The host target we are building
HOST="riscv64-unknown-linux-gnu"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/riscv64-linux/linux-${LINUX_VERSION}
BUILDDIR=packages/riscv64-linux/linux-${LINUX_VERSION}

INSTALLDIR=assets/static/riscv64-linux

echo " - installing/updating RISC-V packages"
./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-linux.log

# Compile native risc-v linux
if [ ! -f ${ROOTDIR}/${BUILDDIR}/arch/riscv/boot/Image ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - configuring a native Linux for RISC-V targets"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- defconfig &> ${ROOTDIR}/${BUILDDIR}/0-make-defconfig.log

  echo " - copying our configuration inside"
  if [ -f ${PATCHDIR}/config-${LINUX_VERSION} ]; then
    cp ${PATCHDIR}/config-${LINUX_VERSION} ${ROOTDIR}/${BUILDDIR}/.config
  else
    cp `ls ${PATCHDIR}/config-* | head -1` ${ROOTDIR}/${BUILDDIR}/.config
  fi

  echo " - building a native Linux for RISC-V targets"
  PATH=${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH run "make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc)" "${ROOTDIR}/${BUILDDIR}/1-make.log"

  cd ${ROOTDIR}
else
  echo " - using native build of Linux for RISC-V targets in ${BUILDDIR}"
fi

# Install native risc-v linux
if [ ! -f ${ROOTDIR}/${INSTALLDIR}/boot/Image ]; then
  echo " - installing native build of RISC-V Linux to ${INSTALLDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  cp -r arch/riscv/boot ${ROOTDIR}/${INSTALLDIR}
  cd ${ROOTDIR}
else
  echo " - using existing installed native Linux for RISC-V targets"
fi

echo " - native Linux installed to ${INSTALLDIR}"

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
