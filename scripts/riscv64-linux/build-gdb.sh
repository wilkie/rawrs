#!/bin/bash

HEADER="Building RISC-V gdb for Linux"
TARGET=riscv64-linux
PACKAGE=gdb

# The host target we are building
HOST="riscv64-unknown-linux-gnu"

source $PWD/scripts/common/include.sh

SRCDIR=${ROOTDIR}/packages/riscv64/riscv-gnu-toolchain/riscv-gdb
JSSRCDIR=${ROOTDIR}/packages/riscv64-linux/riscv-gnu-toolchain/riscv-gdb-js-src
BUILDDIR=packages/riscv64-linux/riscv-gnu-toolchain/riscv-gdb-build
BUILDJSDIR=packages/riscv64-linux/riscv-gnu-toolchain/riscv-gdb-js

# Override patchdir
PATCHDIR=${ROOTDIR}/patches/riscv64/gdb

INSTALLDIR=assets/js/targets/riscv64-linux/gdb

echo " - installing/updating RISC-V for Linux packages"
mkdir -p ${ROOTDIR}/packages/riscv64-linux
./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-gdb.log

# Create build path for building native risc-v gdb
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Compile native risc-v gdb
if [ ! -f ${ROOTDIR}/${BUILDDIR}/bfd/doc/chew ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - configuring a native gdb for RISC-V Linux targets"
  ${SRCDIR}/configure --prefix /usr --target=${HOST} --disable-nls --enable-64-bit-bfd &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native gdb for RISC-V Linux targets"
  run "make" "${ROOTDIR}/${BUILDDIR}/1-make.log"
  cd ${ROOTDIR}
else
  echo " - using native build of RISC-V Linux gdb in ${BUILDDIR}"
fi

# Install native risc-v gdb
if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-gdb ]; then
  echo " - installing native build of RISC-V Linux gdb to ${UTILSDIR}"
  cd ${ROOTDIR}/${BUILDDIR}
  make install DESTDIR=${ROOTDIR}/${UTILSDIR} &> ${ROOTDIR}/${BUILDDIR}/2-make-install.log
  cd ${ROOTDIR}
else
  echo " - using existing installed native gdb for RISC-V Linux targets"
fi

echo " - native gdb installed to ${UTILSDIR}"

PROFILING_OPTS=

# Uncomment to gain stack traces
#PROFILING_OPTS=-g --profiling-funcs

# Create build path for JavaScript build of risc-v gdb
if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

# Initialize emscripten
if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

cd ${ROOTDIR}/${BUILDJSDIR}

# The different flags we need to compile
CFLAGS="-DHAVE_PSIGNAL -m32 ${PROFILING_OPTS} -s DISABLE_EXCEPTION_CATCHING=0 -fexceptions -s USE_PTHREADS=0 -include limits.h"
CPPFLAGS="${CFLAGS}"
LDFLAGS="-Wl,--no-check-features -s ALLOW_MEMORY_GROWTH=1 -s DISABLE_EXCEPTION_CATCHING=0 -s USE_PTHREADS=0"

# Build the JavaScript gdb for risc-v
if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gdb/gdb.o ]; then
  if [ ! -d ${JSSRCDIR} ]; then
    echo " - copying over source tree"
    cp -r ${SRCDIR} ${JSSRCDIR}
  fi

  # Patch
  echo " - patching gdb"

  cd ${JSSRCDIR}

  if [ ! -f ${JSSRCDIR}/patching.log ]; then
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten.patch &> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-readline.patch &>> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-fix-maint.patch &>> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-remote-jsinvoke.patch &>> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-get_tty_state.patch &>> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-removes_warnings.patch &>> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-jsinvoke_prompt.patch &>> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-no-wait-for-continue.patch &>> ${JSSRCDIR}/patching.log
    patch -N -p1 < ${PATCHDIR}/gdb-emscripten-no-pthread.patch &>> ${JSSRCDIR}/patching.log
  else
    echo " - patches already believed to be applied (remove patching.log to reapply)"
  fi

  # Configure
  cd ${ROOTDIR}/${BUILDJSDIR}
  echo " - configuring a JavaScript gdb for RISC-V Linux targets"
  emconfigure ${JSSRCDIR}/configure CFLAGS="${CFLAGS}" --target=${HOST} --prefix /usr \
    --disable-nls \
    --enable-lto \
    --disable-install-libiberty \
    --disable-multilib &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  # Build first pass
  echo " - copying over native generators needed during compilation"

  COPIES="bfd/doc/chew bfd/doc/chew.stamp binutils/sysinfo sim/riscv/gentmap"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  echo " - building a JavaScript gdb for RISC-V Linux targets"
  cd ${ROOTDIR}/${BUILDJSDIR}
  run "emmake make CXX=emcc CFLAGS=\"${CFLAGS}\" CPPFLAGS=\"${CPPFLAGS}\" LDFLAGS=\"${LDFLAGS}\" PTHREAD_CFLAGS=\"\"" "${ROOTDIR}/${BUILDJSDIR}/2-make.log"
else
  echo " - using existing built JavaScript gdb for RISC-V Linux targets"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-gdb.js ]; then
  echo " - creating ${INSTALLDIR}/${HOST}-gdb.js"
  cd ${ROOTDIR}/${BUILDJSDIR}/gdb
  em++ -s USE_PTHREADS=0 -s DISABLE_EXCEPTION_CATCHING=0 -s FORCE_FILESYSTEM=1 -s 'EXPORT_NAME="GDB"' -s "EXPORTED_FUNCTIONS=['_jsmain','_jsstep','_jsinvoke']" -s "EXPORTED_RUNTIME_METHODS=['FS','ccall','cwrap','getValue','setValue']" -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 -s MODULARIZE=1 -s NO_EXIT_RUNTIME=1 -lworkerfs.js -lfs.js -s ASSERTIONS=1 -O2   -Wl,--no-check-features -s ERROR_ON_UNDEFINED_SYMBOLS=0 \ -O2   -Wl,--no-check-features           -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-gdb-bare.js ada-exp.o ada-lang.o ada-tasks.o ada-typeprint.o ada-valprint.o ada-varobj.o addrmap.o agent.o alloc.o annotate.o arch-utils.o arch/riscv.o async-event.o auto-load.o auxv.o ax-gdb.o ax-general.o bcache.o bfd-target.o block.o blockframe.o break-catch-sig.o break-catch-syscall.o break-catch-throw.o breakpoint.o btrace.o build-id.o buildsym-legacy.o buildsym.o c-exp.o c-lang.o c-typeprint.o c-valprint.o c-varobj.o charset.o cli-out.o cli/cli-cmds.o cli/cli-decode.o cli/cli-dump.o cli/cli-interp.o cli/cli-logging.o cli/cli-option.o cli/cli-script.o cli/cli-setshow.o cli/cli-style.o cli/cli-utils.o coff-pe-read.o coffread.o compile/compile-c-support.o compile/compile-c-symbols.o compile/compile-c-types.o compile/compile-cplus-symbols.o compile/compile-cplus-types.o compile/compile-loc2c.o compile/compile-object-load.o compile/compile-object-run.o compile/compile.o complaints.o completer.o continuations.o copying.o corefile.o corelow.o cp-abi.o cp-name-parser.o cp-namespace.o cp-support.o cp-valprint.o ctfread.o d-exp.o d-lang.o d-namespace.o d-valprint.o dbxread.o dcache.o debug.o debuginfod-support.o dictionary.o disasm.o dummy-frame.o dwarf2/abbrev.o dwarf2/attribute.o dwarf2/comp-unit.o dwarf2/dwz.o dwarf2/expr.o dwarf2/frame-tailcall.o dwarf2/frame.o dwarf2/index-cache.o dwarf2/index-common.o dwarf2/index-write.o dwarf2/leb.o dwarf2/line-header.o dwarf2/loc.o dwarf2/macro.o dwarf2/read.o dwarf2/section.o dwarf2/stringify.o eval.o event-top.o exceptions.o exec.o expprint.o extension.o f-exp.o f-lang.o f-typeprint.o f-valprint.o filename-seen-cache.o filesystem.o findcmd.o findvar.o frame-base.o frame-unwind.o frame.o gcore.o gdb-demangle.o gdb_bfd.o elfread.o stap-probe.o dtrace-probe.o gdb_obstack.o gdb_regex.o gdbarch.o gdbtypes.o gnu-v2-abi.o gnu-v3-abi.o go-exp.o go-lang.o go-typeprint.o go-valprint.o guile/guile.o inf-child.o inf-loop.o infcall.o infcmd.o inferior.o inflow.o infrun.o inline-frame.o interps.o jit.o language.o linespec.o location.o m2-exp.o m2-lang.o m2-typeprint.o m2-valprint.o macrocmd.o macroexp.o macroscope.o macrotab.o main.o maint-test-options.o maint-test-settings.o maint.o mdebugread.o mem-break.o memattr.o memory-map.o memrange.o mi/mi-cmd-break.o mi/mi-cmd-catch.o mi/mi-cmd-disas.o mi/mi-cmd-env.o mi/mi-cmd-file.o mi/mi-cmd-info.o mi/mi-cmd-stack.o mi/mi-cmd-target.o mi/mi-cmd-var.o mi/mi-cmds.o mi/mi-common.o mi/mi-console.o mi/mi-getopt.o mi/mi-interp.o mi/mi-main.o mi/mi-out.o mi/mi-parse.o mi/mi-symbol-cmds.o minidebug.o minsyms.o mipsread.o namespace.o objc-lang.o objfiles.o observable.o opencl-lang.o osabi.o osdata.o p-exp.o p-lang.o p-typeprint.o p-valprint.o parse.o posix-hdep.o printcmd.o probe.o process-stratum-target.o producer.o progspace-and-thread.o progspace.o prologue-value.o psymtab.o python/python.o ravenscar-thread.o record-btrace.o record-full.o record.o regcache-dump.o regcache.o reggroups.o registry.o remote-fileio.o remote-notif.o remote-sim.o remote.o reverse.o riscv-ravenscar-thread.o riscv-tdep.o run-on-main-thread.o rust-exp.o rust-lang.o sentinel-frame.o ser-base.o ser-event.o ser-pipe.o ser-tcp.o ser-uds.o ser-unix.o serial.o skip.o solib-target.o solib.o source-cache.o source.o stabsread.o stack.o std-regs.o stub-termcap.o symfile-debug.o symfile.o symmisc.o symtab.o target-connection.o target-dcache.o target-descriptions.o target-float.o target-memory.o target.o target/waitstatus.o test-target.o thread-iter.o thread.o tid-parse.o top.o tracectf.o tracefile-tfile.o tracefile.o tracepoint.o trad-frame.o tramp-frame.o type-stack.o typeprint.o ui-file.o ui-out.o ui-style.o user-regs.o utils.o valarith.o valops.o valprint.o value.o varobj.o version.o xml-builtin.o xml-support.o xml-syscall.o xml-tdesc.o init.o           ../sim/riscv/libsim.a ../readline/readline/libreadline.a ../opcodes/libopcodes.a ../libctf/.libs/libctf.a ../bfd/libbfd.a -L./../zlib -lz ../gdbsupport/libgdbsupport.a  ../libiberty/libiberty.a ../libdecnumber/libdecnumber.a   -lm      ../gnulib/import/libgnu.a
  cat ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-gdb-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-gdb.js
else
  echo " - creating ${INSTALLDIR}/${HOST}-gdb.js (exists already)"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
