#!/bin/bash

source scripts/riscv64/versions.sh

# linux
LINUX_URL=https://cdn.kernel.org/pub/linux/kernel/v5.x
LINUX_VERSION=5.10.105

# riscv-pk
RISCVPK_URL=https://github.com/riscv-software-src/riscv-pk
RISCVPK_VERSION=0d3339c73e8401a6dcfee2f0b97f6f52c81181c6

BUSYBOX_URL=https://busybox.net/downloads/
BUSYBOX_VERSION=1.35.0
