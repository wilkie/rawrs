#!/bin/bash

echo ""
echo "Installing RISC-V Linux Target"
echo "=============================="
echo ""

ROOTDIR=$PWD
INSTALLDIR=packages/riscv64-linux

if [ ! -f ${ROOTDIR}/scripts/riscv64-linux/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - checking prerequisites"

CHECK="texi2pdf bison flex gcc make bc"
good=1

for binary in ${CHECK}
do
  if ! hash ${binary} 2>/dev/null; then
    echo " - ERROR: must install ${binary}"
    good=0
  fi
done

if [ ${good} -eq 0 ]; then
  echo " - ERROR: prerequisites needed"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - all prerequisites found"

# Pull in version environment variables
source $PWD/scripts/riscv64-linux/versions.sh

cd ${ROOTDIR}
if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

echo ""

# Install all normal RISC-V toolchains
RAWRS_INSTALLING=1 ${ROOTDIR}/scripts/riscv64/install.sh

echo ""
echo "5. Linux"
echo "--------"
echo ""
echo " - using version ${LINUX_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/linux-${LINUX_VERSION} ]; then
  echo " - downloading Linux ${LINUX_VERSION}"
  wget -nc ${LINUX_URL}/linux-${LINUX_VERSION}.tar.xz -O ${ROOTDIR}/${INSTALLDIR}/linux-${LINUX_VERSION}.tar.xz &> ${ROOTDIR}/${INSTALLDIR}/linux-wget.log
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - unpacking Linux ${LINUX_VERSION}"
  tar xf linux-${LINUX_VERSION}.tar.xz
else
  echo " - linux already found with version ${LINUX_VERSION}"
fi

echo ""
echo "5. RISC-V Proxy Kernel (bootloader)"
echo "-----------------------------------"
echo ""
echo " - using version ${RISCVPK_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/riscv-pk ]; then
  echo " - downloading riscv-pk from git sources"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${RISCVPK_URL} riscv-pk
  cd ../..
else
  echo " - riscv-pk source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/riscv-pk
if ! git cat-file -e ${RISCVPK_VERSION}; then
  echo " - fetching updates to riscv-pk"
  git pull
else
  echo " - revision ${RISCVPK_VERSION} found"
fi

echo ""
echo "6. BusyBox (userspace environment)"
echo "----------------------------------"
echo ""
echo " - using version ${BUSYBOX_VERSION}"

BUSYBOX_TAR_URL=${BUSYBOX_URL}busybox-${BUSYBOX_VERSION}.tar.bz2
if [ ! -d ${ROOTDIR}/${INSTALLDIR}/busybox-${BUSYBOX_VERSION} ]; then
  echo " - downloading BusyBox ${BUSYBOX_VERSION}"
  wget -nc ${BUSYBOX_TAR_URL} -O ${ROOTDIR}/${INSTALLDIR}/busybox-${BUSYBOX_VERSION}.tar.bz2 &> ${ROOTDIR}/${INSTALLDIR}/busybox-wget.log
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - unpacking BusyBox ${BUSYBOX_VERSION}"
  tar xf busybox-${BUSYBOX_VERSION}.tar.bz2
else
  echo " - BusyBox already found with version ${BUSYBOX_VERSION}"
fi

# Return to the base path
cd ${ROOTDIR}

echo ""
echo "Done."
