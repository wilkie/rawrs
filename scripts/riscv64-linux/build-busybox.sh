#!/bin/bash

HEADER="Building RISC-V BusyBox for Linux"
TARGET=riscv64-linux
PACKAGE=busybox

# The host target we are building
HOST="riscv64-unknown-linux-gnu"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/riscv64-linux/busybox-${BUSYBOX_VERSION}
BUILDDIR=packages/riscv64-linux/busybox-build

STATICINSTALLDIR=assets/static/riscv64-linux

echo " - installing/updating RISC-V Linux packages"
./scripts/riscv64-linux/install.sh &> ${ROOTDIR}/packages/riscv64-linux/install-during-busybox.log

# Create build path for building native risc-v busybox
if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - copying source path to ${BUILDDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

# Compile native risc-v busybox
if [ ! -f ${ROOTDIR}/${BUILDDIR}/busybox ]; then
  cd ${ROOTDIR}/${BUILDDIR}

  echo " - configuring a native busybox for RISC-V Linux targets"
  PATH=$PATH:${ROOTDIR}/${UTILSDIR}/usr/bin make defconfig CROSS_COMPILE=riscv64-unknown-linux-gnu- &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native busybox for RISC-V Linux targets"
  run "PATH=$PATH:${ROOTDIR}/${UTILSDIR}/usr/bin make CROSS_COMPILE=riscv64-unknown-linux-gnu-" "${ROOTDIR}/${BUILDDIR}/1-make.log"
else
  echo " - using existing build of RISC-V Linux busybox IN ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${BUILDDIR}/busybox ]; then
  echo " - ERROR: did not build and/or install busybox for RISC-V Linux targets"
  exit 1
fi

# Build ext2 filesystem for running our applications
SBIN_BINARIES="acpid blockdev fbsplash fsck hdparm ifenslave klogd lsmod mkdosfs mkfs.vfat nameif reboot setconsole swapoff syslogd watchdog adjtimex bootchartd fdisk fsck.minix hwclock ifup loadkmap makedevs mke2fs mkswap pivot_root rmmod slattach swapon tunctl zcip arp depmod findfs getty ifconfig logread man mkfs.ext2 modinfo poweroff route start-stop-daemon switch_root udhcpc blkid devmem freeramdisk halt ifdown insmod losetup mdev mkfs.minix modprobe raidautorun runlevel sulogin sysctl vconfig"
BIN_BINARIES="addgroup catv cpio df egrep grep iostat iprule login mknod mt pipe_progress rev sed su uname adduser chattr cttyhack dmesg false gunzip ip iptunnel ls mktemp mv powertop rm setarch sync usleep ash chgrp date dnsdomainname fdflush gzip ipaddr kill lsattr more netstat printenv rmdir sh tar vi base64 chmod dd dumpkmap fgrep hostname ipcalc linux32 lzop mount nice ps rpm sleep touch watch chown delgroup echo fsync hush iplink linux64 makemime mountpoint pidof pwd run-parts stat true zcat cat cp deluser ed getopt ionice iproute ln mkdir mpstat ping reformime scriptreplay stty umount"
USR_BIN_BINARIES="[ cal cryptpw env free install logname microcom patch reset setsid sum time unix2dos wall [[ chat cut envdir ftpget ipcrm lpq mkfifo pgrep resize setuidgid sv timeout unlzma wc add-shell chpst dc envuidgid ftpput ipcs lpr mkpasswd pkill rpm2cpio sha1sum tac top unlzop wget arping chrt deallocvt ether-wake fuser kbd_mode lspci nc pmap rtcwake sha256sum tail tr unxz which awk chvt diff expand hd killall lsusb nmeter printf runsv sha512sum tcpsvd traceroute unzip who basename cksum dirname fdformat head killall5 lzcat nohup pscan runsvdir showkey tee tty uptime whoami beep clear dos2unix fgconsole hexdump last lzma nslookup readlink rx smemcap telnet ttysize uudecode xargs bunzip2 cmp du find hostid length lzopcat od realpath script softlimit test udpsvd uuencode xz bzcat comm dumpleases flock id less md5sum openvt remove-shell seq sort tftp unexpand vlock xzcat bzip2 crontab eject fold ifplugd logger mesg passwd renice setkeycodes split tftpd uniq volname yes"
USR_SBIN_BINARIES="brctl chroot dhcprelay fakeidentd ftpd inetd lpd ntpd rdate readprofile setfont svlogd udhcpd chpasswd crond dnsd fbset httpd loadfont nbd-client popmaildir rdev sendmail setlogcons telnetd"
LIB_LIBRARIES="*.so*"

# Build the filesystem paths
echo " - building ${STATICINSTALLDIR}/busybox-root base paths"
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/dev
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/lib
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/mnt
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/proc
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/root
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/sys
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/tmp
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/var
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/bin
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/sbin
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/bin
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/sbin
mkdir -p ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/lib

# Install /etc
if [ ! -f ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/etc ]; then
  echo " - installing ${STATICINSTALLDIR}/busybox-root/etc"
  cp -r ${PATCHDIR}/etc ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/etc
else
  echo " - installing ${STATICINSTALLDIR}/busybox-root/etc (already exists)"
fi

# Install busybox
if [ ! -f ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/bin/busybox ]; then
  echo " - installing busybox to ${STATICINSTALLDIR}/busybox-root/bin"
  cp ${ROOTDIR}/${BUILDDIR}/busybox ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/bin
else
  echo " - installing busybox to ${STATICINSTALLDIR}/busybox-root/bin (already exists)"
fi

# Install init
if [ ! -f ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/sbin/init ]; then
  echo " - installing init to ${STATICINSTALLDIR}/busybox-root/sbin"
  cp ${PATCHDIR}/init ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/sbin
else
  echo " - installing init to ${STATICINSTALLDIR}/busybox-root/sbin (already exists)"
fi

# Symlink everything else to busybox
echo " - building ${STATICINSTALLDIR}/busybox-root/bin"
for binary in ${BIN_BINARIES}; do
  if [ ! -L "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/bin/${binary}" ]; then
    ln -s busybox "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/bin/${binary}"
  fi
done

echo " - building ${STATICINSTALLDIR}/busybox-root/sbin"
for binary in ${SBIN_BINARIES}; do
  if [ ! -L "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/sbin/${binary}" ]; then
    ln -s ../bin/busybox "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/sbin/${binary}"
  fi
done

echo " - building ${STATICINSTALLDIR}/busybox-root/usr/bin"
for binary in ${USR_BIN_BINARIES}; do
  if [ ! -L "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/bin/${binary}" ]; then
    ln -s ../../bin/busybox "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/bin/${binary}"
  fi
done

echo " - building ${STATICINSTALLDIR}/busybox-root/usr/sbin"
for binary in ${USR_SBIN_BINARIES}; do
  if [ ! -L "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/sbin/${binary}" ]; then
    ln -s ../../bin/busybox "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/sbin/${binary}"
  fi
done

echo " - building ${STATICINSTALLDIR}/busybox-root/usr/lib (copying glibc)"
for library in ${LIB_LIBRARIES}; do
  cp -a --preserve=links ${ROOTDIR}/${STATICINSTALLDIR}/usr/lib/${library} "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/usr/lib/."
done

echo " - building ${STATICINSTALLDIR}/busybox-root/lib (copying glibc interpreter)"
cp -a --preserve=links ${ROOTDIR}/${STATICINSTALLDIR}/usr/lib/ld-* "${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/lib/."

# Build this as an ext2 filesystem binary
if [ ! -f ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root.bin ]; then
  echo " - building busybox-root.bin"
  mke2fs -L '' -N 0 -O ^64bit -d ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root/ -m 5 -r 1 -t ext2 ${ROOTDIR}/${STATICINSTALLDIR}/busybox-root.bin 32M &> ${ROOTDIR}/${BUILDDIR}/2-filesystem-build.log
else
  echo " - building busybox-root.bin (already exists)"
fi

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
exit
