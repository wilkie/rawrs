# Build Scripts

This directory is full of scripts that will build various utilities and the
JavaScript versions of compilation libraries.

It is separated out into several directories. There are "common" utilities and
libraries that might be shared among different targets. And then each target has
a set of build scripts when those targets are to be enabled.
